﻿using System;
using CareConnectionApp.DataAccess;
using CareConnectionApp.DataAccess.Abstractions;
using CareConnectionApp.Services;
using CareConnectionApp.Services.Abstractions;
using CareConnectionApp.Utility_Classes;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CareConnectionApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CCADbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("CCADBConnection")));
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IClientDataAccess, ClientDataAccess>();
            services.AddScoped<IReportingService, ReportingService>();
            services.AddScoped<IReportingDataAccess, ReportingDataAccess>();
            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<IAdminDataAccess, AdminDataAccess>();
            services.AddScoped<IIdentityService, IdentityService>();
            services.AddScoped<IAlertService, AlertService>();
            services.AddScoped<IAlertDataAccess, AlertDataAccess>();
            services.AddTransient<IPhoneNumberClass, PhoneNumberHelper>();
            services.AddScoped<IEmailSender, EmailService>();
            services.AddHealthChecks()
                .AddDbContextCheck<CCADbContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseStatusCodePagesWithReExecute("/Error/{0}");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            if (env.IsDevelopment())
            {
                SeedUsers.Initialize(serviceProvider, env.IsProduction(), Configuration).Wait();
                SeedClients.Initialize(serviceProvider, env.WebRootPath);
                SeedWorkOrderCategories.Initialize(serviceProvider);
                SeedWorkRequests.Initialize(serviceProvider, env.WebRootPath);
                SeedAssistiveDevices.Initialize(serviceProvider);
            }

            if (env.IsProduction())
            {
                SeedUsers.Initialize(serviceProvider, env.IsProduction(), Configuration).Wait();
                SeedWorkOrderCategories.Initialize(serviceProvider);
                SeedAssistiveDevices.Initialize(serviceProvider);
            }
        }
    }
}

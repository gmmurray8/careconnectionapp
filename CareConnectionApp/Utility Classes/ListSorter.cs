﻿using CareConnectionApp.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CareConnectionApp.Utility_Classes
{
    public static class ListSorter
    {
        public static IEnumerable<IGrouping<Client, int>> ClientSort(IEnumerable<IGrouping<Client, int>> clients, string sort, string order)
        {
            switch (sort)
            {
                case "id":
                    clients = order == "asc" ? clients.OrderBy(c => c.Key.Id) : clients.OrderByDescending(c => c.Key.Id);
                    break;
                case "firstName":
                    clients = order == "asc" ? clients.OrderBy(c => c.Key.FirstName) : clients.OrderByDescending(c => c.Key.FirstName);
                    break;
                case "lastName":
                    clients = order == "asc" ? clients.OrderBy(c => c.Key.LastName) : clients.OrderByDescending(c => c.Key.LastName);
                    break;
                case "jobCount":
                    clients = order == "asc" ? clients.OrderBy(c => c.Last())/*.ThenBy(c => c.Key.FirstName) */: clients.OrderByDescending(c => c.Last())/*.ThenBy(c => c.Key.FirstName)*/;
                    break;
                case "streetAddress":
                    clients = order == "asc" ? clients.OrderBy(c => {
                        return Regex.Replace(c.Key.StreetAddress, @"[\d-]", string.Empty);
                        })
                        :
                    clients.OrderByDescending(c => {
                        return Regex.Replace(c.Key.StreetAddress, @"[\d-]", string.Empty);
                        }); 
                    break;
                case "city":
                    clients = order == "asc" ? clients.OrderBy(c => c.Key.City) : clients.OrderByDescending(c => c.Key.City);
                    break;
                case "zip":
                    clients = order == "asc" ? clients.OrderBy(c => c.Key.Zip) : clients.OrderByDescending(c => c.Key.Zip);
                    break;
                default:
                    clients.OrderBy(c => c.Key.Id);
                    break;
            }

            return clients;
        }

        public static IEnumerable<WorkOrder> WorkOrderSort(IEnumerable<WorkOrder> workOrders, string sort, string order)
        {
            switch (sort)
            {
                case "id":
                    workOrders = order == "asc" ? workOrders.OrderBy(w => w.Id) : workOrders.OrderByDescending(w => w.Id);
                    break;
                case "name":
                    workOrders = order == "asc" ? workOrders.OrderBy(w => w.ClientRequest.Client.FirstName).ThenBy(w => w.ClientRequest.Client.LastName) :
                        workOrders.OrderByDescending(w => w.ClientRequest.Client.FirstName).ThenBy(w => w.ClientRequest.Client.LastName);
                    break;
                case "category":
                    workOrders = order == "asc" ? workOrders.OrderBy(w => w.WorkOrderCategory.Category) : workOrders.OrderByDescending(w => w.WorkOrderCategory.Category);
                    break;
                case "date":
                    workOrders = order == "asc" ? workOrders.OrderBy(w => w.DateOfRequest) : workOrders.OrderByDescending(w => w.DateOfRequest);
                    break;
                case "description":
                    workOrders = order == "asc" ? workOrders.OrderBy(w => w.ReqDescription) : workOrders.OrderByDescending(w => w.ReqDescription);
                    break;
                default:
                    workOrders.OrderBy(w => w.Id);
                    break;
            }
            return workOrders;
        }

    }
}

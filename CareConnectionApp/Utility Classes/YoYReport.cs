﻿using System.Collections.Concurrent;

namespace CareConnectionApp.Utility_Classes
{
    public class YoYReport
    {
        public ConcurrentDictionary<int, Year> Years = new ConcurrentDictionary<int, Year>();
        public int TotalUniqueClients { get; set; }
    }
}

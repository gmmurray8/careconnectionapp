﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CareConnectionApp.Utility_Classes
{
    public static class StringHelperClass
    {
        public static string DescriptionEditor(string inputString)
        {
            if (String.IsNullOrEmpty(inputString))
            {
                return "No description";
            }
            else if (inputString.Length > 30)
            {
                return inputString.Substring(0, 30)+"...";
            }
            else
            {
                return inputString;
            }
        }

        public static string CapitalizeLetters(string input)
        {
            if (!String.IsNullOrEmpty(input))
            {
                string[] separator = { " " };
                string[] newString = input.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                string output = "";
                foreach (string s in newString)
                {
                    string temp = char.ToUpper(s[0]) + s.Substring(1);
                    output += temp + " ";
                }

                return output;
            }
            return null;
        }
    }
}

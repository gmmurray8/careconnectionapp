﻿using System;

namespace CareConnectionApp.Utility_Classes
{
    public interface IPhoneNumberClass
    {
        string RemoveDashes(string before);
    }

    public class PhoneNumberHelper : IPhoneNumberClass
    {
        public string RemoveDashes(String before)
        {
            if (before == null)
            {
                return null;
            }
            String after = "";
            if (before.Contains("-"))
            {
                String[] splitString = before.Split("-");
                foreach (String s in splitString)
                {
                    after += s;
                }
                return after;
            }
            else
            {
                return before;
            }

        }
    }
}

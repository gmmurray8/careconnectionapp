﻿using CareConnectionApp.DataAccess;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CareConnectionApp.Utility_Classes
{
    public class SeedWorkOrderCategories
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<CCADbContext>();
            var adminService = serviceProvider.GetService<IAdminService>();
            context.Database.EnsureCreated();

            var workOrderCategories = new List<WorkOrderCategory>();

            workOrderCategories.Add(new WorkOrderCategory { Category = "Other", IsBaseCategory = true });
            workOrderCategories.Add(new WorkOrderCategory { Category = "Donation", IsBaseCategory = true });
            workOrderCategories.Add(new WorkOrderCategory { Category = "Financial Advice", IsBaseCategory = true});
            workOrderCategories.Add(new WorkOrderCategory { Category = "Friendly Visit", IsBaseCategory = true });
            workOrderCategories.Add(new WorkOrderCategory { Category = "Slip & Fall", IsBaseCategory = true });
            workOrderCategories.Add(new WorkOrderCategory { Category = "Assistive Device", IsBaseCategory = true });
            workOrderCategories.Add(new WorkOrderCategory { Category = "Ramp", IsBaseCategory = true });
            workOrderCategories.Add(new WorkOrderCategory { Category = "Repair", IsBaseCategory = true });
            workOrderCategories.Add(new WorkOrderCategory { Category = "Lawn Service", IsBaseCategory = true });

            workOrderCategories.OrderBy(c => c.Category);
            AddCategoriesSeedData(workOrderCategories, adminService);

        }

        public static void AddCategoriesSeedData(List<WorkOrderCategory> categories, IAdminService adminService)
        {
            if (adminService.GetWorkOrderCategories().Any())
            {
                return;
            }
            foreach (var c in categories)
            {
                adminService.AddWorkOrderCategory(c);
            }
        }
    }
}

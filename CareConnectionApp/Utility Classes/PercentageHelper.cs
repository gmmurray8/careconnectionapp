﻿using System.Collections.Concurrent;

namespace CareConnectionApp.Utility_Classes
{
    public class PercentageHelper
    {
        public int TotalJobs { get; set; }
        public ConcurrentDictionary<string, float> Percents = new ConcurrentDictionary<string, float>();
    }
}

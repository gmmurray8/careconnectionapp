﻿using CareConnectionApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CareConnectionApp.Utility_Classes
{
    public class LazyLoader
    {
        public static IEnumerable<object> LazyLoad(IQueryable<Object> query, int pageNum, int resultsPerPage, out int totalResults, out int shownResults)
        {
            totalResults = query.Count();
            var pageResults = query.Take(pageNum * resultsPerPage).ToList();
            shownResults = pageResults.Count();

            return pageResults;
        }

        public static /*List<IGrouping<Client, int>> */ IEnumerable<Client> ClientLazyLoad(IQueryable<Client> query, int pageNum, int resultsPerPage,
                out int totalResults, out int shownResults)
        {
            totalResults = query.Count();
            //var pageResults = query.Select(c => c).Take(pageNum * resultsPerPage).ToList();
            var pageResults = query.Take(pageNum * resultsPerPage);
            shownResults = pageResults.Count();

            return pageResults;
        }

 
    }
}

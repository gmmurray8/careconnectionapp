﻿using System.Collections.Concurrent;
using CareConnectionApp.ViewModels.Reporting;

namespace CareConnectionApp.Utility_Classes
{
    public class Year
    {
        public int YearDate { get; set; }
        public DateRangeReportViewModel YearlyReport = new DateRangeReportViewModel();
        public ConcurrentDictionary<int, Month> Months = new ConcurrentDictionary<int, Month>();

    }
}

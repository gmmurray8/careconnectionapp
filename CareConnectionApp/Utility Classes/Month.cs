﻿using CareConnectionApp.ViewModels.Reporting;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.Utility_Classes

{
    public class Month
    {
        [DataType(DataType.Date)]
        public DateTime MonthDate { get; set; }
        public DateRangeReportViewModel MonthlyReprt { get; set; }
    }
}

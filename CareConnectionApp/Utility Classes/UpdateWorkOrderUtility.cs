﻿using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using CareConnectionApp.ViewModels.WorkOrders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CareConnectionApp.Utility_Classes
{
    public class UpdateWorkOrderUtility 
    {
        private readonly IClientService _clientService;
        public UpdateWorkOrderUtility(IClientService clientService)
        {
            _clientService = clientService;
        }

        private async Task<WorkOrder> UpdateWorkOrder(string workOrderCategory, int Id, JobStatus status, DateTime dateOfReq, string scndContact,
                string scndContactNum, string reqDescrp, int? deviceId, RampRequest ramp, WorkOrderCompletionForm form = null)
        {
            var workOrder = await _clientService.UpdateWorkOrder(workOrderCategory, Id, status, dateOfReq, scndContact,
                    scndContactNum, form, reqDescrp, deviceId);

            if (workOrder.WorkOrderCategory.Category == "Ramp")
            {
                var tempRamp = _clientService.UpdateRampRequest(ramp.Id, ramp.PermanentMobilityImpairment, ramp.MobilityAssistance, ramp.MobilityDescription,
                    ramp.ContactedVA, ramp.OwnerOfHome, ramp.LengthOfHomeOwnership, ramp.LiveAlone);
            }

            return workOrder;
        }

        public async Task<WorkOrder> UpdateIncomepleteWorkOrder(UpdateIncompleteWorkOrderViewModel model)
        {
            var workOrder = await UpdateWorkOrder(model.WorkOrderCategory,model.Id, (JobStatus)model.JobStatus, model.DateOfRequest, model.SecondaryContact,
                model.SecondaryContactNumber, model.ReqDescription, model.DeviceId, model.RampRequest);

            return workOrder;
        }

        public async Task<WorkOrder> UpdateCompleteWorkOrder(UpdateCompleteWorkOrderViewModel model)
        {
            var workOrder = await UpdateWorkOrder(model.WorkOrderCategory ,model.Id, (JobStatus)model.JobStatus, model.DateOfRequest, model.SecondaryContact,
             model.SecondaryContactNumber, model.ReqDescription, model.DeviceId, model.RampRequest, model.WorkOrderCompletionForm);

            return workOrder;

        }
    }
}

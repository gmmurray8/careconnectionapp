﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CareConnectionApp.DataAccess;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace CareConnectionApp.Utility_Classes
{
    public class SeedClients
    {
        public static void Initialize(IServiceProvider serviceProvider, string webRootPath)
        {
            var context = serviceProvider.GetService<CCADbContext>();
            var clientService = serviceProvider.GetService<IClientService>();
            context.Database.EnsureCreated();
            var clientsToAdd = new List<Client>();

            string filePath = webRootPath + "/MOCK_CLIENTS.json";
            using (var reader = new StreamReader(filePath))
            {
                var json = reader.ReadToEnd();
                clientsToAdd = JsonConvert.DeserializeObject<List<Client>>(json);
                if (!context.Clients.Any())
                {
                    SeedClientData(clientsToAdd, clientService);
                }
            }
        }

        public static void SeedClientData(List<Client> clientList, IClientService clientService)
        {
            foreach(var c in clientList)
            {
                clientService.AddClient(c);
            }
        }
    }
}

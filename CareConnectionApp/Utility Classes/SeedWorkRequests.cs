﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CareConnectionApp.DataAccess;
using CareConnectionApp.DataAccess.Abstractions;
using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace CareConnectionApp.Utility_Classes
{
    public class SeedWorkRequests
    {
        public static void Initialize(IServiceProvider serviceProvider, string webRootPath)
        {
            var context = serviceProvider.GetService<CCADbContext>();
            if (!context.WorkOrders.Any())
            {
                var clientDataAccess = serviceProvider.GetService<IClientDataAccess>();
                context.Database.EnsureCreated();
                var requestsToAdd = new List<WorkOrder>();
                var rampsToAdd = new List<RampRequest>();
                var completionFormsToAdd = new List<WorkOrderCompletionForm>();
                string filePath = webRootPath + "/MOCK_REQUESTS.json";
                string rampFilePath = webRootPath + "/MOCK_RAMP.json";
                string formFilePath = webRootPath + "/MOCK_COMPLETION.json";
                using (var reader = new StreamReader(filePath))
                {
                    var json = reader.ReadToEnd();
                    requestsToAdd = JsonConvert.DeserializeObject<List<WorkOrder>>(json);

                }
                using (var newReader = new StreamReader(rampFilePath))
                {
                    var json = newReader.ReadToEnd();
                    rampsToAdd = JsonConvert.DeserializeObject<List<RampRequest>>(json);
                }
                using (var newerReader = new StreamReader(formFilePath))
                {
                    var json = newerReader.ReadToEnd();
                    completionFormsToAdd = JsonConvert.DeserializeObject<List<WorkOrderCompletionForm>>(json);
                }
                SeedRequestData(requestsToAdd, rampsToAdd, completionFormsToAdd, clientDataAccess);
            }
        }

        public static void SeedRequestData(List<WorkOrder> requestList, List<RampRequest> rampList, List<WorkOrderCompletionForm> formList, IClientDataAccess clientData)
        {
            int workOrderId = 1;
            int rampId = 1;
            int formId = 1;

            int numCategories = clientData.GetWorkOrderCategories().Count();

            foreach (var wo in requestList)
            {
                var random = new Random();
                wo.WorkOrderCategoryId = random.Next(1, numCategories);
                if (clientData.GetWorkOrderCategory(wo.WorkOrderCategoryId).Category == "Ramp")
                {
                    var ramp = clientData.AddRampRequest(rampList[rampId]);
                    wo.RampRequestId = ramp.Id;
                    rampId++;
                }

                if (wo.JobStatus == JobStatus.Completed || wo.JobStatus == JobStatus.Unfulfilled)
                {
                    var completionForm = clientData.AddWorkOrderCompletionForm(formList[formId]);
                    wo.WorkOrderCompletionFormId = completionForm.Id;
                    formId++;
                }
                clientData.AddWorkOrder(wo, workOrderId);
                workOrderId++;
            }
        }
    }
}

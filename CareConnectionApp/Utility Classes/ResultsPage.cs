﻿namespace CareConnectionApp.Utility_Classes
{
    public abstract class ResultsPage
    {
        public int TotalResults { get; set; }
        public int ShownResults { get; set; }
        public int ResultsPerPage { get; set; }
        public int PageNum { get; set; }
    }
}

﻿using System;
using System.Threading.Tasks;
using CareConnectionApp.DataAccess;
using CareConnectionApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CareConnectionApp.Utility_Classes
{
    public class SeedUsers
    {
        public static async Task Initialize(IServiceProvider serviceProvider, bool production, IConfiguration configuration)
        {
            var context = serviceProvider.GetService<CCADbContext>();
            context.Database.EnsureCreated();
            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();
            var roleManager = serviceProvider.GetService<RoleManager<IdentityRole>>();
            string[] roles = new string[] { "Admin", "Office Staff", "Inactive"};

            foreach (string role in roles)
            {
                var roleExists = await roleManager.RoleExistsAsync(role);

                if (!roleExists)
                {
                    await roleManager.CreateAsync(new IdentityRole(role));
                }
            }

            CreateAndAddUserToRole(userManager, "Super User", configuration["SuperUserUsername"], configuration["SuperUserPassword"], "Admin");
            if (!production)
            {
                CreateAndAddUserToRole(userManager, "Debbie Downer", configuration["TestOfficeStaffUsername"], configuration["TestOfficeStaffPassword"], "Office Staff");
                CreateAndAddUserToRole(userManager, "He Leftus", configuration["TestInactiveUsername"], configuration["TestInactivePassword"], "Inactive");
            }
        }

        public static void CreateAndAddUserToRole(UserManager<ApplicationUser> userManager, string fullName, string email, string pw, string role)
        {
            var user = new ApplicationUser
            {
                FullName = fullName,
                Email = email,
                UserName = email,
                CreateDate = DateTimeOffset.UtcNow.ToLocalTime()
            };
            if (userManager.FindByEmailAsync(user.Email).Result == null)
            {
                var password = new PasswordHasher<ApplicationUser>();
                var hashed = password.HashPassword(user, pw);
                user.PasswordHash = hashed;
                var result = userManager.CreateAsync(user).Result;
                if (result.Succeeded)
                    userManager.AddToRoleAsync(user, role).Wait();
            }
        }
    }
}


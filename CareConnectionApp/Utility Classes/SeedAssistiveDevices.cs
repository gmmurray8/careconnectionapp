﻿using CareConnectionApp.DataAccess;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CareConnectionApp.Utility_Classes
{
    public class SeedAssistiveDevices
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var adminService = serviceProvider.GetService<IAdminService>();

            List<AssistiveDeviceCategory> categories = new List<AssistiveDeviceCategory>();

            categories.Add(new AssistiveDeviceCategory { Name = "Bed Rails", IsActive=true });
            categories.Add(new AssistiveDeviceCategory { Name = "Bedside Commodes", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Cane", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Crutches", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Grab Bars", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Grabbers", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Hospital Bed", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "IV Pole", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Leg Compression", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Lumbar Massager", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Oxygen Tank Holders", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Pedometer", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Ramp", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Safety", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Scooter", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Security Pole", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Shower Stools", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Step Stools", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Toilet Seats", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Transfer/Shower Bench", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Tub", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Walkers", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Rollators", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Wheelchairs", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Commodes", IsActive = true });
            categories.Add(new AssistiveDeviceCategory { Name = "Lifts", IsActive = true });

            var devices = new List<AssistiveDevice>();

            devices.Add(new AssistiveDevice { Name = "Standard Bed Rail", CategoryId = 1, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Bedside Commode With Wheels", CategoryId = 2, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Bedside Commode", CategoryId = 2, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Commode/Toilet Elevated Rails", CategoryId = 25, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Commode - Extra Wide", CategoryId = 25, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Commode - Wheelchair", CategoryId = 25, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Standard Cane", CategoryId = 3, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Cane - Quad Base", CategoryId = 3, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Crutches - Underarm", CategoryId = 4, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Crutches - Forearm", CategoryId = 4, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Grab Bar - 12\" Stainless Steel", CategoryId = 5, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Grab Bar -  18\" Stainless Steel", CategoryId = 5, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Grab Bar -  24\" Stainless Steel", CategoryId = 5, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Grab Bar -  36\" Stainless Steel", CategoryId = 5, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Grabber/Reacher", CategoryId = 6, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Hospital Bed, Invacare Elec. Model with Rails", CategoryId = 7, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Hoyer Lift Complete - Elec. Lift Model", CategoryId = 26, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "IV Pole - Standalone", CategoryId = 8, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Leg Compression Package with pump in tub", CategoryId = 9, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Lumbar Massager Kit in Box", CategoryId = 10, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Oxygen Tank Holders for Wheelchairs", CategoryId = 11, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Pedometer Exciser", CategoryId = 12, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Portable Aluminum non-folding Ramp", CategoryId = 13, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Portable Wood Ramp", CategoryId = 13, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Rug Traction Tape", CategoryId = 14, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Bath Mats for tub", CategoryId = 14, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Bath Mats for shower", CategoryId = 14, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "LED Nite Lights", CategoryId = 14, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Vinyl Reflective Tape - in shed", CategoryId = 14, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Scooter Electric 3 Wheels", CategoryId = 15, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Security Pole", CategoryId = 16, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Shower Stool with back", CategoryId = 17, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Shower Stool no back", CategoryId = 17, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Shower Stool with back and side handles", CategoryId = 17, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Step Stool with Handle", CategoryId = 18, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Toilet Seat - Raised - Standard RND no handles", CategoryId = 19, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Toilet Seat - Raised - Standard RND with handles", CategoryId = 19, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Toilet Seat - Raised - Elongated Style", CategoryId = 19, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Transfer/Shower Bench for Tub", CategoryId = 20, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Transfer/Shower Bench for Shower", CategoryId = 20, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Tub Bench - no legs", CategoryId = 21, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Tub Rails", CategoryId = 21, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Walker -  Hemi Single Side Assistant", CategoryId = 22, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Walker - No Wheels", CategoryId = 22, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Walker - Knee Walker", CategoryId = 22, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Walker - Rollator Wide (Heavy Duty)", CategoryId = 22, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Walker - Rollator (Standard)", CategoryId = 22, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Walker - With Wheels", CategoryId = 22, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Walker - With Wheels and Arm Supports", CategoryId = 22, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Walker - 3 Wheels with Brakes", CategoryId = 22, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Rollator Trays", CategoryId = 23, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Rollator Carry Bags", CategoryId = 23, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Rollator Baskets", CategoryId = 23, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Rollator Carry Bags", CategoryId = 23, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Wheelchair - High Back", CategoryId = 24, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Wheelchair - Manual (Standard - 16\" Seat Width)", CategoryId = 24, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Wheelchair - Manual (18\" or Wider Seat Width)", CategoryId = 24, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Wheelchair - Motorized - Standard", CategoryId = 24, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Wheelchair - Transport Standard Model", CategoryId = 24, IsActive = true });
            devices.Add(new AssistiveDevice { Name = "Wheelchair - Manual Small Size Model", CategoryId = 24, IsActive = true });

            AddAssistiveDevicesSeedData(categories, devices, adminService);
        }

        public static void AddAssistiveDevicesSeedData(List<AssistiveDeviceCategory> categories, List<AssistiveDevice> devices, IAdminService adminService)
        {
            if (adminService.GetAssistiveDeviceCategories().Any() && adminService.GetAssistiveDevices().Any())
            {
                return;
            }
            foreach (AssistiveDeviceCategory c in categories)
            {
                adminService.AddAssistiveDeviceCategory(c);
            }
            foreach (AssistiveDevice d in devices)
            {
                adminService.AddAssistiveDevice(d);
            }
        }
    }
}

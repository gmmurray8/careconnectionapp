﻿using CareConnectionApp.DataAccess.Abstractions;
using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using CareConnectionApp.Utility_Classes;
using CareConnectionApp.ViewModels.Reporting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CareConnectionApp.Services
{
    public class ReportingService : IReportingService
    {
        private readonly IReportingDataAccess _reporting;
        private readonly IClientDataAccess _clientDataAccess;

        public ReportingService
        (
            IReportingDataAccess reportingDataAccess,
            IClientDataAccess clientDataAccess
        )
        {
            _reporting = reportingDataAccess;
            _clientDataAccess = clientDataAccess;
        }

        public JobSummaryResult JobSummary(JobTypeSummaryReportViewModel model)
        {
            var workOrders = _clientDataAccess.GetWorkOrders().ToList()
                .Where(w => ((w.JobStatus == JobStatus.Completed || w.JobStatus == JobStatus.Unfulfilled) && (w.WorkOrderCompletionForm.DateCompleted <= model.UpperBound &&
                    w.WorkOrderCompletionForm.DateCompleted >= model.LowerBound && w.WorkOrderCategory.Category.Equals(model.workOrderCategory)))
                    || (w.JobStatus == JobStatus.Active || w.JobStatus == JobStatus.Inactive) && (w.DateOfRequest >= model.LowerBound &&
                    w.WorkOrderCategory.Category.Equals(model.workOrderCategory)));

            decimal expenses = 0;
            decimal donations = 0;
            int miles = 0;
            float hours = 0;

            foreach (var wo in workOrders)
            {
                expenses += wo.WorkOrderCompletionForm?.Expenses ?? 0;
                donations += wo.WorkOrderCompletionForm?.Donation ?? 0;
                miles += wo.WorkOrderCompletionForm?.Mileage ?? 0;
                hours += wo.WorkOrderCompletionForm?.HoursWorked ?? 0;
            }

            var jobSummaryResult = new JobSummaryResult
            {
                JobType = model.workOrderCategory,
                NumOfTypeActive = workOrders.Where(wo => wo.JobStatus.Equals(JobStatus.Active)).Count(),
                NumOfTypeCompleted = workOrders.Where(wo => wo.JobStatus.Equals(JobStatus.Completed)).Count(),
                NumOfTypeDeferred = workOrders.Where(wo => wo.JobStatus.Equals(JobStatus.Unfulfilled)).Count(),
                NumOfTypeInactive = workOrders.Where(wo => wo.JobStatus.Equals(JobStatus.Inactive)).Count(),
                NumOfTypes = workOrders.Count(),
                NumOfUniqueClients = workOrders.Select(wo => wo.ClientRequest.Client.Id).Distinct().Count(),
                Expenses = expenses,
                Donations = donations,
                Miles = miles,
                Hours = hours
            };
            return jobSummaryResult;
        }

        public List<string> WorkOrderCategoryNames()
        {
            return _reporting.GetWorkOrderCategoryNames();
        }

        public async Task<DateRangeReportViewModel> GenerateDateRangeReport(DateRangeViewModel dateRangeViewModel)
        {
            var report = await _reporting.GetWorkOrders(w => w.WorkOrderCompletionForm.DateCompleted <= dateRangeViewModel.UpperBound
                                                        && w.WorkOrderCompletionForm.DateCompleted >= dateRangeViewModel.LowerBound
                                                        && !w.WorkOrderCategory.Category.Equals("Lawn Service"));

            var workOrderCategories = report.Select(w => w.WorkOrderCategory.Category).Distinct().ToList(); // _reporting.GetWorkOrderCategoryNames().Where(cat => !cat.Equals("Lawn Service")).ToList();

            return CalculateReport(report, workOrderCategories);
        }

        public async Task<YoYReport> YearOverYearReport(int startYear, int endYear)
        {
            var startDate = DateTime.Parse("01/01/" + startYear);
            var endDate = DateTime.Parse("12/31/" + endYear);

            Expression<Func<WorkOrder, bool>> filter = w => w.WorkOrderCompletionForm.DateCompleted <= endDate
                                                             && w.WorkOrderCompletionForm.DateCompleted >= startDate
                                                             && !w.WorkOrderCategory.Category.Equals("Lawn Service");

            var workOrders = await _reporting.GetWorkOrders(filter);
            var workOrderCategories = _reporting.GetWorkOrderCategoryNames().Where(cat => !cat.Equals("Lawn Service")).ToList();
            //var workOrderCategories = workOrders.Select(w => w.WorkOrderCategory.Category).Distinct().ToList();
            var totalReportUniqueClients = workOrders.Select(wo => wo.ClientRequest.ClientId).Distinct().Count();

            var yoYReport = new YoYReport();
            yoYReport.TotalUniqueClients = totalReportUniqueClients;

            var start = startDate;
            var numOfYears = endYear - startYear + 1;


           // Parallel.For(0, numOfYears, index =>
           for(int i = startYear; i <= endYear; i++) // yearly loop
            {
                //Generate the years and yearly reports for the yoYReport Object
                var year = new Year();
                //year.YearDate = startYear + index;
                year.YearDate = i;
                //var monthDate = start.AddYears(index);
                var monthDate = startDate.AddYears(i - startYear);
                var workOrderRangeYearly = workOrders.Where(wo => wo.WorkOrderCompletionForm.DateCompleted.Year == year.YearDate).ToList();
                year.YearlyReport = CalculateReport(workOrderRangeYearly, workOrderCategories);

               // Parallel.For(0, 12, Index =>
               for(int j = 0; j < 12; j++)
                {
                    //Generate the month and monthly reports for the year Object
                    var month = new Month();
                    month.MonthDate = monthDate.AddMonths(j+1);

                    var workOrdeRangeMonthly = workOrders
                        .Where(wo => wo.WorkOrderCompletionForm.DateCompleted <= month.MonthDate.AddMonths(1).AddDays(-1)
                            && wo.WorkOrderCompletionForm.DateCompleted >= month.MonthDate).ToList();
                    month.MonthlyReprt = CalculateReport(workOrdeRangeMonthly, workOrderCategories);

                    year.Months.TryAdd(month.MonthDate.Month, month);

                };//end month loop

                yoYReport.Years.TryAdd(year.YearDate, year);

            };//end year loop

            return yoYReport;

        }

        public Dictionary<string, int> GetIndividualClientStatistics(int id)
        {
            var client = _clientDataAccess.GetClient(id);
            var result = new Dictionary<string, int>();
            var workOrders = _reporting.GetWorkOrdersByClient(id);
            result.Add($"{client.FirstName} {client.LastName}", 0);
            foreach (var order in workOrders)
            {
                var categoryName = order.WorkOrderCategory.Category;
                var completedName = $"{categoryName} Completed";
                if (!result.ContainsKey(categoryName))
                {
                    result.Add(categoryName, 1);
                }
                else
                {
                    result[categoryName]++;
                }

                if (order.JobStatus == JobStatus.Completed)
                {
                    if (!result.ContainsKey(completedName))
                    {
                        result.Add(completedName, 1);
                    }
                    else
                    {
                        result[completedName]++;
                    }
                }
            }
            result.Add("Completed Requests", workOrders.Where(order => order.JobStatus == JobStatus.Completed).Count());
            result.Add("Total Requests", workOrders.Count());

            return result;
        }

        public List<object> GetClientsWithMostWorkOrders()
        {
            var clients = new List<object>();

            var topFive = _clientDataAccess.GetClients().ToList()
                .OrderByDescending(client => client.ClientRequests.Count)
                .Take(5);

            foreach (var client in topFive)
            {
                clients.Add(new
                {
                    id = client.Id,
                    name = $"{client.FirstName} {client.LastName}",
                    numRequests = client.ClientRequests.Count()
                });
            }

            return clients;
        }

        public double GetAverageNumWorkOrder()
        {
            var numberWorkOrders = _clientDataAccess.GetClients().ToList()
                .Select(client => client.ClientRequests.Count());
            return numberWorkOrders.Average();
        }

        public object PrepareClientsReport()
        {
            return new
            {
                totalClients = _clientDataAccess.GetClients().Count(),
                activeClients = _clientDataAccess.GetClients().ToList().Where(client => client.ClientStatus == ClientStatus.Active).Count(),
                mostWorkOrders = GetClientsWithMostWorkOrders(),
                avgWorkOrders = GetAverageNumWorkOrder()
            };
        }

        private IEnumerable<Client> GetClientsByWorkOrderCategory(string categoryName)
        {
            var clients = _clientDataAccess.GetClients().ToList();
            var categoryId = _clientDataAccess.GetWorkOrderCategoryId(categoryName);
            var clientIds = _clientDataAccess.GetWorkOrders().ToList()
                .Where(workOrder => workOrder.WorkOrderCategoryId == categoryId)
                .Select(workOrder => workOrder.ClientRequest.ClientId)
                .Distinct();
            var result = clients.Where(client => clientIds.Contains(client.Id));

            return result;
        }

        public object PrepareClientsByCategoryReport(string categoryName)
        {
            var clientsList = new List<object>();
            foreach (var client in GetClientsByWorkOrderCategory(categoryName).ToList())
            {
                clientsList.Add(new
                {
                    id = client.Id,
                    name = $"{client.FirstName} {client.LastName}",
                    phone = client.HomePhone ?? client.CellPhone,
                    address = client.StreetAddress
                });
            }
            return new
            {
                category = categoryName,
                results = clientsList
            };
        }

        public LawnServiceReportViewModel LawnServiceReport(DateTime start, DateTime end)
        {
            var clientLawnService = _reporting.GetClientLawnServices()
                .Where(cls => cls.TimeStamp >= start && cls.TimeStamp <= end);
            var lawnServices = _reporting.GetLawnServices()
                .Where(ls => ls.TimeStamp >= start && ls.TimeStamp <= end);
            var uniqueClients = clientLawnService.GroupBy(cls => cls.ClientId)
                .Select(c => c.Distinct()).Count();

            var report = new LawnServiceReportViewModel()
            {
                TotalLawnsCut = clientLawnService.Count(),
                TotalUniqueClients = uniqueClients,
                TotalLawnServices = lawnServices.Count()
            };

            var zoneList = clientLawnService.GroupBy(cls => cls.ZoneId).Select(z => z.Distinct()).ToList();


            foreach (var z in clientLawnService.GroupBy(cls => cls.ZoneId).Select(z => z.Distinct()))
            {
                object zoneReport = new
                {
                    z.First().Client.ZoneJoining.Zone.Name,
                    LawnsCut = clientLawnService.Where(cls => cls.ZoneId == z.First().ZoneId).Count(),
                    uniqueClients = clientLawnService.Where(cls => cls.ZoneId == z.First().ZoneId).GroupBy(cls => cls.ClientId).Select(c => c.Distinct()).Count()
                };
                report.ZoneReports.Add(zoneReport);
            }
            report.StartDate = start;
            report.EndDate = end;
            return report;
        }

        private DateRangeReportViewModel CalculateReport(List<WorkOrder> report, List<string> workOrderCategoryNames = null)
        {
            var dateRangeReportViewModel = new DateRangeReportViewModel
            {

                NumOfUniqueClients = report.Select(wo => wo.ClientRequest.Client.Id).Distinct().Count(),
                NumOfJobsCompleted = report.Select(wo => wo).Where(wo => wo.JobStatus.Equals(JobStatus.Completed)).Count(),
                NumOfJobsDeferred = report.Select(wo => wo).Where(wo => wo.JobStatus.Equals(JobStatus.Unfulfilled)).Count(),

            };

            foreach (var c in workOrderCategoryNames)
            {
                object rprtObject = new
                {
                    category = c,
                    count = report.Select(wo => wo).Where(wo => wo.JobStatus.Equals(JobStatus.Completed) && wo.WorkOrderCategory.Category.Equals(c)).Count()
                };

                dateRangeReportViewModel.CategoriesReport.Add(rprtObject);
            }

            foreach (var wo in report)
            {
                dateRangeReportViewModel.Donations += wo.WorkOrderCompletionForm.Donation;
                dateRangeReportViewModel.Expenses += wo.WorkOrderCompletionForm.Expenses;
                dateRangeReportViewModel.Hours += wo.WorkOrderCompletionForm.HoursWorked;
                dateRangeReportViewModel.Miles += wo.WorkOrderCompletionForm.Mileage;
            }

            return dateRangeReportViewModel;

        }
    }
}

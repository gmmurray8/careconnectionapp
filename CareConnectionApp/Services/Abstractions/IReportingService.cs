﻿using CareConnectionApp.Models;
using CareConnectionApp.Utility_Classes;
using CareConnectionApp.ViewModels.Reporting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CareConnectionApp.Services.Abstractions
{
    public interface IReportingService
    {
        /// <summary>
        /// This Method takes a date range from two date variables
        /// and returns a DateRangeReportViewModel that contains
        /// all of the relavent reporting data for the given 
        /// range. 
        /// 
        /// This data includes total number of unique clients served,
        /// number of jobs completed, number of jobs defered,
        /// total volunteer miliage, total volunteer hours,
        /// total donations, and total expenses
        /// </summary>
        /// <param name="dateRangeViewModel"></param>
        /// <returns>DateRangeViewModel</returns>
        Task<DateRangeReportViewModel> GenerateDateRangeReport(DateRangeViewModel dateRangeViewModel);
        Task<YoYReport> YearOverYearReport(int start, int endYear);
        JobSummaryResult JobSummary(JobTypeSummaryReportViewModel model);
        List<string> WorkOrderCategoryNames();
        Dictionary<string, int> GetIndividualClientStatistics(int id);
        object PrepareClientsReport();
        object PrepareClientsByCategoryReport(string categoryName);
        LawnServiceReportViewModel LawnServiceReport(DateTime start, DateTime end);
    }
}

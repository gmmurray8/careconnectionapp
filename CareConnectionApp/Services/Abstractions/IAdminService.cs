﻿using CareConnectionApp.Models;
using System;
using System.Collections.Generic;

namespace CareConnectionApp.Services.Abstractions
{
    public interface IAdminService
    {
        List<WorkOrderCategory> GetWorkOrderCategories();
        WorkOrderCategory AddWorkOrderCategory(WorkOrderCategory workOrderCategory);
        WorkOrderCategory GetWorkOrderCategory(int Id);
        WorkOrderCategory UpdateWorkOrderCategory(WorkOrderCategory workorderChanges);
        bool DeleteWorkOrderCategory(int workOrderCategoryId);
        bool WorkOrderCategoryExists(WorkOrderCategory category);
        bool CategoryNameExists(WorkOrderCategory category);
        AssistiveDevice AddAssistiveDevice(AssistiveDevice device);
        AssistiveDevice GetAssistiveDevice(int? id);
        AssistiveDeviceCategory AddAssistiveDeviceCategory(AssistiveDeviceCategory category);
        AssistiveDeviceCategory GetAssistiveDeviceCategory(int id);
        AssistiveDevice UpdateAssistiveDevice(int id, bool isActive, string name, string description, int categoryId);

        AssistiveDeviceCategory UpdateAssistiveDeviceCategory(bool isActive, string name, string description, int categoryId);
        IEnumerable<AssistiveDevice> GetAssistiveDevices();
        IEnumerable<AssistiveDeviceCategory> GetAssistiveDeviceCategories();

        Dictionary<int, int> GetNumberNewClients(string adminId);

        Dictionary<int, int> GetNumberJobsOpened(string adminId);

        Dictionary<int, int> GetNumberJobsCompleted(string adminId);

        IEnumerable<WorkOrder> GetRecentWorkOrders(string userId);

        IEnumerable<WorkOrder> GetRecentAssistiveDeviceWorkOrders(string userId);
        Dictionary<WorkOrder, AssistiveDevice> CombineWorkOrderAssistiveDevice(IEnumerable<WorkOrder> workOrders);
        IEnumerable<PendingApproval> GetPendingApprovals();
        void DeleteFromPendingApprovals(PendingApproval pendingApproval);
        void DeleteFromPendingApprovals(int id);
        PendingApproval GetPendingApproval(int id);
        WaitList AddToWaitList(int id);
        void DeleteFromWaitList(WaitList waitList);
        WaitList GetWaitListById(int id);
        LawnServiceList AddToLawnServiceList(int id);
        LawnServiceList GetLawnServiceListById(int id);
        void DeleteFromServiceList(int id);
        Zone AddZone(Zone zone);
        Zone UpdateZone(Zone zone);
        Zone GetZone(int id);
        ZoneJoining AddZoneJoin(int clientId, int zoneId);
        IEnumerable<Zone> GetZones();
        IEnumerable<ZoneJoining> GetZoneJoins();
        LawnService AddLawnService(int zoneId, DateTime timeStamp);
        bool CheckZoneStatus(int id);
        IEnumerable<PendingApproval> GetRecentPendingApprovals(string userId);
        List<WaitList> AddToWaitList(IEnumerable<int> waitListIds);
        List<LawnServiceList> AddToLawnServiceList(IEnumerable<int> waitListIds);
        AdminSetting GetAdminSetting(string userId);
        int GetAlertSetting(string userId);
        int GetAtAGlanceSetting(string userId);
        int GetRecentRequestSetting(string userId);
        int GetServiceListLimitSetting(string userId);
        AdminSetting SetNewSettings(AdminSetting newSettings);
        AdminSetting SetAlertSetting(string userId, int duration);
        AdminSetting SetAtAGlanceSetting(string userId, int duration);
        AdminSetting SetRecentRequestSetting(string userId, int count);
        AdminSetting SetServiceListLimitSetting(string userId, int count);
        string TooManyClientsWarning(string userId);
    }
}

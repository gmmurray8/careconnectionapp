﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using CareConnectionApp.Models;
using Microsoft.AspNetCore.Identity;

namespace CareConnectionApp.Services.Abstractions
{
    public interface IIdentityService
    {
        Task<IdentityResult> AddUser(string email, string username, string fullName, string password, string role);
        Task<IdentityResult> AddUserToRoles(ApplicationUser user, IEnumerable<string> roles);
        Task<IdentityResult> ChangePassword(ApplicationUser user, string oldPass, string newPass);
        IEnumerable<IdentityRole> GetRoles();
        Task<ApplicationUser> GetUser(string id);
        Task<ApplicationUser> GetUserByClaim(ClaimsPrincipal user);
        Task<string> GetUserIdByClaim(ClaimsPrincipal user);
        Task<ApplicationUser> GetUserByUsername(string username);
        Task<ApplicationUser> GetUserByEmail(string email);
        Task<ApplicationUser> GetUserByEmailOrUserName(string input);
        Task<string> GetUserPrimaryRole(ApplicationUser user);
        Task<string> GetUserPrimaryRole(ClaimsPrincipal principal);
        Task<IEnumerable<string>> GetUserRoles(ApplicationUser user);
        Task<ApplicationUser> UserIsAdmin(ApplicationUser user);
        Task<ApplicationUser> UserIsAdmin(ClaimsPrincipal principal);
        IEnumerable<ApplicationUser> GetUsers();
        IEnumerable<ApplicationUser> GetUsersByEmail(string email);
        IEnumerable<ApplicationUser> SearchUsers(string searchString);
        Task<IdentityResult> RemoveUserFromRoles(ApplicationUser user, IEnumerable<string> roles);
        Task<IdentityResult> ResetPassword(ApplicationUser user, string password);
        Task<IEnumerable<ApplicationUser>> GetUsersByRole(string role);
        Task<IdentityResult> ResetPassword(ApplicationUser user, string token, string password);
        Task<IdentityResult> UpdateUser(ApplicationUser user);
        Task<IdentityResult> UpdateUserInformation(ApplicationUser user, string email, string userName, string fullName);
        Task<IdentityResult> UpdateUserLoginDate(ApplicationUser user);
        Task<string> GetUserEmailConfirmationTokenById(string userId);
        Task<IdentityResult> ConfirmEmailWithId(string userId, string token);
        Task<string> GetPasswordResetTokenById(string userId);
        Task SendForgotPasswordEmail(string link, string email);
        Task SendConfirmationEmail(string link, string email);
    }
}
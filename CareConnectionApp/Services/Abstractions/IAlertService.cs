﻿using CareConnectionApp.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CareConnectionApp.Services.Abstractions
{
    public interface IAlertService
    {
        Task AddClientAlert(string alertText, int? ClientId = null, int? workOrderId = null, int? assistiveDeviceId = null);
        void MarkAlertAsRead(IEnumerable<int> id);
        Task<object> GetAlerts(string AdminId);
        void DeleteAlerts(IEnumerable<int> alertIds);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using CareConnectionApp.ViewModels;
using CareConnectionApp.ViewModels.Clients;

namespace CareConnectionApp.Services.Abstractions
{
    public interface IClientService
    {
        Client AddClient(Client client);
        Eheap AddEheap(Eheap eheap);
        RampRequest AddRampRequest(RampRequest rampRequest);
        WorkOrder AddWorkOrder(WorkOrder workOrder, int clientId);
        WorkOrderCompletionForm AddWorkOrderCompletionForm(WorkOrderCompletionForm form);
        Eheap CreateEheap(DateTime dateOfService, int clientId, decimal expenses);
        RampRequest CreateRampRequest(RampRequest rampRequest, WorkOrder workOrder, int clientId);
        Task<WorkOrder> CreateWorkOrder(int clientId, string type, string description, string secondaryContact, string secondaryContactNumber);
        IEnumerable<ClientRequest> FilterClientRequests(Client client, string type, JobStatus status);
        IQueryable<WorkOrder> FilterWorkOrders(string type, JobStatus status);
        Client GetClient(int id);
        IEnumerable<ClientRequest> GetClientRequests(int clientId);
        Eheap GetEheap(int? id);
        Eheap UpdateEheap(int clientId, DateTime dateOfService, decimal expenses);
        RampRequest GetRampRequest(int? id);
        WorkOrderCompletionForm GetWorkOrderCompletionForm(int id);
        WorkOrder GetWorkOrderWithClient(int id);
        IEnumerable<Client> ListActiveClients();
        IEnumerable<Client> ListClients();
        SearchClientViewModel SearchClients(SearchClientViewModel search);
        Client UpdateClient(Client client);
        WorkOrder CompleteWorkOrder(int id, JobStatus status, string secondaryContact,
        string secondaryContactNumber, WorkOrderCompletionForm form, string reqDescription, int? deviceId);
        Task<WorkOrder> UpdateWorkOrder(string category, int id, JobStatus status, DateTime dateOfRequest, string secondaryContact, string secondaryContactNumber, WorkOrderCompletionForm form, string reqDescription, int? deviceId);
        WorkOrderCompletionForm UpdateWorkOrderCompletionForm(WorkOrderCompletionForm form);
        IEnumerable<UserClientAdditions> GetUserClientAdditions(string userId);
        UserClientAdditions AddUserClientAddition(string userId, int clientId);
        IEnumerable<UserClientAdditions> SearchUserClientAdditions(string userId, string searchString);
        AdminFaveClient AddFavorite(int clientId, string adminId);
        AdminFaveClient SearchAdminFave(int clientId, string adminId);
        AdminFaveClient RemoveFavorite(int clientId, string adminId);
        List<Client> GetAdminFaveClients(string adminId);
        List<int> GetAdminFaveIDs(string adminId);
        IEnumerable<AssistiveDeviceCategory> GetActiveAssistiveDeviceCategories();
        IEnumerable<AssistiveDevice> GetActiveAssistiveDevices();
        IEnumerable<AssistiveDevice> GetAssistiveDevices(int CategoryId);
        List<AssistiveDevice> GetActiveAssistiveDevices(int CategoryId);
        AssistiveDevice GetAssistiveDevice(int? assistiveDeviceId);
        AssistiveDevice CreateAssistiveDevice(AssistiveDevice assistiveDevice, WorkOrder workOrder, int ClientId);
        List<WorkOrderCategory> GetWorkOrderCategories();
        List<string> GetWorkOrderCategoriesNames();
        int GetWorkOrderCategoryId(string cat);
        List<string> GetworkOrderCategoryNamesForCreation();
        List<string> GetWorkOrderCategoryNamesForSearching();
        WorkOrderCategory GetWorkOrderCategory(int id);
        RampRequest UpdateRampRequest(int id, bool mobilityImpairment, MobilityAssistance mobilityAssistance, string mobilityDescription, bool VA, bool homeOwner, string lengthOfResidence, bool liveAlone);
        PendingApproval AddToPendingList(int clientId, int workOrderId);
        IEnumerable<WaitList> GetWaitList();
        IEnumerable<Zone> GetZones();
        Zone GetZoneById(int id);
        int DaysSinceLastService(int id);
        IEnumerable<WaitList> GetWaitListByZone(int id);
        string ListClientIsOn(int id);
        Dictionary<LawnServiceList, string> GetLawnServiceList();
        Dictionary<LawnServiceList, string> GetLawnServiceList(int id);
        IEnumerable<LawnServiceList> GetLawnServiceListForPrinting(int id);
    }
}
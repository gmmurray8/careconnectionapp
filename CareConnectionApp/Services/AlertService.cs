﻿using CareConnectionApp.DataAccess.Abstractions;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CareConnectionApp.Services
{
    public class AlertService : IAlertService
    {
        private readonly IAlertDataAccess _alertDataAccess;
        private readonly IAdminService _adminService;
        private readonly IClientDataAccess _clientDataAccess;
        private readonly IIdentityService _identityService;

        public AlertService
        (
            IAlertDataAccess alertDataAccess,
            IClientDataAccess clientDataAccess,
            IAdminService adminService,
            IIdentityService identityService
        )
        {
            _alertDataAccess = alertDataAccess;
            _adminService = adminService;
            _clientDataAccess = clientDataAccess;
            _identityService = identityService;
        }

        /// <summary>
        /// Adds an alert for each admin that has the specified client on their watch list. 
        /// </summary>
        /// <param name="alertText"></param>
        /// <param name="ClientId"></param>
        /// <param name="workOrderId"></param>
        /// <param name="assistiveDeviceId"></param>
        public async Task AddClientAlert(string alertText, int? ClientId = null, int? workOrderId = null, int? assistiveDeviceId = null) //this method should be asynch
        {

            var adminsWithClientFavorited = await AdminsWithClientFavorited(ClientId);

            foreach (var a in adminsWithClientFavorited)
            {
                var alert = new Alert
                {
                    AdminId = a,
                    AlertText = alertText,
                    ClientId = ClientId,
                    WorkOrderId = workOrderId,
                    AssistiveDeviceId = assistiveDeviceId,
                    TimeStamp = DateTime.Now,
                    Read = false
                };

                _alertDataAccess.AddAlert(alert);
            }

        }

        /// <summary>
        /// Adds an alert that will ping all Admins
        /// </summary>
        /// <param name="alertText"></param>
        /// <param name="ClientId"></param>
        /// <param name="workOrderId"></param>
        /// <param name="assistiveDeviceId"></param>
        public async Task AddUniversalAlert(string alertText, int? ClientId = null, int? workOrderId = null, int? assistiveDeviceId = null)
        {
            var admins = await _identityService.GetUsersByRole("Admin");
            foreach (var a in admins)
            {
                var alert = new Alert
                {
                    AdminId = a.Id,
                    AlertText = alertText,
                    ClientId = ClientId,
                    WorkOrderId = workOrderId,
                    AssistiveDeviceId = assistiveDeviceId,
                    TimeStamp = DateTime.Now,
                    Read = false
                };

                _alertDataAccess.AddAlert(alert);
            }
        }

        public async Task PruneAlerts(string AdminId)
        {
            var admin = (await _identityService.GetUsersByRole("Admin")).FirstOrDefault(a => a.Id.Equals(AdminId));
            var alertLifespan = _adminService.GetAlertSetting(AdminId);
            var deleteDate = DateTime.Today.AddDays(-alertLifespan);
            await _alertDataAccess.PruneAlerts(admin, deleteDate);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="AdminId"></param>
        /// <returns></returns>
        public async Task<object> GetAlerts(string AdminId)
        {
            await PruneAlerts(AdminId);
            var alertsList = _alertDataAccess.GetAlerts(AdminId);
            object alerts = new
            {
                alerts = alertsList,
                unreadAlertCount = alertsList.Count(A => A.Read == false),
                allAlertCount = alertsList.Count()
            };
            return alerts;
        }

        public void MarkAlertAsRead(IEnumerable<int> AlertId)
        {
            _alertDataAccess.MarkAlertAsRead(AlertId);
        }

        public async Task<List<string>> AdminsWithClientFavorited(int? ClientId)
        {
            var admins = await _identityService.GetUsersByRole("Admin");
            var adminsWithClientFavorited = new List<string>();

            foreach (var a in admins)
            {
                var adminFaves = _clientDataAccess.GetAdminFaveIDs(a.Id);
                if (adminFaves.Any(af => af == ClientId))
                {
                    adminsWithClientFavorited.Add(a.Id);
                }
            }

            return adminsWithClientFavorited;
        }

        public void DeleteAlerts(IEnumerable<int> alertIds)
        {
            _alertDataAccess.DeleteAlerts(alertIds);
        }
    }
}

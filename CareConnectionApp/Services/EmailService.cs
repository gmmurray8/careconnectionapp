﻿using System.Threading.Tasks;
using SendGrid.Helpers.Mail;
using SendGrid;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Configuration;

namespace CareConnectionApp.Services
{
    public class EmailService : IEmailSender
    {
        private readonly IConfiguration _configuration;

        public EmailService
        (
            IConfiguration configuration
        )
        {
            _configuration = configuration;
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            var myMessage = new SendGridMessage();
            myMessage.AddTo(email);
            myMessage.From = new EmailAddress(_configuration["SendGridFromAddress"], _configuration["SendGridFromName"]);
            myMessage.Subject = subject;
            myMessage.PlainTextContent = message;
            myMessage.HtmlContent = message;

            var apiKey = _configuration["SendGridAPIKey"];

            var client = new SendGridClient(apiKey);
            return client.SendEmailAsync(myMessage);
        }
    }
}

﻿using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CareConnectionApp.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IEmailSender _emailSender;

        public IdentityService(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IEmailSender emailSender
        )
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
        }

        #region Users

        public IEnumerable<ApplicationUser> GetUsers()
        {
            return _userManager.Users;
        }

        public IEnumerable<ApplicationUser> GetUsersByEmail(string email)
        {
            return _userManager.Users.Where(u => u.Email.Contains(email));
        }

        public IEnumerable<ApplicationUser> SearchUsers(string searchString)
        {
            if (string.IsNullOrEmpty(searchString))
            {
                return _userManager.Users;
            }
            searchString = searchString.ToLower();
            return _userManager.Users.Where(u => u.UserName.ToLower().Contains(searchString) || u.Email.ToLower().Contains(searchString));
        }

        public async Task<IdentityResult> AddUser(string email, string username, string fullName, string password, string role)
        {
            var user = new ApplicationUser
            {
                FullName = fullName,
                Email = email,
                UserName = username,
                CreateDate = DateTimeOffset.UtcNow.ToLocalTime(),
                LastLoginDate = DateTimeOffset.UtcNow.ToLocalTime()
            };
            var result = await _userManager.CreateAsync(user, password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, role);
            }
            return result;
        }

        public async Task<ApplicationUser> GetUser(string id)
        {
            return await _userManager.FindByIdAsync(id);
        }

        public async Task<ApplicationUser> GetUserByClaim(ClaimsPrincipal user)
        {
            return await _userManager.GetUserAsync(user);
        }

        public async Task<string> GetUserIdByClaim(ClaimsPrincipal user)
        {
            return (await _userManager.GetUserAsync(user)).Id;
        }

        public async Task<ApplicationUser> GetUserByUsername(string username)
        {
            return await _userManager.FindByNameAsync(username);
        }

        public async Task<ApplicationUser> GetUserByEmail(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<ApplicationUser> GetUserByEmailOrUserName(string input)
        {
            var user = await GetUserByEmail(input);
            if (user == null)
            {
                user = await GetUserByUsername(input);
            }
            return user;
        }

        public async Task<IdentityResult> UpdateUser(ApplicationUser user)
        {
            return await _userManager.UpdateAsync(user);
        }

        public async Task<IdentityResult> UpdateUserInformation(ApplicationUser user, string email, string userName, string fullName)
        {
            user.Email = email;
            user.UserName = userName;
            user.FullName = fullName;
            return await UpdateUser(user);
        }

        public async Task<IdentityResult> UpdateUserLoginDate(ApplicationUser user)
        {
            user.LastLoginDate = DateTimeOffset.UtcNow.ToLocalTime();
            return await UpdateUser(user);
        }

        #endregion

        #region Roles

        public IEnumerable<IdentityRole> GetRoles()
        {
            return _roleManager.Roles;
        }

        public async Task<IEnumerable<string>> GetUserRoles(ApplicationUser user)
        {
            var roles = new List<string>();
            foreach (var r in GetRoles())
            {
                if (await _userManager.IsInRoleAsync(user, r.Name))
                {
                    roles.Add(r.Name);
                }
            }
            return roles;
        }

        public async Task<IdentityResult> RemoveUserFromRoles(ApplicationUser user, IEnumerable<string> roles)
        {
            return await _userManager.RemoveFromRolesAsync(user, roles);
        }

        public async Task<IdentityResult> AddUserToRoles(ApplicationUser user, IEnumerable<string> roles)
        {
            return await _userManager.AddToRolesAsync(user, roles);
        }

        public async Task<ApplicationUser> UserIsAdmin(ApplicationUser user)
        {
            var roles = await GetUserRoles(user);
            if (roles.Contains("Admin"))
            {
                return user;
            }
            return null;
        }

        public async Task<ApplicationUser> UserIsAdmin(ClaimsPrincipal principal)
        {
            var user = await GetUserByClaim(principal);
            var roles = await GetUserRoles(user);
            if (roles.Contains("Admin"))
            {
                return user;
            }
            return null;
        }

        public async Task<string> GetUserPrimaryRole(ApplicationUser user)
        {
            var roles = await GetUserRoles(user);
            if (roles.Contains("Admin"))
            {
                return "Admin";
            }
            else if (roles.Contains("Office Staff"))
            {
                return "Clients";
            }
            else
            {
                return null;
            }
        }

        public async Task<string> GetUserPrimaryRole(ClaimsPrincipal principal)
        {
            var user = await GetUserByClaim(principal);

            if (user == null)
            {
                return null;
            }

            return await GetUserPrimaryRole(user);
        }

        public async Task<IEnumerable<ApplicationUser>> GetUsersByRole(string role)
        {
            return await _userManager.GetUsersInRoleAsync(role);
        }

        #endregion

        #region Password

        private async Task<string> GetNewPasswordResetToken(ApplicationUser user)
        {
            return await _userManager.GeneratePasswordResetTokenAsync(user);
        }

        public async Task<IdentityResult> ResetPassword(ApplicationUser user, string password)
        {
            var token = await GetNewPasswordResetToken(user);
            return await _userManager.ResetPasswordAsync(user, token, password);
        }

        public async Task<IdentityResult> ResetPassword(ApplicationUser user, string token, string password)
        {
            return await _userManager.ResetPasswordAsync(user, token, password);
        }

        public async Task<IdentityResult> ChangePassword(ApplicationUser user, string oldPass, string newPass)
        {
            return await _userManager.ChangePasswordAsync(user, oldPass, newPass);
        }

        #endregion

        #region Account Recovery

        public async Task<string> GetUserEmailConfirmationTokenById(string userId)
        {
            var user = await GetUser(userId);
            return await _userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<string> GetPasswordResetTokenById(string userId)
        {
            var user = await GetUser(userId);
            return await _userManager.GeneratePasswordResetTokenAsync(user);
        }

        public async Task<IdentityResult> ConfirmEmailWithId(string userId, string token)
        {
            var user = await GetUser(userId);
            return await _userManager.ConfirmEmailAsync(user, token);
        }

        public async Task SendForgotPasswordEmail(string link, string email)
        {
            await _emailSender.SendEmailAsync(email,
                "CareConnectionApplication Password Recovery",
                "Hello, to reset the password to your account please click the following link<br>" +
                "<a href=\"" + link + "\">Click Here</a><br>" +
                "If you did not request this password recovery, please change your password immediately.");
        }

        public async Task SendConfirmationEmail(string link, string email)
        {
            await _emailSender.SendEmailAsync(email,
                "CareConnectionApplication Email Confirmation",
                "Hello, to confirm the email address on your account please click the following link<br>" +
                "<a href=\"" + link + "\">Click Here</a><br>" +
                "If you did not request this email confirmation, please change your password immediately.");
        }

        #endregion
    }
}

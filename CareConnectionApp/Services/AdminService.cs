﻿using CareConnectionApp.DataAccess.Abstractions;
using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CareConnectionApp.Services
{
    public class AdminService : IAdminService
    {
        private readonly IAdminDataAccess _adminDataAccess;
        private readonly IClientDataAccess _clientDataAccess;

        public AdminService
        (
            IAdminDataAccess adminDataAccess,
            IClientDataAccess clientDataAccess
        )
        {
            _adminDataAccess = adminDataAccess;
            _clientDataAccess = clientDataAccess;
        }

        #region WorkOrderCategories

        public List<WorkOrderCategory> GetWorkOrderCategories()
        {
            return _adminDataAccess.GetWorkOrderCategories();
        }

        public WorkOrderCategory AddWorkOrderCategory(WorkOrderCategory workOrderCategory)
        {
            return _adminDataAccess.AddWorkOrderCategory(workOrderCategory);
        }

        public WorkOrderCategory GetWorkOrderCategory(int Id)
        {
            return _adminDataAccess.GetWorkOrderCategory(Id);
        }

        public WorkOrderCategory UpdateWorkOrderCategory(WorkOrderCategory workorderChanges)
        {
            return _adminDataAccess.UpdateWorkOrderCategory(workorderChanges);
        }

        public bool DeleteWorkOrderCategory(int workOrderCategoryd)
        {
            return _adminDataAccess.DeleteWorkOrderCategory(workOrderCategoryd);
        }

        public bool WorkOrderCategoryExists(WorkOrderCategory category)
        {
            return GetWorkOrderCategories().Any(c => category.Category.Equals(c.Category)) && GetWorkOrderCategories().FirstOrDefault(woc => woc.Category.Equals(category.Category)).Id != category.Id;
        }

        public bool CategoryNameExists(WorkOrderCategory category)
        {
            return GetWorkOrderCategories().Any(c => category.Category.Equals(c.Category));
        }
        #endregion

        #region Assistive Devices

        public AssistiveDevice AddAssistiveDevice(AssistiveDevice device)
        {
            return _adminDataAccess.AddAssistiveDevice(device);
        }

        public AssistiveDevice GetAssistiveDevice(int? id)
        {
            return _adminDataAccess.GetAssistiveDevice(id);
        }

        public IEnumerable<AssistiveDevice> GetAssistiveDevices()
        {
            return _adminDataAccess.GetAssistiveDevices();
        }

        public AssistiveDevice UpdateAssistiveDevice(int id, bool isActive, string name, string description, int categoryId)
        {
            var assistiveDevice = GetAssistiveDevice(id);
            assistiveDevice.IsActive = isActive;
            assistiveDevice.Name = name;
            assistiveDevice.Description = description;
            assistiveDevice.CategoryId = categoryId;
            return _adminDataAccess.UpdateAssistiveDevice(assistiveDevice);
        }

        #endregion

        #region Assistive Device Categories

        public AssistiveDeviceCategory AddAssistiveDeviceCategory(AssistiveDeviceCategory category)
        {
            return _adminDataAccess.AddAssistiveDeviceCategory(category);
        }

        public AssistiveDeviceCategory GetAssistiveDeviceCategory(int id)
        {
            return _adminDataAccess.GetAssistiveDeviceCategory(id);
        }

        public AssistiveDeviceCategory UpdateAssistiveDeviceCategory(bool isActive, string name, string description, int categoryId)
        {
            var assistiveDeviceCategory = GetAssistiveDeviceCategory(categoryId);
            assistiveDeviceCategory.IsActive = isActive;
            assistiveDeviceCategory.Name = name;
            assistiveDeviceCategory.Description = description;
            return _adminDataAccess.UpdateAssistiveDeviceCategory(assistiveDeviceCategory);
        }

        public IEnumerable<AssistiveDeviceCategory> GetAssistiveDeviceCategories()
        {
            return _adminDataAccess.GetAssistiveDeviceCategories();
        }

        #endregion

        #region Admin Dashboard

        public Dictionary<int, int> GetNumberNewClients(string adminId) // change to use admin preferences
        {
            var adminSetting = GetAtAGlanceSetting(adminId);
            var end = DateTime.Now;
            var start = end.AddMonths(-adminSetting);

            var numClients = _clientDataAccess.GetClients()
                .Where(c => DateTime.Compare(c.CreationDate, start) > 0 && DateTime.Compare(c.CreationDate, end) < 0)
                .Count();
            return new Dictionary<int, int>
            {
                { adminSetting, numClients }
            };
        }

        public Dictionary<int, int> GetNumberJobsOpened(string adminId) // change to use admin preferences
        {
            var adminSetting = GetAtAGlanceSetting(adminId);
            var end = DateTime.Now;
            var start = end.AddMonths(-adminSetting);

            var numJobs = _clientDataAccess.GetWorkOrders()
                .Where(wo => DateTime.Compare(wo.DateOfRequest, start) > 0 && DateTime.Compare(wo.DateOfRequest, end) < 0)
                .Count();
            return new Dictionary<int, int>
            {
                { adminSetting, numJobs }
            };
        }

        public Dictionary<int, int> GetNumberJobsCompleted(string adminId) // change to use admin preferences
        {
            var adminSetting = GetAtAGlanceSetting(adminId);
            var end = DateTime.Now;
            var start = end.AddMonths(-adminSetting);

            var numJobs = _clientDataAccess.GetWorkOrders()
                .Where(wo => wo.JobStatus == JobStatus.Completed && DateTime.Compare(wo.WorkOrderCompletionForm.DateCompleted, start) > 0 && DateTime.Compare(wo.WorkOrderCompletionForm.DateCompleted, end) < 0)
                .Count();
            return new Dictionary<int, int>
            {
                { adminSetting, numJobs }
            };
        }

        public IEnumerable<WorkOrder> GetRecentWorkOrders(string userId)
        {
            var adminSetting = GetRecentRequestSetting(userId);

            return _clientDataAccess.GetWorkOrders()
                .OrderByDescending(wo => wo.DateOfRequest)
                .Where(wo => wo.JobStatus == JobStatus.Active)
                .Take(adminSetting);
        }

        public IEnumerable<WorkOrder> GetRecentAssistiveDeviceWorkOrders(string userId)
        {
            var adminSetting = GetRecentRequestSetting(userId);

            return _clientDataAccess.GetWorkOrders()
                .Where(wo => wo.WorkOrderCategory.Category == "Assistive Device")
                .OrderByDescending(wo => wo.DateOfRequest)
                .Take(adminSetting);
        }

        public Dictionary<WorkOrder, AssistiveDevice> CombineWorkOrderAssistiveDevice(IEnumerable<WorkOrder> workOrders)
        {
            var dict = new Dictionary<WorkOrder, AssistiveDevice>();
            foreach (var wo in workOrders)
            {
                var device = GetAssistiveDevice(wo.AssistiveDeviceId);
                dict.Add(wo, device);
            }
            return dict;
        }

        public IEnumerable<PendingApproval> GetPendingApprovals()
        {
            return _adminDataAccess.GetPendingApprovals().OrderBy(pa => pa.TimeStamp);

        }

        public IEnumerable<PendingApproval> GetRecentPendingApprovals(string userId)
        {
            var adminSetting = GetRecentRequestSetting(userId);

            return _adminDataAccess.GetPendingApprovals().Take(adminSetting);
        }

        #endregion

        #region Lawn Service

        public void DeleteFromPendingApprovals(PendingApproval pendingApproval)
        {
            _adminDataAccess.DeleteFromPendingApprovals(pendingApproval);
        }

        public void DeleteFromPendingApprovals(int id)
        {
            _adminDataAccess.DeleteFromPendingApprovals(GetPendingApproval(id));
        }

        public PendingApproval GetPendingApproval(int id)
        {
            return _adminDataAccess.GetPendingApprovals().FirstOrDefault(pa => pa.Id == id);
        }

        public WaitList AddToWaitList(int id)
        {
            var pendingApproval = GetPendingApproval(id);
            var waitList = new WaitList()
            {
                Client = pendingApproval.Client,
                ClientId = pendingApproval.ClientId,
                TimeStamp = DateTime.Today,
                WorkOrderId = pendingApproval.WorkOrderId
            };
            DeleteFromPendingApprovals(pendingApproval);
            return _adminDataAccess.AddToWaitList(waitList);
        }

        public bool CheckZoneStatus(int id)
        {
            return _adminDataAccess.GetZoneJoins().Where(zj => zj.ClientId == id).Any();
        }

        public WaitList GetWaitListById(int id)
        {
            return _clientDataAccess.GetWaitList().FirstOrDefault(wl => wl.Id == id);
        }

        public void DeleteFromWaitList(WaitList waitList)
        {
            _adminDataAccess.DeleteFromWaitList(waitList);
        }

        public string TooManyClientsWarning(string userId)
        {

            var adminSetting = GetServiceListLimitSetting(userId);
            if (adminSetting <= _clientDataAccess.GetLawnServiceList().Count())
            {
                return "There are " + _clientDataAccess.GetLawnServiceList().Count() + " clients on the list and a desired limit of " + adminSetting + " please consider removing a few clients or increase the limit.";
            }
            else
            {
                return null;
            }
        }

        public LawnServiceList AddToLawnServiceList(int id)
        {
            var waitList = GetWaitListById(id);
            var lawnServiceList = new LawnServiceList()
            {
                Client = waitList.Client,
                ClientId = waitList.ClientId,
                TimeStamp = DateTime.Today,
                WorkOrderId = waitList.WorkOrderId
            };
            CompleteLawnRequests(waitList.ClientId);
            _adminDataAccess.DeleteFromWaitList(waitList);
            return _adminDataAccess.AddToLawnServiceList(lawnServiceList);
        }

        public void CompleteLawnRequests(int id)
        {
            var clientRequests = _clientDataAccess.GetClientWorkOrders(id, "Lawn Service").ToList();
            var workOrders = new List<WorkOrder>();
            var formIds = AddWorkOrderCompletionForms(clientRequests.Count);
            int counter = 0;
            foreach (var cr in clientRequests)
            {
                cr.WorkOrder.JobStatus = JobStatus.Completed;
                cr.WorkOrder.WorkOrderCompletionFormId = formIds.ElementAt(counter).Id;
                workOrders.Add(cr.WorkOrder);
                counter++;
            }
            _adminDataAccess.CompleteLawnServices(workOrders);
            return;
        }


        public List<WorkOrderCompletionForm> AddWorkOrderCompletionForms(int count)
        {
            var forms = new List<WorkOrderCompletionForm>();
            for (int i = 0; i < count; i++)
            {
                var newForm = new WorkOrderCompletionForm
                {
                    DateAccepted = DateTime.Now,
                    Volunteers = "N/A",
                    DateStarted = DateTime.Now,
                    DateCompleted = DateTime.Now,
                    Comments = "N/A",
                    Expenses = 0,
                    HoursWorked = 0,
                    Mileage = 0,
                    Donation = 0,
                    JobReferral = Referral.Other
                };
                forms.Add(newForm);
            }
            _adminDataAccess.CompleteLawnServices(forms);
            return _adminDataAccess.GetCompletionForms(forms.Count);
        }

        public LawnServiceList GetLawnServiceListById(int id)
        {
            return _clientDataAccess.GetLawnServiceList().FirstOrDefault(lsl => lsl.Id == id);
        }

        public void DeleteFromServiceList(int id)
        {
            _adminDataAccess.DeleteFromServiceList(GetLawnServiceListById(id));
        }

        public IEnumerable<ClientLawnService> AddClientLawnServices(int zoneId, DateTime timeStamp)
        {
            var lsList = _clientDataAccess.GetLawnServiceList().Where(lsl => lsl.Client.ZoneJoining.ZoneId == zoneId);
            var clsList = new List<ClientLawnService>();
            foreach (LawnServiceList ls in lsList)
            {
                if (ls.TimeStamp <= timeStamp)
                {
                    var temp = new ClientLawnService()
                    {
                        ZoneId = zoneId,
                        TimeStamp = timeStamp,
                        ClientId = ls.ClientId
                    };
                    clsList.Add(temp);
                }
            }
            return _adminDataAccess.AddClientLawnServices(clsList);
        }

        public List<WaitList> AddToWaitList(IEnumerable<int> pendingApprovalIds)
        {
            var waitList = new List<WaitList>();
            var pending = new List<PendingApproval>();
            foreach (var i in pendingApprovalIds)
            {
                var temp = _adminDataAccess.GetPendingApprovals().FirstOrDefault(pa => pa.Id == i);
                pending.Add(temp);
                var newWaitList = new WaitList()
                {
                    ClientId = temp.ClientId,
                    WorkOrderId = temp.WorkOrderId,
                    TimeStamp = DateTime.Now
                };
                waitList.Add(newWaitList);
            }
            _adminDataAccess.DeleteFromPendingApprovals(pending);
            return _adminDataAccess.AddToWaitList(waitList);
        }

        public void CompleteLawnRequests(IEnumerable<int> ids)
        {
            var workOrders = new List<WorkOrder>();
            var clients = _clientDataAccess.GetWaitList().Where(wl => ids.Contains(wl.Id));
            foreach (var client in clients)
            {
                var clientRequests = _clientDataAccess.GetClientWorkOrders(client.ClientId, "Lawn Service").ToList();
                var formIds = AddWorkOrderCompletionForms(clientRequests.Count);
                int counter = 0;
                foreach (var cr in clientRequests)
                {
                    cr.WorkOrder.JobStatus = JobStatus.Completed;
                    cr.WorkOrder.WorkOrderCompletionFormId = formIds.ElementAt(counter).Id;
                    workOrders.Add(cr.WorkOrder);
                    counter++;
                }
            }
            _adminDataAccess.CompleteLawnServices(workOrders);
            return;
        }

        public List<LawnServiceList> AddToLawnServiceList(IEnumerable<int> waitListIds)
        {
            var waitList = new List<WaitList>();
            var serviceList = new List<LawnServiceList>();
            foreach (var i in waitListIds)
            {
                var temp = _clientDataAccess.GetWaitList().FirstOrDefault(wl => wl.Id == i);
                waitList.Add(temp);
                var newServiceList = new LawnServiceList()
                {
                    ClientId = temp.ClientId,
                    WorkOrderId = temp.WorkOrderId,
                    TimeStamp = DateTime.Now
                };
                serviceList.Add(newServiceList);
            }
            CompleteLawnRequests(waitListIds);
            _adminDataAccess.DeleteFromWaitList(waitList);
            return _adminDataAccess.AddToLawnServiceList(serviceList);
        }

        #endregion

        #region Lawn Service Zones

        public Zone AddZone(Zone zone)
        {
            return _adminDataAccess.AddZone(zone);
        }

        public Zone UpdateZone(Zone zone)
        {
            return _adminDataAccess.UpdateZone(zone);
        }

        public Zone GetZone(int id)
        {
            return _adminDataAccess.GetZones().FirstOrDefault(z => z.Id == id);
        }

        public IEnumerable<Zone> GetZones()
        {
            return _adminDataAccess.GetZones();
        }

        public ZoneJoining AddZoneJoin(int clientId, int zoneId)
        {
            var joins = GetZoneJoins().Where(j => j.ClientId == clientId);
            var client = _clientDataAccess.GetClient(clientId);
            if (joins.Any())
            {
                ZoneJoining zoneJoin = _adminDataAccess.GetZoneJoins().FirstOrDefault(zj => zj.ClientId == clientId);
                zoneJoin.ZoneId = zoneId;
                zoneJoin.Zone = GetZones().FirstOrDefault(z => z.Id == zoneId);
                client.ZoneJoining = zoneJoin;
                return _adminDataAccess.UpdateZoneJoin(zoneJoin);
            }
            else
            {
                ZoneJoining zoneJoining = new ZoneJoining
                {
                    ClientId = clientId,
                    ZoneId = zoneId,
                    Zone = GetZones().FirstOrDefault(z => z.Id == zoneId),
                    Client = _clientDataAccess.GetClient(clientId)
                };
                client.ZoneJoining = zoneJoining;
                return _adminDataAccess.AddZoneJoin(zoneJoining);
            }
        }

        public IEnumerable<ZoneJoining> GetZoneJoins()
        {
            return _adminDataAccess.GetZoneJoins();
        }

        public LawnService AddLawnService(int zoneId, DateTime timeStamp)
        {
            var lawnServicesWithClients = _clientDataAccess.GetLawnServiceList()
                .Where(service => service.Client.ZoneJoining.ZoneId == zoneId && service.TimeStamp < timeStamp);
            if (lawnServicesWithClients.Any())
            {
                var lawnService = new LawnService()
                {
                    ZoneId = zoneId,
                    TimeStamp = timeStamp
                };
                var ls = _adminDataAccess.AddLawnService(lawnService);
                var cls = AddClientLawnServices(zoneId, timeStamp);
                return ls;
            }
            return null;
        }

        #endregion

        #region Admin Settings

        public AdminSetting GetAdminSetting(string userId)
        {
            return _adminDataAccess.GetAdminSetting(userId);
        }

        public int GetAlertSetting(string userId)
        {
            return _adminDataAccess.GetAdminSetting(userId)?.AlertLifeSpan ?? 30;
        }

        public int GetAtAGlanceSetting(string userId)
        {
            return _adminDataAccess.GetAdminSetting(userId)?.AtAGlanceDuration ?? 4;
        }

        public int GetRecentRequestSetting(string userId)
        {
            return _adminDataAccess.GetAdminSetting(userId)?.DashboardCardHistoryCount ?? 5;
        }

        public int GetServiceListLimitSetting(string userId)
        {
            return _adminDataAccess.GetAdminSetting(userId)?.ServiceListLimit ?? 75;
        }

        public AdminSetting SetNewSettings(AdminSetting newSettings)
        {
            var updatedAlertSetting = SetAlertSetting(newSettings.ApplicationUserId, newSettings.AlertLifeSpan);
            var updatedAtAGlanceSetting = SetAtAGlanceSetting(newSettings.ApplicationUserId, newSettings.AtAGlanceDuration);
            var updatedRecentRequestsSetting = SetRecentRequestSetting(newSettings.ApplicationUserId, newSettings.DashboardCardHistoryCount);
            var updatedServiceListSetting = SetServiceListLimitSetting(newSettings.ApplicationUserId, newSettings.ServiceListLimit);

            return (updatedAlertSetting ?? updatedAtAGlanceSetting ?? updatedRecentRequestsSetting ?? updatedServiceListSetting) == null ? null : newSettings;
        }

        public AdminSetting SetAlertSetting(string userId, int duration)
        {
            return _adminDataAccess.SetAdminAlertSetting(userId, duration);
        }

        public AdminSetting SetAtAGlanceSetting(string userId, int duration)
        {
            return _adminDataAccess.SetAtAGlanceSetting(userId, duration);
        }

        public AdminSetting SetRecentRequestSetting(string userId, int count)
        {
            return _adminDataAccess.SetDashboardHistoryCount(userId, count);
        }

        public AdminSetting SetServiceListLimitSetting(string userId, int count)
        {
            return _adminDataAccess.SetServiceListLimit(userId, count);
        }

        #endregion
    }
}

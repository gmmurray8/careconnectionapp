﻿using CareConnectionApp.DataAccess.Abstractions;
using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using CareConnectionApp.Utility_Classes;
using CareConnectionApp.ViewModels.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CareConnectionApp.Services
{
    public class ClientService : IClientService
    {
        private readonly IClientDataAccess _clientDataAccess;
        private readonly IPhoneNumberClass _phoneNumberClass;
        private readonly IAlertService _alertService;
        private readonly IAdminDataAccess _adminDataAccess;

        public ClientService
        (
            IClientDataAccess clientDataAccess, 
            IPhoneNumberClass phoneNumberClass, 
            IAdminDataAccess adminDataAccess, 
            IAlertService alertService
        )
        {
            _clientDataAccess = clientDataAccess;
            _adminDataAccess = adminDataAccess;
            _phoneNumberClass = phoneNumberClass;
            _alertService = alertService;
        }

        #region Clients
        public Client GetClient(int id)
        {
            return _clientDataAccess.GetClient(id);
        }

        public IEnumerable<Client> ListActiveClients()
        {
            return _clientDataAccess.GetClients().Where(c => c.ClientStatus == ClientStatus.Active);
        }

        public IEnumerable<Client> ListClients()
        {
            return _clientDataAccess.GetClients();
        }

        public Client AddClient(Client client)
        {
            client.EmergencyContactName = StringHelperClass.CapitalizeLetters(client.EmergencyContactName);
            client.FirstName = StringHelperClass.CapitalizeLetters(client.FirstName);
            client.LastName = StringHelperClass.CapitalizeLetters(client.LastName);
            client.City = StringHelperClass.CapitalizeLetters(client.City);
            client.StreetAddress = StringHelperClass.CapitalizeLetters(client.StreetAddress);
            client.CaseManager = StringHelperClass.CapitalizeLetters(client.CaseManager);
            client.ClientStatus = ClientStatus.Active;
            client.HomePhone = _phoneNumberClass.RemoveDashes(client.HomePhone);
            client.CellPhone = _phoneNumberClass.RemoveDashes(client.CellPhone);
            client.EmergencyContactPhoneNumber = _phoneNumberClass.RemoveDashes(client.EmergencyContactPhoneNumber);
            client.CreationDate = DateTime.Now;
            return _clientDataAccess.AddClient(client);
        }

        public UserClientAdditions AddUserClientAddition(string userId, int clientId)
        {
            if (userId != null)
            {
                var model = new UserClientAdditions
                {
                    UserId = userId,
                    ClientId = clientId,
                    CreationDate = DateTime.Now
                };
                return _clientDataAccess.AddUserClientAddition(model);
            }
            return null;
        }

        public IEnumerable<UserClientAdditions> SearchUserClientAdditions(string userId, string searchString)
        {
            if (string.IsNullOrEmpty(searchString))
            {
                return GetUserClientAdditions(userId);
            }

            searchString = searchString.ToLower(); // fix case sensitivity

            var result = (from userClient in GetUserClientAdditions(userId).ToList()
                          let clientName = (userClient.Client.FirstName + userClient.Client.LastName).ToLower()
                          where clientName.Contains(searchString) ||
                          userClient.ClientId.ToString().Contains(searchString) ||
                          userClient.CreationDate.ToString("MM/dd/yyy") == searchString
                          select userClient);
            return result;
        }

        private IEnumerable<IGrouping<Client, int>> ClientSearchFunc(IEnumerable<Client> clients)
        {
            var result = from c in clients
                         group c.ClientRequests
                         .Where(cr => cr.WorkOrder.JobStatus
                         .Equals(JobStatus.Active))
                         .Count()
                         by c;

            return result;
        }

        public SearchClientViewModel SearchClients(SearchClientViewModel search)
        {

            var clients = _clientDataAccess.GetClientsNoTracking().Where(c => c.ClientStatus.Equals(search.clientStatus));
        

            if (search.ClientID != null && search.ClientID > 0)
            {
                clients = clients.Where(c => c.Id == search.ClientID);

                search.SearchClientResults = ListSorter.ClientSort(ClientSearchFunc(clients), search.SortBy, search.SortOrder);
                search.ShownResults = search.SearchClientResults.Count();
                search.TotalResults = search.ShownResults;
                return search;
            }

            if (!string.IsNullOrEmpty(search.FirstName))
            {
                clients = clients.Where(c => c.FirstName.Contains(search.FirstName));
            }
            if (!string.IsNullOrEmpty(search.LastName))
            {
                clients = clients.Where(c => c.LastName.Contains(search.LastName));
            }
            if (!string.IsNullOrEmpty(search.StreetAddress))
            {
                clients = clients.Where(c => c.StreetAddress.Contains(search.StreetAddress));
            }
            if (!string.IsNullOrEmpty(search.City))
            {
                clients = clients.Where(c => c.City.Contains(search.City));
            }
            if (search.Zip != 0 && search.Zip != null)
            {
                clients = clients.Where(c => c.Zip == search.Zip);
            }
            if (!string.IsNullOrEmpty(search.HomePhone))
            {
                clients = clients.Where(c => c.HomePhone.Contains(_phoneNumberClass.RemoveDashes(search.HomePhone)));
            }

            var clientResult = LazyLoader.ClientLazyLoad(clients, search.PageNum, search.ResultsPerPage, out int totalResults, out int shownResults);
            search.ShownResults = shownResults;
            search.TotalResults = totalResults;

            search.SearchClientResults = ListSorter.ClientSort(ClientSearchFunc(clientResult), search.SortBy, search.SortOrder);
            return search;

        }

        public Client UpdateClient(Client client)
        {
            client.EmergencyContactName = StringHelperClass.CapitalizeLetters(client.EmergencyContactName);
            client.FirstName = StringHelperClass.CapitalizeLetters(client.FirstName);
            client.LastName = StringHelperClass.CapitalizeLetters(client.LastName);
            client.City = StringHelperClass.CapitalizeLetters(client.City);
            client.StreetAddress = StringHelperClass.CapitalizeLetters(client.StreetAddress);
            client.CaseManager = StringHelperClass.CapitalizeLetters(client.CaseManager);
            client.HomePhone = _phoneNumberClass.RemoveDashes(client.HomePhone);
            client.CellPhone = _phoneNumberClass.RemoveDashes(client.CellPhone);
            client.EmergencyContactPhoneNumber = _phoneNumberClass.RemoveDashes(client.EmergencyContactPhoneNumber);
            return _clientDataAccess.UpdateClient(client);
        }

        #endregion

        #region WorkOrders

        public async Task<WorkOrder> CreateWorkOrder(int clientId, string type, string description, string secondaryContact, string secondaryContactNumber)
        {
            var categoryId = _clientDataAccess.GetWorkOrderCategoryId(type);
            var client = _clientDataAccess.GetClient(clientId);
            var workOrder = new WorkOrder
            {
                WorkOrderCategoryId = categoryId,
                ReqDescription = description,
                DateOfRequest = DateTime.Today,
                JobStatus = JobStatus.Active,
                SecondaryContact = secondaryContact,
                SecondaryContactNumber = _phoneNumberClass.RemoveDashes(secondaryContactNumber)
            };
            var result = _clientDataAccess.AddWorkOrder(workOrder, clientId);
            var category = _clientDataAccess.GetWorkOrderCategory(categoryId)?.Category; // Get category name for alert
            await _alertService.AddClientAlert($"{client.FirstName} {client.LastName} requested a new {category} (ID {workOrder.Id})", client.Id, workOrder.Id);

            return result;
        }

        public WorkOrder AddWorkOrder(WorkOrder workOrder, int clientId)
        {
            return _clientDataAccess.AddWorkOrder(workOrder, clientId);
        }

        public WorkOrder GetWorkOrderWithClient(int id)
        {
            return _clientDataAccess.GetWorkOrderWithClient(id);
        }

        public WorkOrder CompleteWorkOrder(int id, JobStatus status, string secondaryContact,
                string secondaryContactNumber, WorkOrderCompletionForm form, string reqDescription, int? deviceId)
        {
            var workOrder = GetWorkOrderWithClient(id);

            workOrder.JobStatus = status;
            workOrder.SecondaryContact = secondaryContact;
            workOrder.SecondaryContactNumber = _phoneNumberClass.RemoveDashes(secondaryContactNumber);
            workOrder.ReqDescription = reqDescription;
            workOrder.AssistiveDeviceId = deviceId == 0 ? null : deviceId;

            var newCompleteForm = AddWorkOrderCompletionForm(form);
            workOrder.WorkOrderCompletionFormId = newCompleteForm.Id;

            return _clientDataAccess.UpdateWorkOrder(workOrder);

        }

        public async Task<WorkOrder> UpdateWorkOrder(string category, int id, JobStatus status, DateTime dateofRequest, string secondaryContact,
            string secondaryContactNumber, WorkOrderCompletionForm form, string reqDescription, int? deviceId)
        {
            var workOrder = GetWorkOrderWithClient(id);

            WorkOrderCompletionForm workOrderCompletionForm = new WorkOrderCompletionForm();

            if(form != null)
            {
                workOrderCompletionForm = _clientDataAccess.GetWorkOrderCompletionForm(form.Id);
            }

            workOrder.JobStatus = status;
            workOrder.DateOfRequest = dateofRequest;
            workOrder.SecondaryContact = secondaryContact;
            workOrder.SecondaryContactNumber = _phoneNumberClass.RemoveDashes(secondaryContactNumber);
            workOrder.ReqDescription = reqDescription;
            workOrder.AssistiveDeviceId = deviceId == 0 ? null : deviceId;
            if (!workOrder.WorkOrderCategory.Category.Equals("Lawn Service") && !workOrder.WorkOrderCategory.Category.Equals("Ramp") &&
                !workOrder.WorkOrderCategory.Category.Equals("Assistive Device"))
            {
                workOrder.WorkOrderCategory = _clientDataAccess.GetWorkOrderCategories().Where(cat => cat.Category == category).FirstOrDefault();
            }

            if (workOrder.WorkOrderCompletionFormId != null)
            {
                workOrderCompletionForm.Comments = form.Comments;
                workOrderCompletionForm.DateAccepted = form.DateAccepted;
                workOrderCompletionForm.DateCompleted = form.DateCompleted;
                workOrderCompletionForm.DateStarted = form.DateStarted;
                workOrderCompletionForm.Donation = form.Donation;
                workOrderCompletionForm.Expenses = form.Expenses;
                workOrderCompletionForm.HoursWorked = form.HoursWorked;
                workOrderCompletionForm.JobReferral = form.JobReferral;
                workOrderCompletionForm.Mileage = form.Mileage;
                workOrderCompletionForm.Volunteers = form.Volunteers;

                UpdateWorkOrderCompletionForm(workOrderCompletionForm);
            }

            await _alertService.AddClientAlert($"{workOrder.ClientRequest.Client.FirstName} {workOrder.ClientRequest.Client.LastName} (ID {workOrder.ClientRequest.ClientId}) has had their workorder updated.", workOrder.ClientRequest.ClientId, workOrder.Id);
            return _clientDataAccess.UpdateWorkOrder(workOrder);
        }

        public WorkOrderCompletionForm GetWorkOrderCompletionForm(int id)
        {
            return _clientDataAccess.GetWorkOrderCompletionForm(id);
        }

        public WorkOrderCompletionForm UpdateWorkOrderCompletionForm(WorkOrderCompletionForm form)
        {
            return _clientDataAccess.UpdateWorkOrderCompletionForm(form);
        }

        public WorkOrderCompletionForm AddWorkOrderCompletionForm(WorkOrderCompletionForm form)
        {
            return _clientDataAccess.AddWorkOrderCompletionForm(form);
        }

        public IQueryable<WorkOrder> FilterWorkOrders(string type, JobStatus status)
        {
            return _clientDataAccess.SearchWorkOrders(type, status);
        }

        #endregion

        #region Ramp Requests

        public RampRequest GetRampRequest(int? id)
        {
            return _clientDataAccess.GetRampRequest(id);
        }

        public RampRequest AddRampRequest(RampRequest rampRequest)
        {
            return _clientDataAccess.AddRampRequest(rampRequest);
        }

        public RampRequest CreateRampRequest(RampRequest rampRequest, WorkOrder workOrder, int clientId)
        {
            rampRequest = _clientDataAccess.AddRampRequest(rampRequest);
            workOrder.RampRequestId = rampRequest.Id;
            workOrder.JobStatus = JobStatus.Active;
            workOrder.DateOfRequest = DateTime.Today;
            AddWorkOrder(workOrder, clientId);
            return rampRequest;
        }

        public RampRequest UpdateRampRequest(int id, bool mobilityImpairment, MobilityAssistance mobilityAssistance, string mobilityDescription, bool VA, bool homeOwner, string lengthOfResidence, bool liveAlone)
        {
            var ramp = GetRampRequest(id);
            ramp.PermanentMobilityImpairment = mobilityImpairment;
            ramp.MobilityAssistance = mobilityAssistance;
            ramp.MobilityDescription = mobilityDescription;
            ramp.ContactedVA = VA;
            ramp.OwnerOfHome = homeOwner;
            ramp.LengthOfHomeOwnership = lengthOfResidence;
            ramp.LiveAlone = liveAlone;

            return _clientDataAccess.UpdateRampRequest(ramp);
        }

        #endregion

        #region EHeaps
        public Eheap GetEheap(int? id)
        {
            return _clientDataAccess.GetEheap(id);
        }

        public Eheap AddEheap(Eheap eheap)
        {
            return _clientDataAccess.AddEheap(eheap);
        }

        public Eheap CreateEheap(DateTime dateOfService, int clientId, decimal expenses)
        {
            var eheap = new Eheap
            {
                DateOfService = dateOfService,
                Expenses = expenses
            };
            AddEheap(eheap);
            var client = GetClient(clientId);
            if (client == null)
            {
                return null;
            }
            client.EheapId = eheap.Id;
            UpdateClient(client);
            return eheap;
        }

        public Eheap UpdateEheap(int clientId, DateTime dateOfService, decimal expenses)
        {
            var client = _clientDataAccess.GetClient(clientId);
            var eheap = GetEheap(client.EheapId);
            eheap.DateOfService = dateOfService;
            eheap.Expenses = expenses;

            return _clientDataAccess.UpdateEheap(eheap);
        }

        #endregion

        #region Client Requests

        public IEnumerable<ClientRequest> GetClientRequests(int clientId)
        {
            return _clientDataAccess.GetClientWorkOrders(clientId);
        }

        public IEnumerable<ClientRequest> FilterClientRequests(Client client, string type, JobStatus status)
        {
            return _clientDataAccess.GetClientWorkOrders(client.Id, type, status);
        }

        #endregion

        #region Admin Client Watch List

        public AdminFaveClient AddFavorite(int clientId, string adminId)
        {
            var adminFaveClient = new AdminFaveClient
            {
                ClientId = clientId,
                AdminId = adminId
            };

            return _clientDataAccess.AddAdminFaveClient(adminFaveClient);
        }

        public AdminFaveClient SearchAdminFave(int clientId, string adminId)
        {
            return _clientDataAccess.SearchAdminFave(clientId, adminId);
        }

        public AdminFaveClient RemoveFavorite(int clientId, string adminId)
        {
            return _clientDataAccess.RemoveAdminFave(clientId, adminId);
        }

        public List<Client> GetAdminFaveClients(string adminId)
        {
            return _clientDataAccess.GetAdminFaveClients(adminId);
        }
        public List<int> GetAdminFaveIDs(string adminId)
        {
            return _clientDataAccess.GetAdminFaveIDs(adminId);
        }

        #endregion

        #region Assistive Devices

        public AssistiveDevice CreateAssistiveDevice(AssistiveDevice assistiveDevice, WorkOrder workOrder, int clientId)
        {
            workOrder.AssistiveDeviceId = assistiveDevice.Id;
            workOrder.JobStatus = JobStatus.Active;
            workOrder.DateOfRequest = DateTime.Today;
            AddWorkOrder(workOrder, clientId);
            return assistiveDevice;
        }

        public AssistiveDevice GetAssistiveDevice(int? id)
        {
            return _clientDataAccess.GetAssistiveDevice(id);
        }

        public IEnumerable<AssistiveDeviceCategory> GetActiveAssistiveDeviceCategories()
        {
            return _clientDataAccess.GetActiveAssistiveDeviceCategories();
        }

        public IEnumerable<AssistiveDevice> GetAssistiveDevices(int CategoryId)
        {
            return _clientDataAccess.GetAssistiveDevices(CategoryId);
        }
        public IEnumerable<AssistiveDevice> GetActiveAssistiveDevices()
        {
            return _clientDataAccess.GetActiveAssistiveDevices();
        }
        public List<AssistiveDevice> GetActiveAssistiveDevices(int CategoryId)
        {
            return _clientDataAccess.GetActiveAssistiveDevices(CategoryId).ToList();
        }

        #endregion

        #region Work Order Categories

        public List<WorkOrderCategory> GetWorkOrderCategories()
        {
            return _clientDataAccess.GetWorkOrderCategories();
        }

        public List<string> GetWorkOrderCategoriesNames()
        {
            var categories = GetWorkOrderCategories();
            var categoryNames = new List<string>();
            foreach (var cat in categories)
            {
                categoryNames.Add(cat.Category);
            }

            return categoryNames;
        }

        public List<string> GetworkOrderCategoryNamesForCreation()
        {
            var categories = GetWorkOrderCategories().Where(c => c.IsActive == true);
            var categoryNames = new List<string>();
            foreach (var cat in categories)
            {
                categoryNames.Add(cat.Category);
            }

            return categoryNames;
        }

        public List<string> GetWorkOrderCategoryNamesForSearching()
        {
            var categoryNames = GetWorkOrderCategoriesNames();
            categoryNames = categoryNames.OrderBy(c => c).ToList();
            categoryNames.Insert(0, "Any");
            return categoryNames;
        }

        public int GetWorkOrderCategoryId(string cat)
        {
            return _clientDataAccess.GetWorkOrderCategoryId(cat);
        }

        public WorkOrderCategory GetWorkOrderCategory(int id)
        {
            return _clientDataAccess.GetWorkOrderCategory(id);
        }

        #endregion

        #region Lawn Service

        public PendingApproval AddToPendingList(int clientId, int workOrderId)
        {

            if (!_clientDataAccess.GetPendingApproval().Where(pa => pa.ClientId == clientId).Any()
                && !_clientDataAccess.GetWaitList().Where(pa => pa.ClientId == clientId).Any()
                && !_clientDataAccess.GetLawnServiceList().Where(pa => pa.ClientId == clientId).Any())
            {
                var pendingApproval = new PendingApproval()
                {
                    ClientId = clientId,
                    Client = GetClient(clientId),
                    TimeStamp = DateTime.Today,
                    WorkOrderId = workOrderId
                };
                return _clientDataAccess.AddToPendingList(pendingApproval);
            }

            return null;
        }

        public string ListClientIsOn(int id)
        {
            if (_clientDataAccess.GetPendingApproval().Where(pa => pa.ClientId == id).Any())
            {
                return "Pending Approval";
            }
            else if (_clientDataAccess.GetWaitList().Where(pa => pa.ClientId == id).Any())
            {
                return "Wait List";
            }
            else if (_clientDataAccess.GetLawnServiceList().Where(pa => pa.ClientId == id).Any())
            {
                return "Service List";
            }
            else
            {
                return "Not on a list";
            }
        }

        public Dictionary<LawnServiceList, string> GetLawnServiceList()
        {
            var keyValues = new Dictionary<LawnServiceList, string>();
            var lsl = _clientDataAccess.GetLawnServiceList();
            var clientServices = _clientDataAccess.GetClientLawnServices();
            foreach (var l in lsl)
            {
                if (clientServices.Where(c => c.ClientId == l.ClientId).Any())
                {
                    keyValues.Add(l, clientServices.Where(c => c.ClientId == l.ClientId).OrderByDescending(ls => ls.TimeStamp).First().TimeStamp.ToString("MM/dd/yyyy"));
                }
                else
                {
                    keyValues.Add(l, "--");
                }
            }
            return keyValues;
        }

        public Dictionary<LawnServiceList, string> GetLawnServiceList(int id)
        {
            var keyValues = new Dictionary<LawnServiceList, string>();
            var clientServices = _clientDataAccess.GetClientLawnServices();
            var lsl = _clientDataAccess.GetLawnServiceList().Where(ls => ls.Client.ZoneJoining.ZoneId == id)
                .OrderBy(ls => ls.Client.ZoneJoining.ZoneId);
            foreach (var l in lsl)
            {
                if (clientServices.Where(c => c.ClientId == l.ClientId).Any())
                {
                    keyValues.Add(l, clientServices.Where(c => c.ClientId == l.ClientId).OrderByDescending(ls => ls.TimeStamp).First().TimeStamp.ToString("MM/dd/yyyy"));
                }
                else
                {
                    keyValues.Add(l, "--");
                }
            }
            return keyValues;
        }

        public IEnumerable<LawnServiceList> GetLawnServiceListForPrinting(int id)
        {
            return _clientDataAccess.GetLawnServiceList().Where(ls => ls.Client.ZoneJoining.ZoneId == id);
        }

        public int DaysSinceLastService(int id)
        {
            if (_clientDataAccess.GetLawnServices().Where(ls => ls.ZoneId == id).Any())
            {
                var serviceList = _clientDataAccess.GetLawnServices().Where(ls => ls.ZoneId == id).OrderBy(ls => ls.TimeStamp).Last();
                int days = (DateTime.Now - serviceList.TimeStamp).Days;
                return days;
            }
            else
            {
                return -1;
            }
        }
        public IEnumerable<WaitList> GetWaitList()
        {
            return _clientDataAccess.GetWaitList();
        }

        public IEnumerable<Zone> GetZones()
        {
            return _clientDataAccess.GetZones();
        }

        public Zone GetZoneById(int id)
        {
            return _clientDataAccess.GetZones().FirstOrDefault(z => z.Id == id);
        }

        public IEnumerable<WaitList> GetWaitListByZone(int id)
        {
            return _clientDataAccess.GetWaitList().Where(wl => wl.Client.ZoneJoining.ZoneId == id);
        }

        #endregion

        public IEnumerable<UserClientAdditions> GetUserClientAdditions(string userId)
        {
            return _clientDataAccess.GetUserClientAdditions(userId);
        }
    }
}
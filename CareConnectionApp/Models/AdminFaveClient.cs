﻿namespace CareConnectionApp.Models
{
    public class AdminFaveClient
    {
        public string AdminId { get; set; }
        public int ClientId { get; set; }
    }
}

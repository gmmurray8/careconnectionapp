﻿namespace CareConnectionApp.Models
{
    public class ZoneJoining
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int ZoneId { get; set; }
        public Zone Zone { get; set; }
        public Client Client { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.Models
{
    public class AssistiveDeviceCategory
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter a category name.")]
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public ICollection<AssistiveDevice> AssistiveDevices { get; set; }
    }
}

﻿namespace CareConnectionApp.Models
{
    public class ClientRequest
    {
        public int ClientId { get; set; }
        public int WorkOrderId { get; set; }
        public Client Client { get; set; }
        public WorkOrder WorkOrder { get; set; }
    }
}

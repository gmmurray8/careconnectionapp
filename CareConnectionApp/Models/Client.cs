﻿using CareConnectionApp.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.Models
{
    public class Client
    {
        public int Id { get; set; }
        [Required (ErrorMessage = "Please enter a first name.")]
        public string FirstName { get; set; }
        [Required (ErrorMessage = "Please enter a last name.")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Please enter a birth date.")]
        [DataType(DataType.Date)]
        public DateTime BirthDay { get; set; }
        public bool Veteran { get; set; }
        [Required(ErrorMessage = "Please enter a street address.")]
        public string StreetAddress { get; set; }
        [Required(ErrorMessage = "Please enter a city.")]
        public string City { get; set; }
        [Required(ErrorMessage = "Please enter a zip code.")]
        [RegularExpression(@"(^\d{5}(-\d{4})?$)", ErrorMessage ="Invalid Zip Code.")]
        public int Zip { get; set; }
        [Required(ErrorMessage = "Please enter a home phone number.")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Invalid Phone Number")]
        public string HomePhone { get; set; }
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Invalid Phone Number")]
        public string CellPhone { get; set; } = "Only LandLine";
        public bool OwnerOfHome { get; set; }
        public string HasPets { get; set; }
        public ClientStatus ClientStatus { get;set; }
        public DateTime CreationDate { get; set; }
        public ICollection<ClientRequest> ClientRequests { get; set; }

        public int? EheapId { get; set; }

        public Eheap Eheap { get; set; }

        public string EmergencyContactName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Invalid Phone Number")]
        public string EmergencyContactPhoneNumber { get; set; }

        public string EmergencyContactRelationship { get; set; }
        public ZoneJoining ZoneJoining { get; set; }
        public string CaseManager { get; set; }
    }
}

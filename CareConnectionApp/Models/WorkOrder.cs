﻿using CareConnectionApp.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.Models
{
    public class WorkOrder
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Type Of Order")]
        public int WorkOrderCategoryId { get; set; }
        public WorkOrderCategory WorkOrderCategory { get; set; }

        [Required(ErrorMessage = "Please provide a description for the type of order the client requested")]
        [Display(Name = "Request Description")]
        public string ReqDescription { get; set; }

        [Required(ErrorMessage = "Please provide the start date of the corresponding work-order.")]
        [DataType(DataType.Date)]
        public DateTime DateOfRequest { get; set; }

        [Required]
        public JobStatus JobStatus { get; set; }
        public int? RampRequestId { get; set; }
        public RampRequest RampRequest { get; set; }
        public int? WorkOrderCompletionFormId { get; set; }
        public WorkOrderCompletionForm WorkOrderCompletionForm { get; set; }
        public ClientRequest ClientRequest { get; set; }

        [Display(Name = "Secondary Contact")]
        public string SecondaryContact { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Invalid Phone Number")]
        [Display(Name = "Secondary Contact Phone Number")]
        public string SecondaryContactNumber { get; set; }
        public int? AssistiveDeviceId { get; set; }
    }
}

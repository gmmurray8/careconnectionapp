﻿using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.Models
{
    public class AdminSetting
    {
        [Key]
        public string ApplicationUserId { get; set; }
        public int AlertLifeSpan { get; set; }
        public int AtAGlanceDuration { get; set; }
        public int DashboardCardHistoryCount { get; set; }
        public int ServiceListLimit { get; set; }
    }
}

﻿using CareConnectionApp.Enums;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.Models
{
    public class RampRequest
    {
        public int Id { get; set; }
        public bool PermanentMobilityImpairment { get; set; }
        public MobilityAssistance MobilityAssistance { get; set; }
        [Required(ErrorMessage = "Please provide a short description of client's mobility.")]
        public string MobilityDescription { get; set; }
        public bool ContactedVA { get; set; }
        public bool OwnerOfHome { get; set; }
        [Required(ErrorMessage = "Please provide a length of home residency.")]
        public string LengthOfHomeOwnership { get; set; }
        public bool LiveAlone { get; set; }      
    }
}

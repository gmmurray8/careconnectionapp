﻿using System;

namespace CareConnectionApp.Models
{
    public class Alert
    {
        public int Id { get; set; }
        public string AdminId { get; set; }
        public string AlertText { get; set; }
        public DateTime TimeStamp { get; set; }
        public int? ClientId { get; set; }
        public int? WorkOrderId { get; set; }
        public int? AssistiveDeviceId { get; set; }
        public bool Read { get; set; } = false;
    }
}

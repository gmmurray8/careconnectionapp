﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.Models
{
    public class Zone
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a name for the zone")]
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<LawnService> LawnService { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.Models
{
    public class WorkOrderCategory
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a category name")]
        public string Category { get; set; }
        public bool IsActive { get; set; } = true;
        public bool IsBaseCategory { get; set; }
    }
}

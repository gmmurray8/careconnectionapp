﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareConnectionApp.Models
{
    public class Eheap
    {
        public int Id { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateOfService { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18,4)")]
        public decimal Expenses { get; set; }
    }
}

﻿using System;

namespace CareConnectionApp.Models
{
    public class UserClientAdditions
    {
        public string UserId { get; set; }
        public int ClientId { get; set; }
        
        public DateTime CreationDate { get; set; }

        public ApplicationUser User { get; set; }
        public Client Client { get; set; }
    }
}

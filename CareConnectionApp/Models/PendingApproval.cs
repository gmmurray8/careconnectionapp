﻿using System;

namespace CareConnectionApp.Models
{
    public class PendingApproval
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int WorkOrderId { get; set; }
        public DateTime TimeStamp { get; set; }
        public Client Client { get; set; }
        public WorkOrder WorkOrder { get; set; }

    }
}

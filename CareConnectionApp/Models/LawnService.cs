﻿using System;

namespace CareConnectionApp.Models
{
    public class LawnService
    {
        public int Id { get; set; }
        public int ZoneId { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}

﻿using System;

namespace CareConnectionApp.Models
{
    public class ClientLawnService
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int ZoneId { get; set; }
        public DateTime TimeStamp { get; set; }
        public Client Client { get; set; }
    }
}

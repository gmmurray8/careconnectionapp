﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CareConnectionApp.Models
{
    public class AssistiveDevice
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [ForeignKey("AssitiveDeviceCategory")]
        public int CategoryId { get; set; }
        public bool IsActive { get; set; }
        public AssistiveDeviceCategory Category { get; set; }
    }
}

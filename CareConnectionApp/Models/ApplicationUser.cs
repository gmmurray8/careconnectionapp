﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Required, Display(Name = "Full Name")]
        public string FullName { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public DateTimeOffset LastLoginDate { get; set; }
    }
}

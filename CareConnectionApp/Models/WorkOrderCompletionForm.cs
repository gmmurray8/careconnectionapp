﻿using CareConnectionApp.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CareConnectionApp.Models
{
    public class WorkOrderCompletionForm
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter an accepted date.")]
        [DataType(DataType.Date)]
        public DateTime DateAccepted { get; set; }
        public string Volunteers { get; set; }

        [Required(ErrorMessage = "Please enter a start date.")]
        [Display(Name = "Date Started")]
        [DataType(DataType.Date)]
        public DateTime DateStarted { get; set; }

        [Required(ErrorMessage = "Please enter a completed date.")]
        [Display(Name = "Date Completed")]
        [DataType(DataType.Date)]
        public DateTime DateCompleted { get; set; }
        public string Comments { get; set; }

        [Required(ErrorMessage = "Please enter the amount of expenses.")]
        [Column(TypeName = "decimal(18,4)")]
        public decimal Expenses { get; set; }

        [Required(ErrorMessage = "Please enter number of hours worked.")]
        [Display(Name = "Hours Worked")]
        public float HoursWorked { get; set; }

        [Required(ErrorMessage = "Please enter the mileage.")]
        public int Mileage { get; set; }

        [Required(ErrorMessage = "Please enter number the donation amount.")]
        [Column(TypeName = "decimal(18,4)")]
        public decimal Donation { get; set; }
        public Referral JobReferral { get; set; }
    }
}

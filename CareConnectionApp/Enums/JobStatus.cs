﻿namespace CareConnectionApp.Enums
{
    public enum JobStatus
    {
        Active,
        Inactive,
        Completed,
        Unfulfilled
    }
}

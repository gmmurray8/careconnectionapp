﻿namespace CareConnectionApp.Enums
{
    public enum MobilityAssistance
    {
        Cane,
        Walker,
        Wheelchair
    }
}

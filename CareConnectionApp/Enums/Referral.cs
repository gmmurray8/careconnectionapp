﻿namespace CareConnectionApp.Enums
{
    public enum Referral
    {
        UbanPlunge,
        CareFest,
        SJHP,
        Other
    }
}

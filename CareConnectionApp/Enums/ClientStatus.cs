﻿namespace CareConnectionApp.Enums
{
    public enum ClientStatus
    {
        Active,
        Inactive,
        Deceased
    }
}

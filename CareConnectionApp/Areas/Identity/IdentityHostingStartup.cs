﻿using System;
using CareConnectionApp.DataAccess;
using CareConnectionApp.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(CareConnectionApp.Areas.Identity.IdentityHostingStartup))]
namespace CareConnectionApp.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<CCADbContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("CCADBConnection")));

                services.AddIdentity<ApplicationUser, IdentityRole>(options => options.Stores.MaxLengthForKeys = 128)
                    .AddEntityFrameworkStores<CCADbContext>()
                    .AddDefaultTokenProviders()
                    .AddRoles<IdentityRole>();

                services.ConfigureApplicationCookie(options =>
                {
                    options.Cookie.Name = "CareConnectionApp";
                    options.AccessDeniedPath = "/Account/AccessDenied";
                    options.LoginPath = "/Account/Login";
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                });
            });
        }
    }
}
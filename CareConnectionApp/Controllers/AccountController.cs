﻿using System.Threading.Tasks;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using CareConnectionApp.ViewModels.Account;
using CareConnectionApp.ViewModels.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CareConnectionApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IIdentityService _identityService;
        private readonly IAlertService _alertService;
        private readonly IAdminService _adminService;

        public AccountController
        (
            SignInManager<ApplicationUser> signInManager,
            IIdentityService identityService,
            IAlertService alertService,
            IAdminService adminService
        )
        {
            _signInManager = signInManager;
            _identityService = identityService;
            _alertService = alertService;
            _adminService = adminService;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Login");
        }

        #region Login/Logout
        [HttpGet]
        public async Task<IActionResult> Login()
        {
            if (_signInManager.IsSignedIn(User))
            {
                var role = await _identityService.GetUserPrimaryRole(User);
                if (role == null)
                {
                    ModelState.AddModelError(string.Empty, "Invalid/Inactive User Account");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index", role);
                }
            }
            return View();
        }

        // Login to identity using LoginViewModel with local redirects
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl)
        {
            // Login only if model is valid
            if (ModelState.IsValid)
            {
                // Check login credentials
                var result = await _signInManager.PasswordSignInAsync(
                    model.Username, model.Password, false, false);
                // If login successful
                if (result.Succeeded)
                {
                    var user = await _identityService.GetUserByUsername(model.Username);
                    if (user == null)
                    {
                        return NotFound("Unable to load user to update last login.");
                    }

                    // Redirect to local page if return URL is included
                    if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                    {
                        return LocalRedirect(returnUrl);
                    }

                    // If no return URL, redirect user to home page based on role
                    else
                    {
                        var role = await _identityService.GetUserPrimaryRole(user);
                        if (role == null)
                        {
                            ModelState.AddModelError(string.Empty, "Invalid Login Attempt");
                            return View();
                        }
                        else
                        {
                            await _identityService.UpdateUserLoginDate(user);
                            return RedirectToAction("Index", role);
                        }
                    }
                }
                ModelState.AddModelError(string.Empty, "Invalid Login Attempt");
            }
            return View(model);
        }

        // Logout of identity, returns confirmation page
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return RedirectToAction("Confirmation", ConfirmationViewModel.Initialize(null, "You have successfully logged out.", null));
        }
        #endregion

        #region AccountManagement
        [Authorize]
        public async Task<IActionResult> ManageAccount()
        {
            var user = await _identityService.GetUserByClaim(User);
            if (user == null)
            {
                return RedirectToAction("NotFound", new NotFoundViewModel { Message = "User cannot be found"});
            }

            return View(user);
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> UpdateAccount()
        {
            var user = await _identityService.GetUserByClaim(User);

            if (user == null)
            {
                return RedirectToAction("NotFound", new NotFoundViewModel { Message = "User cannot be found" });
            }

            var viewModel = UpdateAccountViewModel.Initialize(user);

            return View(viewModel);
        }

        [HttpPost, Authorize]
        public async Task<IActionResult> UpdateAccount(UpdateAccountViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var user = await _identityService.GetUser(viewModel.Id);

                if (user == null)
                {
                    return RedirectToAction("NotFound", new NotFoundViewModel { Message = $"User with ID = {viewModel.Id} cannot be found" });
                }
                var result = await _identityService.UpdateUserInformation(user, viewModel.Email, viewModel.UserName, viewModel.FullName);

                if (result.Succeeded)
                {
                    return RedirectToAction("Confirmation", ConfirmationViewModel.Initialize(user.Id, "User information successfully updated.", "Account"));
                }
            }
            return View(viewModel);
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> ChangePassword()
        {
            var user = await _identityService.GetUserByClaim(User);

            if (user == null)
            {
                return View("NotFound", new NotFoundViewModel { Message = "User cannot be found" });
            }

            ChangePasswordViewModel viewModel = new ChangePasswordViewModel
            {
                Id = user.Id
            };

            return View(viewModel);
        }

        [HttpPost, Authorize]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var user = await _identityService.GetUser(viewModel.Id);

                if (user == null)
                {
                    return View("NotFound", new NotFoundViewModel { Message = $"User with ID = {viewModel.Id} cannot be found" });
                }
                var result = await _identityService.ChangePassword(user, viewModel.OldPassword, viewModel.NewPassword);

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", "Could not change password");
                    return View(viewModel);
                }

                return RedirectToAction("Confirmation", ConfirmationViewModel.Initialize(user.Id, "Password successfully updated.", "Account"));
            }

            return View(viewModel);
        }

        [Authorize]
        public async Task<IActionResult> EmailConfirmation(string userId)
        {
            var user = await _identityService.GetUser(userId);

            var token = await _identityService.GetUserEmailConfirmationTokenById(userId);
            var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId, token }, protocol: Request.Scheme);
            await _identityService.SendConfirmationEmail(callbackUrl, user.Email);

            return RedirectToAction("Confirmation", ConfirmationViewModel.Initialize(userId, "Email confirmation link sent.", "Account"));
        }
        [Authorize]
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            var result = await _identityService.ConfirmEmailWithId(userId, token);
            if (result.Succeeded)
            {
                return RedirectToAction("Confirmation", ConfirmationViewModel.Initialize(userId, "Successfully confirmed email address.", "Account"));
            }
            else
            {
                return RedirectToAction("Confirmation", ConfirmationViewModel.Initialize(userId, "Error confirmation email.", "Account"));
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> AdminSettings()
        {
            var user = await _identityService.GetUserByClaim(User);
            var viewModel = AdminSettingsViewModel.Initialize(_adminService, user.Id);
            return View(viewModel);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult AdminSettings(AdminSettingsViewModel settingsViewModel)
        {
            if (ModelState.IsValid)
            {
                var updatedSettings = _adminService.SetNewSettings(AdminSettingsViewModel.GetAdminSetting(settingsViewModel));
                if (updatedSettings == null )
                {
                    return RedirectToAction("Confirmation", new ConfirmationViewModel
                    {
                        Id = settingsViewModel.UserId,
                        Message = "Error updating settings.",
                        Sender = "Account"
                    });
                }
                else
                {
                    ViewData["Success"] = true;
                    return View(settingsViewModel);
                }
            }
            return View(settingsViewModel);
        }
        #endregion

        #region PasswordRecovery

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(string email)
        {
            var user = await _identityService.GetUserByEmail(email);
            if (user != null && user.EmailConfirmed)
            {
                var token = await _identityService.GetPasswordResetTokenById(user.Id);
                var resetLink = Url.Action("ResetPassword", "Account", new { email, token }, Request.Scheme);
                await _identityService.SendForgotPasswordEmail(resetLink, email);
            }
            return RedirectToAction("Confirmation", ConfirmationViewModel.Initialize(null, "Password recovery link sent.", "Account"));
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string email, string token)
        {
            if (token == null || email == null)
            {
                ModelState.AddModelError("", "Invalid password reset token");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Find the user by email
                var user = await _identityService.GetUserByEmail(model.Email);

                if (user != null)
                {
                    // reset the user password
                    var result = await _identityService.ResetPassword(user, model.Token, model.Password);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Confirmation", ConfirmationViewModel.Initialize(user.Id, "Password reset.", "Account"));
                    }
                    // Display validation errors. For example, password reset token already
                    // used to change the password or password complexity rules not met
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                    return View(model);
                }

                // To avoid account enumeration and brute force attacks, don't
                // reveal that the user does not exist
                return RedirectToAction("Confirmation", ConfirmationViewModel.Initialize(null, "Password reset", "Account"));
            }
            // Display validation errors if model state is not valid
            return View(model);
        }
        #endregion

        public IActionResult AccessDenied()
        {
            if (_signInManager.IsSignedIn(User))
            {
                return View();
            }
            return LocalRedirect("Login");

        }

        public IActionResult Confirmation(ConfirmationViewModel viewModel)
        {
            return View(viewModel);
        }

        public IActionResult NotFound(NotFoundViewModel viewModel)
        {
            return View(viewModel);
        }
    }
}
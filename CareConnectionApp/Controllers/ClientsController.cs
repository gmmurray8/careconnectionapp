﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CareConnectionApp.Enums;
using CareConnectionApp.Services.Abstractions;
using CareConnectionApp.Utility_Classes;
using CareConnectionApp.ViewModels.Clients;
using CareConnectionApp.ViewModels.LawnService;
using CareConnectionApp.ViewModels.Shared;
using CareConnectionApp.ViewModels.WorkOrders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CareConnectionApp.Controllers
{
    [Authorize(Roles = "Office Staff, Admin")]
    public class ClientsController : Controller
    {
        private readonly IClientService _clientService;
        private readonly IAdminService _adminService;
        private readonly IIdentityService _identityService;

        public ClientsController
        (
            IClientService clientService,
            IAdminService adminService,
            IIdentityService identityService
            )
        {
            _clientService = clientService;
            _adminService = adminService;
            _identityService = identityService;
        }

        public IActionResult Index()
        {
            return RedirectToAction("ViewClients");
        }

        #region ViewClients
        [HttpGet]
        public IActionResult ViewClients()
        {
            var seachClientView = SearchClientViewModel.Initialize(_clientService);

            return View(seachClientView);
        }

        [HttpPost]
        public IActionResult MoreClients(SearchClientViewModel clientSearch)
        {
            clientSearch = SearchClientViewModel.MoreClients(_clientService, clientSearch);

            return View("ViewClients", clientSearch);
        }

        [HttpPost]
        public IActionResult SearchClients(SearchClientViewModel searchClientView)
        {
            searchClientView = SearchClientViewModel.SearchClients(_clientService, searchClientView);

            return View("ViewClients", searchClientView);
        }
        #endregion

        #region CreateClient
        [HttpGet]
        public IActionResult CreateClient()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateClient(CreateClientViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var client = _clientService.AddClient(viewModel.Client);
                _clientService.AddUserClientAddition(viewModel.UserId, client.Id);
                if (client == null) // handle add client error
                {
                    Response.StatusCode = 404;
                    return View(viewModel);
                }

                if (viewModel.CreateWorkRequestAfter) //redirect to work order directly after if user wanted to
                {
                    return RedirectToAction("CreateWorkOrder", new { id = client.Id });
                }

                return RedirectToAction("ViewClients");
            }
            return View(viewModel);
        }
        #endregion

        #region ClientDetails
        [HttpGet]
        public IActionResult ClientDetails(int id, bool? updateError)
        {
            if (updateError != null)
            {
                ViewBag.updateError = updateError;
            }

            ViewBag.lawnServiceListClientIsOn = _clientService.ListClientIsOn(id);
            var viewModel = ClientDetailsViewModel.Initialize(_clientService, id);
            if (viewModel.Client == null)
            {
                Response.StatusCode = 404;
                return View("ClientNotFound", id);
            }
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult ClientDetails(ClientDetailsViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var client = viewModel.Client;
                _clientService.UpdateClient(client);
                if (viewModel.Client == null)
                {
                    Response.StatusCode = 404;
                    return View("ClientNotFound", viewModel.Client.Id);
                }

                if (viewModel.ZoneId != null)
                {
                    var zone = viewModel.ZoneId ?? default;
                    _adminService.AddZoneJoin(viewModel.Client.Id, zone);
                }
                ViewBag.UpdateError = false;
                return RedirectToAction("ClientDetails", new { id = viewModel.Client.Id });
            }
            var updateError = true;
            return RedirectToAction("ClientDetails", new { id = viewModel.Client.Id, updateError });
        }
        #endregion

        #region WorkOrders
        [HttpGet]
        public IActionResult CreateWorkOrder(int id, string lawnServiceError = null)
        {
            var client = _clientService.GetClient(id);
            if (client == null)
            {
                Response.StatusCode = 404;
                return View("ClientNotFound", id);
            }
            if (lawnServiceError != null)
            {
                ViewBag.lawnServiceErrorMessage = lawnServiceError + _clientService.ListClientIsOn(id);
            }

            return View(CreateWorkOrderViewModel.Initialize(_clientService, client));
        }

        [HttpPost]
        public async Task<IActionResult> CreateWorkOrder(CreateWorkOrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.WorkOrderCategory == "Ramp")
                {
                    return RedirectToAction("CreateRampRequest", model);
                }
                else if (model.WorkOrderCategory == "Assistive Device")
                {
                    return RedirectToAction("CreateAssistiveDeviceWorkOrder", model);
                }
                if (model.WorkOrderCategory == "Lawn Service")
                {
                    var workOrder = await _clientService.CreateWorkOrder(model.ClientId, model.WorkOrderCategory, model.ReqDescription, model.SecondaryContact, model.SecondaryContactNumber);
                    var inList = _clientService.AddToPendingList(model.ClientId, workOrder.Id);
                    if (inList == null)
                    {
                        return RedirectToAction("CreateWorkOrder", new { id = model.ClientId, lawnServiceError = "Client is already on the lawn service list. List: " });
                    }
                    return RedirectToAction("GetClientWorkOrders", new { id = model.ClientId });
                }
                else
                {
                    var workOrder = await _clientService.CreateWorkOrder(model.ClientId, model.WorkOrderCategory, model.ReqDescription, model.SecondaryContact, model.SecondaryContactNumber);
                    if (workOrder == null) // handle work order creation error
                    {
                        Response.StatusCode = 404;
                        return View(model);
                    }
                    return RedirectToAction("GetClientWorkOrders", new { id = model.ClientId });
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult CompleteWorkOrder(int id)
        {
            var workOrder = _clientService.GetWorkOrderWithClient(id);
            if(workOrder.WorkOrderCategory.Category == "Lawn Service")
            {
                return RedirectToAction("Confirmation", "Account", ConfirmationViewModel.Initialize(null, "Cannot complete lawn service workorders.", "Clients"));
            }
            if(workOrder.WorkOrderCompletionFormId != null)
            {
                Exception e = new Exception($"WorkOrder #{workOrder.Id} has already been completed.");
                throw e;
            }

            var model = UpdateCompleteWorkOrderViewModel.Initialize(_clientService, workOrder);

            return View(model);
        }

        [HttpPost]
        public IActionResult CompleteWorkOrder(UpdateCompleteWorkOrderViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var workOrder = _clientService.CompleteWorkOrder(model.Id, (JobStatus)model.JobStatus, model.SecondaryContact,
                            model.SecondaryContactNumber, model.WorkOrderCompletionForm, model.ReqDescription, model.DeviceId);

            if (workOrder.WorkOrderCategory.Category == "Ramp")
            {
                 _clientService.UpdateRampRequest(model.RampRequest.Id, model.RampRequest.PermanentMobilityImpairment, model.RampRequest.MobilityAssistance, model.RampRequest.MobilityDescription,
                    model.RampRequest.ContactedVA, model.RampRequest.OwnerOfHome, model.RampRequest.LengthOfHomeOwnership, model.RampRequest.LiveAlone);
            }

            if (workOrder == null) // handle work order update error
            {
                Response.StatusCode = 404;
                return View(model);
            }

            return RedirectToAction("ViewWorkOrderDetails", new { id = model.Id });
        }

        [HttpGet]
        public IActionResult UpdateIncompleteWorkOrder(int id)
        {
            var workOrder = _clientService.GetWorkOrderWithClient(id);

            if (workOrder.AssistiveDeviceId == 0 && workOrder.WorkOrderCategory.Category == "Assistive Device")
            {
                return RedirectToAction("Confirmation", "Account", ConfirmationViewModel.Initialize(null, "Cannot update this work order because it is missing its assistive device. Try making a new work order and having this one made inactive by an admin.", "Clients"));
            }

            if(workOrder.WorkOrderCompletionFormId != null)
            {
                Exception e = new Exception($"A completion form exists for WorkOrder #{workOrder.Id}.");
                throw e;
            }

            var model = UpdateIncompleteWorkOrderViewModel.Initialize(_clientService, workOrder);

            return View(model);
        }

        [HttpGet]
        public IActionResult UpdateWorkOrder(int id)
        {
            var workOrderComplete = _clientService.GetWorkOrderWithClient(id).WorkOrderCompletionForm;
            if(workOrderComplete != null)
            {
                return RedirectToAction("UpdateCompleteWorkOrder", new { id = id });
            }

            return RedirectToAction("UpdateIncompleteWorkOrder", new { id = id });

        }

        [HttpPost]
        public async Task<IActionResult> UpdateIncompleteWorkOrder(UpdateIncompleteWorkOrderViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Please enter all required fields.");
                model = UpdateIncompleteWorkOrderViewModel.ReinitializeForInvalidModelState(_clientService, model);
                return View(model);
            }

            var update = new UpdateWorkOrderUtility(_clientService);

            var workOrder = await update.UpdateIncomepleteWorkOrder(model);

            if (workOrder == null) // handle work order update error
            {
                Response.StatusCode = 404;
                return View(model);
            }
            return RedirectToAction("ViewWorkOrderDetails", new { id = model.Id });
        }

        [HttpGet]
        public IActionResult UpdateCompleteWorkOrder(int id)
        {
            var workOrder = _clientService.GetWorkOrderWithClient(id);

            if (workOrder.AssistiveDeviceId == 0 && workOrder.WorkOrderCategory.Category == "Assistive Device")
            {
                return RedirectToAction("Confirmation", "Account", ConfirmationViewModel.Initialize(null, "Cannot update this work order because it is missing its assistive device. Try making a new work order and having this one made inactive by an admin.", "Clients"));
            }

            var model = UpdateCompleteWorkOrderViewModel.Initialize(_clientService, workOrder);

            if (workOrder.WorkOrderCompletionFormId != null)
            {
                model.WorkOrderCompletionForm = _clientService.GetWorkOrderCompletionForm((int)workOrder.WorkOrderCompletionFormId);
            }

            ViewBag.dateOfReq = workOrder.DateOfRequest.ToString();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateCompleteWorkOrder(UpdateCompleteWorkOrderViewModel model)
        {
            if (ModelState.IsValid)
            {
                var updateUtility = new UpdateWorkOrderUtility(_clientService);

                var workOrder = await updateUtility.UpdateCompleteWorkOrder(model);

                if (workOrder == null) // handle work order update error
                {
                    Response.StatusCode = 404;
                    return View(model);
                }
                return RedirectToAction("ViewWorkOrderDetails", new { id = model.Id });
            }
            model = UpdateCompleteWorkOrderViewModel.ReinitializeForInvalidModelState(_clientService, model);
            return View(model);
        }

        [HttpGet]
        public IActionResult GetClientWorkOrders(int id)
        {
            var client = _clientService.GetClient(id);
            if (client == null)
            {
                Response.StatusCode = 404;
                return View("ClientNotFound", id);
            }

            var clientRequests = _clientService.GetClientRequests(id);
            var viewModel = GetClientWorkOrdersViewModel.Initialize(_clientService, client, clientRequests);

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult GetClientWorkOrders(GetClientWorkOrdersViewModel model)
        {
            var client = _clientService.GetClient(model.Client.Id);
            if (client == null)
            {
                Response.StatusCode = 404;
                return View("ClientNotFound", model.Client.Id);
            }

            var clientRequests = _clientService.FilterClientRequests(client, model.WorkOrderCategory, model.JobStatus);
            var viewModel = GetClientWorkOrdersViewModel.Initialize(_clientService, model.Client, clientRequests);

            return View(viewModel);
        }

        public IActionResult ViewWorkOrderDetails(int Id)
        {
            var workOrder = _clientService.GetWorkOrderWithClient(Id);
            var client = workOrder.ClientRequest.Client;
            if (workOrder == null || client == null)
            {
                return View("ClientNotFound", workOrder.ClientRequest.Client.Id);
            }
   
            var workDetailsViewModel = WorkDetailsViewModel.Initialize(_clientService, client, workOrder);
            return View(workDetailsViewModel);
        }

        public IActionResult PrintWorkOrderDetails(int Id)
        {
            var workOrder = _clientService.GetWorkOrderWithClient(Id);
            var client = workOrder.ClientRequest.Client;

            var workDetailsViewModel = WorkDetailsViewModel.Initialize(_clientService, client, workOrder);
            return View(workDetailsViewModel);
        }

        [HttpGet]
        public IActionResult SearchWorkOrders()
        {
            return View(new SearchWorkOrdersViewModel
            {
                Categories = _clientService.GetWorkOrderCategoryNamesForSearching()
            });
        }

        [HttpPost]
        public IActionResult SearchWorkOrders(SearchWorkOrdersViewModel model)
        {
            var viewModel = SearchWorkOrdersViewModel.Initialize(_clientService, model, true);
            return View("SearchWorkOrdersResults", viewModel);
        }

        [HttpPost]
        public IActionResult MoreWorkOrders(SearchWorkOrdersViewModel model)
        {
            var viewModel = SearchWorkOrdersViewModel.Initialize(_clientService, model, false);
            return View("SearchWorkOrdersResults", viewModel);
        }

        public IActionResult BlankWorkOrder()
        {
            return View();
        }

        #endregion

        #region CreateRampRequest

        [HttpGet]
        public IActionResult CreateRampRequest(CreateWorkOrderViewModel model)
        {
            return View(CreateRampRequestViewModel.Initialize(model));
        }

        [HttpPost]
        public IActionResult CreateRampRequest(CreateRampRequestViewModel model)
        {
            if (ModelState.IsValid)
            {
                var rampRequest = CreateRampRequestViewModel.GetRampRequest(model);

                var workOrder = CreateRampRequestViewModel.GetWorkOrder(_clientService, model);

                rampRequest = _clientService.CreateRampRequest(rampRequest, workOrder, model.ClientId);

                if (rampRequest == null) // handle ramp request error
                {
                    Response.StatusCode = 404;
                    return View(model);
                }
                return RedirectToAction("ViewClients");
            }
            return View(model);
        }

        #endregion

        #region Eheaps

        public IActionResult GetEheap(int id)
        {
            var client = _clientService.GetClient(id);
            var eheap = _clientService.GetEheap(client.EheapId);
            if (eheap == null) // handle eheap does not exist
            {
                Response.StatusCode = 404;
                return View("ClientNotFound", id);
            }
            var getEheapViewModel = new GetEheapViewModel
            {
                Client = client,
                Eheap = eheap
            };
            return View(getEheapViewModel);
        }

        [HttpGet]
        public IActionResult CreateEheap(int id)
        {
            var client = _clientService.GetClient(id);
            if (client == null)
            {
                Response.StatusCode = 404;
                return View("ClientNotFound", id);
            }
            var EheapViewModel = new EheapViewModel
            {
                ClientId = client.Id,
                Client = client
            };
            return View(EheapViewModel);
        }

        [HttpPost]
        public IActionResult CreateEheap(EheapViewModel model)
        {
            if (ModelState.IsValid)
            {
                var eheap = _clientService.CreateEheap(model.DateOfService, model.ClientId, model.Expenses);
                if (eheap == null) // handle eheap creation error
                {
                    Response.StatusCode = 404;
                    return View(model);
                }
                return RedirectToAction("ViewClients");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult UpdateEheap(int Id)
        {
            var client = _clientService.GetClient(Id);
            var eheap = _clientService.GetEheap(client.EheapId);
            if (client == null)
            {
                Response.StatusCode = 404;
                return View("ClientNotFound", Id);
            }
            var model = new UpdateEheapViewModel
            {
                EheapId = eheap.Id,
                ClientId = client.Id,
                DateOfService = eheap.DateOfService,
                Expenses = eheap.Expenses
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult UpdateEheap(UpdateEheapViewModel model)
        {
            if (ModelState.IsValid)
            {
                _clientService.UpdateEheap(model.ClientId, model.DateOfService, model.Expenses);
                return RedirectToAction("ViewClients");
            }
            return View(model);
        }

        #endregion

        #region AssistiveDevices

        [HttpGet]
        public IActionResult CreateAssistiveDeviceWorkOrder(CreateWorkOrderViewModel model)
        {
            var viewModel = CreateAssistiveDeviceViewModel.Initialize(_clientService, model);
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult CreateAssistiveDeviceWorkOrder(CreateAssistiveDeviceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var device = _clientService.GetAssistiveDevice(model.DeviceId);
                var workOrder = CreateAssistiveDeviceViewModel.GetWorkOrder(_clientService, model);
                _clientService.CreateAssistiveDevice(device, workOrder, model.ClientId);
                return RedirectToAction("ViewClients");
            }
            return View(model);
        }

        public JsonResult GetDevices(int catId)
        {
            var result = _clientService.GetActiveAssistiveDevices(catId);
            return Json(result);
        }

        #endregion

        #region Lawn Service

        [HttpGet]
        public async Task<IActionResult> ViewServiceList()
        {
            var user = await _identityService.UserIsAdmin(User);
            var viewModel = LawnServiceViewModel.Initialize(_adminService, _clientService, user);
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult ViewServiceList(int id)
        {
            if (ModelState.IsValid)
            {
                var viewModel = LawnServiceViewModel.Update(_clientService, id);
                return View(viewModel);
            }
            return RedirectToAction("ViewServiceList");
        }

        [HttpGet]
        public async Task<IActionResult> ViewWaitList()
        {
            var user = await _identityService.UserIsAdmin(User);
            var viewModel = WaitListViewModel.Initialize(_clientService, _adminService, user);
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult ViewWaitList(int id)
        {
            var viewModel = WaitListViewModel.Update(_clientService, id);
            return View(viewModel);
        }

        public IActionResult PrintServiceList(int id)
        {
            var viewModel = PrintLawnServiceViewModel.Initialize(_clientService, id);
            return View(viewModel);
        }

        #endregion

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
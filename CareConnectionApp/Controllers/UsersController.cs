﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CareConnectionApp.Services.Abstractions;
using CareConnectionApp.ViewModels.Shared;
using CareConnectionApp.ViewModels.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CareConnectionApp.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("Admin/Users")]
    public class UsersController : Controller
    {
        private readonly IIdentityService _identityService;
        private readonly IClientService _clientService;

        public UsersController
        (
            IIdentityService identityService,
            IClientService clientService
        )
        {
            _identityService = identityService;
            _clientService = clientService;
        }

        [Route("")]
        public IActionResult Index()
        {
            return RedirectToAction("ViewUsers");
        }

        #region ListUsers

        [Route("List")]
        public IActionResult ViewUsers()
        {
            var viewModel = new ViewUsersViewModel
            {
                Users = _identityService.GetUsers()
            };
            return View(viewModel);
        }

        [Route("List")]
        [HttpPost]
        public IActionResult ViewUsers(ViewUsersViewModel viewModel)
        {
            viewModel.Users = _identityService.SearchUsers(viewModel.SearchString);
            return View(viewModel);
        }

        #endregion

        #region CreateUser

        [Route("New")]
        public IActionResult NewUser()
        {
            return View();
        }

        [Route("New")]
        [HttpPost]
        public async Task<IActionResult> NewUser(NewUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _identityService.AddUser(model.Email, model.Username, model.FullName, model.Password, model.RoleName);

                if (result.Succeeded)
                {
                    return RedirectToAction("ViewUsers");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View(model);
        }

        #endregion

        #region ManageUsers

        [Route("Manage")]
        [HttpGet]
        public async Task<IActionResult> ManageUser(string id)
        {
            var user = await _identityService.GetUser(id);
            if (user == null)
            {
                return RedirectToAction("NotFound", "Account", new NotFoundViewModel { Message = $"User with ID {id} cannot be found" });
            }
            return View(user);
        }

        [Route("Info")]
        [HttpGet]
        public async Task<IActionResult> UpdateUserInfo(string id)
        {
            var user = await _identityService.GetUser(id);
            if (user == null)
            {
                return RedirectToAction("NotFound", "Account", new NotFoundViewModel { Message = $"User with ID {id} cannot be found" });
            }

            var viewModel = UpdateUserInfoViewModel.Initialize(user);

            return View(viewModel);
        }

        [Route("Info")]
        [HttpPost]
        public async Task<IActionResult> UpdateUserInfo(UpdateUserInfoViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var user = await _identityService.GetUser(viewModel.Id);

                if (user == null)
                {
                    return RedirectToAction("NotFound", "Account", new NotFoundViewModel { Message = $"User with ID {viewModel.Id} cannot be found" });
                }

                var result = await _identityService.UpdateUserInformation(user, viewModel.Email, viewModel.UserName, viewModel.FullName);

                if (result.Succeeded)
                {
                    return RedirectToAction("Confirmation", ConfirmationViewModel.Initialize(user.Id, "User information successfully updated.", "Users"));
                }
            }
            return View(viewModel);
        }

        [Route("Roles")]
        [HttpGet]
        public async Task<IActionResult> ManageUserRoles(string id)
        {
            ViewBag.userId = id;

            var user = await _identityService.GetUser(id);

            if (user == null)
            {
                return RedirectToAction("NotFound", "Account", new NotFoundViewModel { Message = $"User with ID {id} cannot be found" });
            }

            var modelList = new List<UserRolesViewModel>();
            var userRoles = await _identityService.GetUserRoles(user);

            foreach (var r in _identityService.GetRoles())
            {
                var viewModel = new UserRolesViewModel
                {
                    RoleId = r.Id,
                    RoleName = r.Name
                };
                viewModel.IsSelected = userRoles.Contains(r.Name);
                modelList.Add(viewModel);
            }
            return View(modelList);
        }

        [Route("Roles")]
        [HttpPost]
        public async Task<IActionResult> ManageUserRoles(List<UserRolesViewModel> model, string id)
        {
            var user = await _identityService.GetUser(id);

            if (user == null)
            {
                return RedirectToAction("NotFound", "Account", new NotFoundViewModel { Message = $"User with ID {id} cannot be found" });
            }

            var roles = await _identityService.GetUserRoles(user);
            var result = await _identityService.RemoveUserFromRoles(user, roles);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Cannot remove user existing roles");
                return View(model);
            }

            result = await _identityService.AddUserToRoles(user,
                model.Where(x => x.IsSelected).Select(y => y.RoleName));

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Cannot add selected roles to user");
                return View(model);
            }

            return RedirectToAction("Confirmation", ConfirmationViewModel.Initialize(user.Id, "Roles successfully updated.", "Users"));
        }

        [Route("Password")]
        [HttpGet]
        public async Task<IActionResult> UpdatePassword(string id)
        {
            var user = await _identityService.GetUser(id);

            if (user == null)
            {
                return RedirectToAction("NotFound", "Account", new NotFoundViewModel { Message = $"User with ID {id} cannot be found" });
            }

            var viewModel = new UpdatePasswordViewModel
            {
                Id = id
            };

            return View(viewModel);
        }

        [Route("Password")]
        [HttpPost]
        public async Task<IActionResult> UpdatePassword(UpdatePasswordViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var user = await _identityService.GetUser(viewModel.Id);

                if (user == null)
                {
                    return RedirectToAction("NotFound", "Account", new NotFoundViewModel { Message = $"User with ID {viewModel.Id} cannot be found" });
                }

                var result = await _identityService.ResetPassword(user, viewModel.NewPassword);

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", "Could not change password");
                    return RedirectToAction("ViewUsers");
                }

                return RedirectToAction("Confirmation", ConfirmationViewModel.Initialize(user.Id, "Password successfully updated.", "Users"));
            }
            return View(viewModel);
        }


        [Route("clients")]
        public async Task<IActionResult> ViewUserClients(string id)
        {
            var user = await _identityService.GetUser(id);
            if (user == null)
            {
                return RedirectToAction("NotFound", "Account", new NotFoundViewModel { Message = $"User with ID {id} cannot be found" });
            }

            var viewModel = ViewUserClientsViewModel.Initialize(user.Id, user.UserName, _clientService.GetUserClientAdditions(user.Id));
            return View(viewModel);
        }

        [Route("clients")]
        [HttpPost]
        public async Task<IActionResult> ViewUserClients(ViewUserClientsViewModel viewModel)
        {
            var user = await _identityService.GetUser(viewModel.UserId);
            if (user == null)
            {
                return RedirectToAction("NotFound", "Account", new NotFoundViewModel { Message = $"User with ID {viewModel.UserId} cannot be found" });
            }
            viewModel = ViewUserClientsViewModel.Initialize(user.Id, user.UserName, _clientService.SearchUserClientAdditions(viewModel.UserId, viewModel.SearchString));
            return View(viewModel);
        }

        #endregion

        [Route("Confirmation")]
        public IActionResult Confirmation(ConfirmationViewModel viewModel)
        {
            return View(viewModel);
        }
    }
}
﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CareConnectionApp.Controllers
{

    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("login", "account");
        }

        [Authorize]
        public IActionResult HelpfulLinks()
        {
            return View();
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using CareConnectionApp.Services.Abstractions;
using CareConnectionApp.ViewModels.Reporting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CareConnectionApp.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("Admin/Reporting")]
    public class ReportingController : Controller
    {
        private readonly IReportingService _reportingService;
        private readonly IClientService _clientService;

        public ReportingController
        (
            IReportingService reportingService, 
            IClientService clientService)
        {
            _reportingService = reportingService;
            _clientService = clientService;
        }
        [Route("")]
        [Route("Home")]
        public IActionResult Reporting()
        {
            return View("Reporting");
        }

        #region JobTypeSummaryReport

        [Route("JobSummaryReport")]
        [HttpGet]
        public IActionResult JobTypeSummaryReport()
        {
            var viewModel = new JobTypeSummaryReportViewModel
            {
                Categories = _reportingService.WorkOrderCategoryNames()
            };
            return View(viewModel);
        }

        [Route("GenerateJobSummaryReport")]
        public JsonResult GenerateJobSummaryReport(JobTypeSummaryReportViewModel model)
        {
            var result = _reportingService.JobSummary(model);
            return Json(result);
        }

        #endregion

        #region DateRangeReport

        [Route("DateRangeReport")]
        [HttpGet]
        public IActionResult DateRangeReports()
        {
            var dateRangeReportsViewModel = new DateRangeViewModel();
            return View(dateRangeReportsViewModel);
        }

        [Route("GenerateDateRangeReport")]
        public async Task<JsonResult> GenerateDateRangeReport(DateTime lower, DateTime upper)
        {
            var dateRangeReportViewModel = await _reportingService.GenerateDateRangeReport(new DateRangeViewModel { LowerBound = lower, UpperBound = upper });
            return Json(dateRangeReportViewModel);
        }

        #endregion

        #region YearOverYearReport

        [Route("YearOverYear")]
        [HttpGet]
        public IActionResult YearOverYearReport()
        {
            return View(new YearOverYearReportViewModel());
        }

        [Route("GenerateYearOverYearReport")]
        public async Task<JsonResult> GenerateYearOverYearReport(int startYear, int endYear)
        {
            var result = await _reportingService.YearOverYearReport(startYear, endYear);
            return Json(result);
        }

        #endregion

        #region ClientReports

        [Route("ClientReports")]
        [HttpGet]
        public IActionResult ClientReports()
        {
            return View();
        }

        [Route("ClientReports")]
        [HttpPost]
        public IActionResult ClientReports(string reportType = null)
        {
            switch (reportType)
            {
                case "Individual":
                    return RedirectToAction("IndividualClientReport");
                case "AllClients":
                    return RedirectToAction("AllClientsReport");
                case "ClientsByCategory":
                    return RedirectToAction("ClientsByCategoryReport");
                default:
                    return View();
            }
        }

        [Route("IndividualClientReport")]
        public IActionResult IndividualClientReport()
        {
            return View();
        }
        
        [Route("AllClientsReport")]
        public IActionResult AllClientsReport()
        {
            return View();
        }

        [Route("ClientsByCategoryReport")]
        public IActionResult ClientsByCategoryReport()
        {
            var categories = _clientService.GetWorkOrderCategoriesNames();
            return View(categories);
        }

        [Route("GenerateIndividualClientReport")]
        public IActionResult GenerateIndividualClientReport(int clientId)
        {
            if (_clientService.GetClient(clientId) != null)
            {
                var result = _reportingService.GetIndividualClientStatistics(clientId);
                return Ok(result);
            }
            return Ok(0);
        }

        [Route("GenerateClientsReport")]
        public IActionResult GenerateClientsReport()
        {
            var result = _reportingService.PrepareClientsReport();
            return Ok(result);
        }

        [Route("GenerateClientsByCategoryReport")]
        public IActionResult GenerateClientsByCategoryReport(string category)
        {
            if (!string.IsNullOrEmpty(category))
            {
                var result = _reportingService.PrepareClientsByCategoryReport(category);
                return Ok(result);
            }
            return Ok(0);
        }

        #endregion

        #region LawnServiceReport

        [Route("LawnServiceReports")]
        public IActionResult LawnServiceReports()
        {
            return View();
        }

        [Route("GenerateLawnServiceReport")]
        public JsonResult GenerateLawnServiceReport(DateTime lower, DateTime upper)
        {
            var lawnServiceReporting = _reportingService.LawnServiceReport(lower, upper);
            return Json(lawnServiceReporting);
        }

        #endregion
    }
}
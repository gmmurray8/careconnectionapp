﻿using System.Collections.Generic;
using System.Linq;
using CareConnectionApp.Models;
using CareConnectionApp.ViewModels.Admin;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using CareConnectionApp.Services.Abstractions;
using CareConnectionApp.ViewModels.LawnService;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace CareConnectionApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IClientService _clientService;
        private readonly IAdminService _adminService;
        private readonly IAlertService _alertService;
        private readonly IIdentityService _identityService;

        public AdminController
        (
            UserManager<ApplicationUser> userManager,
            IClientService clientService,
            IAdminService adminService,
            IAlertService alertService,
            IIdentityService identityService
        )
        {
            _userManager = userManager;
            _clientService = clientService;
            _adminService = adminService;
            _alertService = alertService;
            _identityService = identityService;
        }
        public async Task<IActionResult> Index()
        {

            var adminId = await _identityService.GetUserIdByClaim(User);
            var viewModel = AdminHomeViewModel.Initialize(_clientService, _adminService, adminId);
            return View(viewModel);
        }

        #region Alerts
        public async Task<IActionResult> GetAlerts()
        {
            var adminId = await _identityService.GetUserIdByClaim(User);
            var result = await _alertService.GetAlerts(adminId);
            return Json(result);
        }

        [HttpPost]
        public IActionResult MarkAlerts([FromBody]IEnumerable<int> alerts)
        {
            _alertService.MarkAlertAsRead(alerts);
            return Ok();
        }

        [HttpPost]
        public IActionResult DeleteAlerts([FromBody]IEnumerable<int> alerts)
        {
            _alertService.DeleteAlerts(alerts);
            return Ok();
        }
        #endregion

        #region AdminFavorites
        public async Task<IActionResult> GetAdminFavoriteIDs()
        {
            var adminId = await _identityService.GetUserIdByClaim(User);
            var result = _clientService.GetAdminFaveIDs(adminId);
            return Json(result);
        }

        public IActionResult FavoriteClient(int clientId)
        {
            if (User.IsInRole("Admin"))
            {
                var faveClient = _clientService.SearchAdminFave(clientId, _userManager.GetUserId(HttpContext.User));
                if (faveClient != null)
                {
                    _clientService.RemoveFavorite(faveClient.ClientId, faveClient.AdminId);
                }
                else
                {
                    _clientService.AddFavorite(clientId, _userManager.GetUserId(HttpContext.User));
                }
            }
            return Ok();
        }
        #endregion

        #region WorkOrderCategoryConfig
        [HttpGet]
        public IActionResult ViewWorkOrderCategories()
        {
            var categories = _adminService.GetWorkOrderCategories();
            return View(categories);
        }

        [HttpGet]
        public IActionResult EditWorkOrderCategory(int Id)
        {
            var cat = _adminService.GetWorkOrderCategory(Id);
            return View(cat);
        }

        [HttpPost]
        public IActionResult EditWorkOrderCategory(WorkOrderCategory workOrderCategory)
        {
            if (ModelState.IsValid)
            {
                if (_adminService.WorkOrderCategoryExists(workOrderCategory))
                {
                    ModelState.AddModelError("Category Naming Conflict",
                       $"A category with the name \"{workOrderCategory.Category}\" already exists." +
                       $" Please Try a different name.");
                    return View(workOrderCategory);
                }

                _adminService.UpdateWorkOrderCategory(workOrderCategory);
                return RedirectToAction("ViewWorkOrderCategories");
            }
            return RedirectToAction("EditWorkOrderCategory", new { Id = workOrderCategory.Id });
        }

        [HttpGet]
        public IActionResult AddWorkOrderCategory()
        {
            return View(new WorkOrderCategory { IsBaseCategory = false });
        }

        [HttpPost]
        public IActionResult AddWorkOrderCategory(WorkOrderCategory categoryName)
        {
            if (ModelState.IsValid)
            {
                if (_adminService.CategoryNameExists(categoryName))
                {
                    ModelState.AddModelError("Category Naming Conflict",
                        $"A category with the name \"{categoryName.Category}\" already exists." +
                        $" Please Try a different name.");
                    return View(categoryName);
                }
                var result = _adminService.AddWorkOrderCategory(categoryName);
                return RedirectToAction("ViewWorkOrderCategories", categoryName);
            }
            return RedirectToAction("AddWorkOrderCategory");
        }

        [HttpPost]
        public IActionResult AttemptDeleteWorkOrder(WorkOrderCategory workOrdercategory)
        {
            var result = _adminService.DeleteWorkOrderCategory(workOrdercategory.Id);
            if (result)
            {
                return RedirectToAction("ViewWorkOrderCategories");
            }
            else
            {
                ModelState.AddModelError("Delete Conflict", "Category is either a base caegory, which can not be deleted, or the category has jobs that" +
                    "have been added to the system");

                return RedirectToAction("EditWorkOrderCategory", workOrdercategory);
            }
        }
        #endregion

        #region AssistiveDeviceConfig
        public IActionResult ViewAssistiveDevices()
        {
            return View(new ViewAssistiveDevicesViewModel { AssistiveDevices = _adminService.GetAssistiveDevices() });
        }

        public IActionResult ViewAssistiveDeviceCategories()
        {
            return View(new ViewAssistiveDeviceCategoriesViewModel { AssistiveDeviceCategories = _adminService.GetAssistiveDeviceCategories() });
        }

        [HttpGet]
        public IActionResult UpdateAssistiveDeviceCategory(int id)
        {
            return View(_adminService.GetAssistiveDeviceCategory(id));
        }

        [HttpPost]
        public IActionResult UpdateAssistiveDeviceCategory(AssistiveDeviceCategory updateCategory)
        {
            if (string.IsNullOrEmpty(updateCategory.Name))
            {
                return RedirectToAction("UpdateAssistiveDeviceCategory", new { id = updateCategory.Id });
            }
            _adminService.UpdateAssistiveDeviceCategory(updateCategory.IsActive, updateCategory.Name,
                updateCategory.Description, updateCategory.Id);
            return RedirectToAction("ViewAssistiveDeviceCategories");
        }


        [HttpGet]
        public IActionResult UpdateAssistiveDevice(int id)
        {
            return View(UpdateAssistiveDeviceViewModel.Initialize(_adminService, id));
        }

        [HttpPost]
        public IActionResult UpdateAssistiveDevice(UpdateAssistiveDeviceViewModel updatedDevice)
        {
            if (string.IsNullOrEmpty(updatedDevice.AssistiveDevice.Name))
            {
                return RedirectToAction("UpdateAssistiveDevice", new { id = updatedDevice.AssistiveDevice.Id });
            }
            _adminService.UpdateAssistiveDevice(updatedDevice.AssistiveDevice.Id, updatedDevice.AssistiveDevice.IsActive, updatedDevice.AssistiveDevice.Name, updatedDevice.AssistiveDevice.Description,
                updatedDevice.AssistiveDevice.CategoryId);
            return RedirectToAction("ViewAssistiveDevices");
        }

        [HttpGet]
        public IActionResult AddAssistiveDevice()
        {
            var model = AddAssistiveDeviceViewModel.Initialize(_clientService);
            return View(model);
        }

        [HttpPost]
        public IActionResult AddAssistiveDevice(AddAssistiveDeviceViewModel addAssistiveDeviceViewModel)
        {
            if (ModelState.IsValid)
            {
                var assistiveDevice = AddAssistiveDeviceViewModel.SetupAssistiveDevice(addAssistiveDeviceViewModel);

                assistiveDevice = _adminService.AddAssistiveDevice(assistiveDevice);
                if (assistiveDevice != null)
                {
                    return RedirectToAction("ViewAssistiveDevices");
                }
            }
            return RedirectToAction("AddAssistiveDevice");
        }

        [HttpGet]
        public IActionResult AddAssistiveDeviceCategory()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddAssistiveDeviceCategory(AddAssistiveDeviceCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_clientService.GetActiveAssistiveDeviceCategories().Any(a => a.Name.Equals(model.Name)))
                {
                    ViewBag.UpdateError = "This category already exists";
                    return View(model);
                }
                var category = AddAssistiveDeviceCategoryViewModel.GetAssistiveDeviceCategory(model);
                category = _adminService.AddAssistiveDeviceCategory(category);
                return RedirectToAction("ViewAssistiveDevices");
            }
            return RedirectToAction("AddAssistiveDeviceCategory");
        }
        #endregion

        #region LawnService

        public IActionResult ViewPendingApprovalList(bool flag = false)
        {
            var viewModel = new ViewPendingApprovalViewModel()
            {
                PendingApprovals = _adminService.GetPendingApprovals()
            };
            if (flag)
            {
                ViewBag.error = "Please enter a zone for the client";
            }
            return View(viewModel);
        }

        public IActionResult RemoveFromPendingApproval(int id)
        {
            _adminService.DeleteFromPendingApprovals(id);
            return RedirectToAction("ViewPendingApprovalList");
        }

        public bool CheckZoneStatus(int id)
        {
            return _adminService.CheckZoneStatus(id);
        }

        public IActionResult AddToWaitList(int id)
        {
            var waitList = _adminService.AddToWaitList(id);
            return RedirectToAction("ViewPendingApprovalList");
        }


        public bool AddMultipleToWaitList([FromBody]IEnumerable<int> ids)
        {
            var waitList = _adminService.AddToWaitList(ids);
            return true;
        }

        public IActionResult AddMultipleToServiceList([FromBody]IEnumerable<int> ids)
        {
            var warningMessage = _adminService.TooManyClientsWarning(_userManager.GetUserId(HttpContext.User));
            var serviceList = _adminService.AddToLawnServiceList(ids);
            return RedirectToAction("ViewWaitList", "Clients", new { warningMessage });
        }

        public IActionResult RemoveFromWaitList(int id)
        {
            var deleteWaitList = _adminService.GetWaitListById(id);
            _adminService.DeleteFromWaitList(deleteWaitList);
            return RedirectToAction("ViewWaitList", "Clients");
        }

        public IActionResult AddToServiceList(int id)
        {
            var lawnServiceList = _adminService.AddToLawnServiceList(id);
            return RedirectToAction("ViewWaitList", "Clients");
        }

        public IActionResult RemoveFromServiceList(int id)
        {
            _adminService.DeleteFromServiceList(id);
            return RedirectToAction("ViewServiceList", "Clients");
        }

        [HttpGet]
        public IActionResult AddLawnService()
        {
            return View(new AddLawnServiceViewModel { Zones = _adminService.GetZones() });
        }

        [HttpPost]
        public IActionResult AddLawnService(AddLawnServiceViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _adminService.AddLawnService(viewModel.ZoneId, viewModel.TimeStamp);
                return RedirectToAction("ViewServiceList", "Clients");
            }
            return RedirectToAction("AddLawnService");
        }

        [HttpGet]
        public IActionResult AddZone()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddZone(Zone zone)
        {
            if (ModelState.IsValid)
            {
                _adminService.AddZone(zone);
                return RedirectToAction("ViewZones");
            }
            return RedirectToAction("AddZone");
        }

        [HttpGet]
        public IActionResult UpdateZone(int id)
        {
            var zone = _adminService.GetZones().FirstOrDefault(z => z.Id == id);
            return View(zone);
        }

        [HttpPost]
        public IActionResult UpdateZone(Zone zone)
        {
            if (ModelState.IsValid)
            {
                _adminService.UpdateZone(zone);
                return RedirectToAction("ViewZones");
            }
            return RedirectToAction("UpdateZone", zone.Id);
        }

        [HttpGet]
        public IActionResult AddZoneToClient(int id)
        {
            var viewModel = AddZoneToClientViewModel.Initialize(_adminService, _clientService, id);
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult AddZoneToClient(AddZoneToClientViewModel viewModel)
        {
            if (viewModel.Zone.Id == 0)
            {
                return RedirectToAction("AddZoneToClient", new { id = viewModel.Client.Id });
            }
            _adminService.AddZoneJoin(viewModel.Client.Id, viewModel.Zone.Id);
            return Redirect(viewModel.ReturnUrl);
        }

        public IActionResult ViewZones()
        {
            IEnumerable<Zone> zones = _adminService.GetZones();
            return View(zones);
        }
        #endregion

        public IActionResult SystemConfiguration()
        {
            return View();
        }
    }
}
﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace CareConnectionApp.Controllers
{
    public class ErrorController : Controller
    {
        [Route("Error/{statusCode}")]
        public IActionResult HttpStatusCodeHandler(int statusCode)
        {
            switch (statusCode)
            {
                case 404:
                    ViewBag.StatusCode = statusCode;
                    ViewBag.ErrorMessage = "Sorry, the resource you requested could not be found";
                    break;
                case 500:
                    ViewBag.StatusCode = statusCode;
                    ViewBag.ErrorMessage = "Sorry, there was an internal server error";
                    break;
                case 502:
                    ViewBag.StatusCode = statusCode;
                    ViewBag.ErrorMessage = "Sorry, one server recieved an invalid response from another";
                    break;
                case 503:
                    ViewBag.StatusCode = statusCode;
                    ViewBag.ErrorMessage = "Sorry, the server is unavailable to handle this request right now. Try clearing browser cache and history";
                    break;
            }

            return View("PageError");
        }

        //Handle unhandled exceptions
        [Route("Error")]
        public IActionResult UnhandledException()
        {
            var exception = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            ViewBag.ExceptionPath = exception.Path;
            ViewBag.ExceptionMessage = exception.Error.Message;

            return View("UnhandledExceptionErrorPage");

        }
    }
}

﻿let ids = [];
$('#addToWaitList').click(function () {
    $.ajax({
        type: "POST",
        data: JSON.stringify(ids),
        url: window.location.origin + "/Admin/AddMultipleToWaitList",
        contentType: "application/json; charset=utf-8",
        processData: true,
        cache: false,

    }).done(function () {
        location.reload();
    });
});

$('#addToServiceList').click(function () {
    $.ajax({
        type: "POST",
        data: JSON.stringify(ids),
        url: window.location.origin + "/Admin/AddMultipleToServiceList",
        contentType: "application/json; charset=utf-8",
        processData: true,
        cache: false,

    })
        .done(function (data) {
            if (data == true) {
                location.reload();
            }
            else {
                window.location.replace("/Clients/ViewWaitList?fullServiceList=true");
            }
        });
});

function addToServiceList(element, id) {
    if (element.checked == true) {
        ids.push(id);
    }
    else {
        ids.splice(ids.indexOf(id), 1);
    }
}

function addToWaitList(element, clientId, id) {
    if (element.checked == true) {
        $.get(
            "/Admin/CheckZoneStatus?id=" + clientId,
            function (data) {
                if (!data) {
                    alert("Please add a zone to the client");
                    element.checked = false;
                }
                else {
                    ids.push(id);
                }
            }
        );
    }
    else {
        ids.splice(ids.indexOf(id), 1);
    }
}


function filterWaitList() {
    let filter = $('#filter');
    if (filter.val() === "") {
        alert("Please pick a zone");
    }
    else {
        $("#waitListFilterForm").submit();
    }
}

function checkFilterStatus() {
    let filter = $('#filter');
    if (filter.val() === "") {
        alert("Please pick a zone");
    }
    else {
        $("#serviceListFilterForm").submit();
    }
}

function checkFilterForPrint() {
    let filter = $('#filter');
    if (filter.val() === "") {
        alert("Please pick a zone");
    }
    else {
        window.open("/Clients/PrintServiceList?id=" + filter.val());
    }
}

function checkZoneStatus(id, pendingId) {
    $(function () {
        $.get(
            "/Admin/CheckZoneStatus?id=" + id,
            function (data) {
                if (!data) {
                    alert("Please add a zone to the client");
                }
                else {
                    window.location.replace("/Admin/AddToWaitList?id=" + pendingId);
                }
            }
        );
    });

}        
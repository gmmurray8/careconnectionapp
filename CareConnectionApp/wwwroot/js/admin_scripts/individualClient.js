﻿let clientArray = [];
let clientName = '';
$('#individualReport').submit(function (e) {
    e.preventDefault();
    showLoader(true);
    let url = $(this).attr('action');
    $.getJSON(url, $(this).serialize(), function (data) {
        if (!validateResults(data)) {
            showLoader(false);
            $('#clientHeader').html('');
            return false;
        }
        clientName = Object.keys(data)[0];
        delete data[clientName];
        $('#clientHeader').html(clientName);
        createTable(data);
        exportReport(data);
    });
});

function validateResults(data) {
    if (data == 0) {
        alert('That client could not be found');
        return false;
    } else {
        return true;
    }
}

function showLoader(loading) {
    if (loading) {
        $('#loaderRow').removeClass('d-none');
        $('#loaderDiv').html('<div class="spinner-border text-primary float-center ml-5" style="width:5rem; height:5rem"role="status"><span class="sr-only"> Loading...</span></div>');
    } else {
        $('#loaderDiv').html('');
        $('#loaderRow').addClass('d-none');
    }
}

function createTable(data) {
    let rows = '';
    $.each(data, function (i, e) {
        rows += `<tr><td>${i}</td><td>${e}</td></tr>`
    });
    showLoader(false);
    $('#reportTable').html(`<thead class="bg-primary text-white"><tr><td>Category</td><td>Value</td></tr></thead><tbody>${rows}</tbody>`);
}

function setClientArray(data) {
    clientArray = [];
    $.each(data, (i, e) => {
        clientArray.push([i, e]);
    });
}

function exportReport(data) {
    setClientArray(data);
    let btn = $('#exportLink');
    btn.removeClass('disabled');
    btn.attr('onclick', 'createCSV()');
}

function createCSV() {
    let csv = `Client Statistics for ${clientName}\n`;
    $.each(clientArray, function (i, e) {
        csv += e.join(',');
        csv += '\n';
    });
    let hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'report.csv'
    hiddenElement.click();
}
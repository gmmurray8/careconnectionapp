﻿function removeFavorite(button){
    let id = button.attr("clientId");
    $.ajax({
        url: window.location.origin + '/Admin/FavoriteClient',
        contentType: 'application/json; charset=utf-8',
        data: { "clientId": id },
        dataType: "json"
    });
}

function removeRowFromTable(button) {
    button.parent('span').parent('div').parent('div').parent('li').remove();
}

function updateFavoriteButton(button) {
    button.toggleClass('btn-warning');
    button.toggleClass('btn-outline-warning');

    button.children().toggleClass('far fa-star');
    button.children().toggleClass('fas fa-star');

    button.children().toggleClass('text-white');
    button.children().toggleClass('text-warning');
}

function updateFavoritesOnLoad(button) {
    $.get(window.location.origin + '/Admin/GetAdminFavoriteIDs', function (data) {
        let list = [];
        $.each(data, function (i, e) {
            list.push(e);
        });
        button.each(function (i, e) {
            let id = $(this).attr('id');
            if (list.some(listId => listId == id)) {
                updateFavoriteButton($(this));
            }
        });
    });
}
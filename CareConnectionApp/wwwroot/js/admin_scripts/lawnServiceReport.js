﻿let zoneArray = [];
let startDate = '';
let endDate = '';
let resultString = '';
let totalServices;

$('#lawnServiceReport').submit(function (e) {
    e.preventDefault();
    showLoader(true);
    if ($('input[name=lower]').val() > $('input[name=upper]').val()) {
        alert('Please enter a start date that is before the end date.');
        showLoader(false);
        return;
    }
    let url = $(this).attr('action');
    $.getJSON(url, $(this).serialize(), function (data) {
        let rows = parseReport(data);
        showLoader(false);
        createTable(rows);
        exportReport(data);
    });
});

function parseReport(data) {
    let rows = '<tbody>';
    $.each(data.zoneReports, (i, e) => {
        rows += `<tr><td>${e.name}</td><td>${e.lawnsCut}</td><td>${e.uniqueClients}</td></tr>`;
    });
    rows += `<tr><td></td><td>${data.totalLawnsCut}</td><td>${data.totalUniqueClients}</td></tr></tbody>`;
    startDate = data.startDate;
    endDate = data.endDate;
    totalServices = data.totalLawnServices;
    resultString = `Total Services: ${data.totalLawnServices}`;
    return rows;
}

function showLoader(loading) {
    if (loading) {
        $('#loaderRow').removeClass('d-none');
        $('#loaderDiv').html('<div class="spinner-border text-primary float-center ml-5" style="width:5rem; height:5rem"role="status"><span class="sr-only"> Loading...</span></div>');
    } else {
        $('#loaderDiv').html('');
        $('#loaderRow').addClass('d-none');
    }
}

function createTable(rows) {
    let table = $('#reportTable');
    let header = '<thead><tr class="bg-primary text-white"><th>Zone</th><th>Lawns Cut</th><th>Clients</th></tr></thead>';
    table.html(header + rows);
    $('#resultsDiv').html(resultString);
}

function exportReport(data) {
    setZoneArray(data);
    let btn = $('#exportLink');
    btn.removeClass('disabled');
    btn.attr('onclick', 'createCSV()');
}

function setZoneArray(data) {
    zoneArray = [];
    zoneArray.push(['Zone','Lawns Cut','Clients'])
    $.each(data.zoneReports, (i, e) => {
        zoneArray.push([e.name,e.lawnsCut,e.uniqueClients]);
    });
}

function createCSV() {
    let csv = `Lawn Services by Zone from ${$('#lower').val()} to ${$('#upper').val()}\n`;
    $.each(zoneArray, function (i, e) {
        csv += e.join(',');
        csv += '\n';
    });
    csv += `Total Services, ${totalServices}`;
    let hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'report.csv'
    hiddenElement.click();
}

﻿let tableCSVArray = [];
let graphCSVArray = [];
let pieCSVArray = [];
let subpieCSVArray = [];
function generateReport(e, thisForm) {
    showLoader(true);
    // Get url from form
    let url = thisForm.attr('action');

    // Post to admin controller for JSON 
    $.getJSON(url, thisForm.serialize(), function (data) {
        console.log('Successfully post server for JSON');
        showLoader(false);
        // Create line chart
        createLineChart(data);

        // Create pie chart
        createPieChart(data);

        // Create subdata pie charts
        createSubPieCharts(data);

        // Create table 
        createSummaryTable(data);

        // Generate report export link
        exportReport();
    });
}

function showLoader(loading) {
    if (loading) {
        $('#loaderRow').removeClass('d-none');
        $('#loaderDiv').html('<div class="spinner-border text-primary float-center ml-5" style="width:5rem; height:5rem"role="status"><span class="sr-only"> Loading...</span></div>');
    } else {
        $('#loaderDiv').html('');
        $('#loaderRow').addClass('d-none');
    }
}

function createSummaryTable(report) {
    tableCSVArray = [];
    let tableArr = [];
    let sumObject = {
        "uniqueClients": 0,
        "jobsCompleted": 0,
        "jobsDeferred": 0,
        "donations": 0,
        "expenses": 0,
        "miles": 0,
        "hours": 0
    };
    $.each(report.years, function (i, e) {
        let row = `<tr><td>${i}</td><td>${e.yearlyReport.numOfUniqueClients}</td>` +
            `<td>${e.yearlyReport.numOfJobsCompleted}</td><td>${e.yearlyReport.numOfJobsDeferred}</td>` +
            `<td>${e.yearlyReport.donations.toFixed(2)}</td><td>${e.yearlyReport.expenses.toFixed(2)}</td>` +
            `<td>${e.yearlyReport.miles}</td><td>${e.yearlyReport.hours}</td></tr>`;
        tableArr.push(row);
        tableSum(sumObject, e);
        tableCSVArray.push([i, e.yearlyReport.numOfUniqueClients, e.yearlyReport.numOfJobsCompleted, e.yearlyReport.numOfJobsDeferred, e.yearlyReport.donations, e.yearlyReport.expenses, e.yearlyReport.miles, e.yearlyReport.hours]);
    });

    let result = '<table class="table table-hover"><thead class="bg-primary text-white"><tr><th>Year</th><th>Unique Clients</th><th>Completed Jobs</th><th>Jobs Deferred</th><th>Donations</th><th>Expenses</th><th>Miles</th><th>Hours</th></tr></thead>';
    $.each(tableArr, function (i, e) {
        result += e;
    });
    result += `<tr class="table-success"><td>Totals</td><td>${report.totalUniqueClients}</td>` +
        `<td>${sumObject.jobsCompleted}</td><td>${sumObject.jobsDeferred}</td>` +
        `<td>${sumObject.donations.toFixed(2)}</td><td>${sumObject.expenses.toFixed(2)}</td>` +
        `<td>${sumObject.miles}</td><td>${sumObject.hours}</td></tr>`;
    result += '</table>';
    $('#tableDiv').html(result);
}

function createLineChart(data) {
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    let traceArray = [];
    graphCSVArray = [];

    // Iterates through JSON once per year
    $.each(data.years, function (i, e) {
        let jobs = [];

        // Iterate through each month in that year
        $.each(e.months, function (index, element) {
            jobs.push(element.monthlyReprt.numOfJobsCompleted);
        }); // End month for each loop
        graphCSVArray.push([i,jobs]);

        // Create trace for graph and add it to array of traces
        let trace = {
            x: months,
            y: jobs,
            text: months,
            type: 'scatter',
            name: i
        };
        traceArray.push(trace);
    }); // End year for each loop

    let layout = {
        title: 'Year Over Year Report',
        xaxis: {
            title: 'Months'
        },
        yaxis: {
            title: 'Number of Jobs'
        }
    };

    Plotly.purge(lineDiv);
    Plotly.react('lineDiv', traceArray, layout);
}

function createPieChart(report) {
    pieCSVArray = [];
    let categoryLabels = []; // Array of labels for pie chart 
    let categoryReports = []; // Array of category reports from json
    let categoryTotals = []; // Array of totals for each category for all years combined
    let categoryObject = {}; // Object with key category and value count

    $.each(report.years, function (i, e) { // Get each report from JSON
        categoryReports.push(e.yearlyReport.categoriesReport);
    });

    $.each(categoryReports, function (index, element) { // For each category report in the array of category reports
        $.each(element, function (key, value) { // For each category in each category report
            categoryObject[value.category] = (categoryObject[value.category] || 0) + value.count; // Create new key if it does not exist and add count as value
        });
    });
    categoryLabels = Object.keys(categoryObject);
    categoryTotals = Object.values(categoryObject);
    pieCSVArray.push([categoryLabels]);
    pieCSVArray.push([categoryTotals]);
    let data = [{
        values: categoryTotals,
        labels: categoryLabels,
        textinfo: "percent",
        automargin: true,
        type: 'pie'
    }];

    let layout = {
        title: 'Total Work Requests'
    };

    Plotly.react('pieDiv', data, layout);
}

function createSubPieCharts(report) {
    subpieCSVArray = [];
    let data = [];
    let x = 0;
    let y = 0;
    let categoryReports = [];
    let categoryLabels = [];
    let currYear = Object.keys(report.years)[0]; // First year is the first year in the json report
    $.each(report.years, function (i, e) { // Get each category report from JSON
        categoryReports.push(e.yearlyReport.categoriesReport);
    });

    $.each(categoryReports, function (i, e) { // For each category report in array of category reports
        let thisYearsRequests = []; // Redeclare this years requests on each iteration
        $.each(e, function (index, element) { // For each category in each category report
            if (!categoryLabels.includes(element.category)) categoryLabels.push(element.category); // If the array of category names does not contain this category, push it
            thisYearsRequests.push(element.count); // Add the requests for this category to the array
        });
        let curr = {
            values: thisYearsRequests,
            labels: categoryLabels,
            type: 'pie',
            name: currYear,
            title: currYear,
            domain: {
                row: x,
                col: y,
            },
            hoverinfo: 'label+percent+value+name',
            textinfo: 'none'
        };
        data.push(curr);
        subpieCSVArray.push(['Year', categoryLabels]);
        subpieCSVArray.push([currYear, thisYearsRequests]);
        x++;
        currYear++;
    });

    if (x == 1) {
        Plotly.purge(smallPieDiv);
        return null;
    }

    let layout = {
        grid: { rows: x, columns: 1 },
        width: 500,
        height: 500,
        showlegend: true,
        automargin: true,
        title: 'Work Requests Per Year',
    };

    Plotly.purge(smallPieDiv);
    Plotly.plot('smallPieDiv', data, layout);
}

function resetGraph() {
    location = location;
}

function tableSum(sumObject, e) {
    sumObject.uniqueClients += e.yearlyReport.numOfUniqueClients;
    sumObject.jobsCompleted += e.yearlyReport.numOfJobsCompleted;
    sumObject.jobsDeferred += e.yearlyReport.numOfJobsDeferred;
    sumObject.donations += e.yearlyReport.donations;
    sumObject.expenses += e.yearlyReport.expenses;
    sumObject.miles += e.yearlyReport.miles;
    sumObject.hours += e.yearlyReport.hours;
}

function exportReport() {
    let btn = $('#exportLink');
    btn.removeClass('disabled');
    btn.attr('onclick', 'reportCSV()');
}

function reportCSV() {
    let csv = 'Work Orders Per Year by Month\nYear,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sept,Oct,Nov,Dec\n';
    $.each(graphCSVArray, function (i, e) {
        csv += e.join(',');
        csv += '\n';
    });
    csv += 'Yearly Totals\nYear,Unique Clients,Jobs Completed,Jobs Deferred,Donations,Expenses,Miles,Hours\n';
    $.each(tableCSVArray, function (i, e) {
        csv += e.join(',');
        csv += '\n';
    });
    csv += 'Totals by Category\n';
    $.each(pieCSVArray, function (i, e) {
        csv += e.join(',');
        csv += '\n';
    });
    csv += 'Totals by Category Per Year\n';
    $.each(subpieCSVArray, function (i, e) {
        csv += e.join(',');
        csv += '\n';
    });
    let hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'report.csv'
    hiddenElement.click();
}
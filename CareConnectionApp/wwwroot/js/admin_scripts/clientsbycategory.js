﻿let clientArray = [];
let categoryName = '';
$('#clientsReport').submit(function (e) {
    e.preventDefault();
    showLoader(true);
    let url = $(this).attr('action');
    $.getJSON(url, $(this).serialize(), function (data) {
        if (!validateResults(data)) {
            showLoader(false);
            $('#categoryHeader').html('');
            $('#reportTable').html('');
            return false;
        }
        categoryName = data.category;
        $('#categoryHeader').html(categoryName);
        createTable(data);
        exportReport(data);
    });
});

function validateResults(data) {
    if (data == 0) {
        alert('That category could not be found');
        return false;
    } else {
        return true;
    }
}

function showLoader(loading) {
    if (loading) {
        $('#loaderRow').removeClass('d-none');
        $('#loaderDiv').html('<div class="spinner-border text-primary float-center ml-5" style="width:5rem; height:5rem"role="status"><span class="sr-only"> Loading...</span></div>');
    } else {
        $('#loaderDiv').html('');
        $('#loaderRow').addClass('d-none');
    }
}

function createTable(data) {
    let rows = '';
    $.each(data.results, function (i, e) {
        rows += `<tr><td>${e.id}</td><td>${e.name}</td><td>${formatPhoneNumber(e.phone)}</td><td>${e.address}</td></tr>`
    });
    showLoader(false);
    $('#reportTable').html(`<thead class="bg-primary text-white"><tr><td>Id</td><td>Name</td><td>Phone Number</td><td>Address</td></tr></thead><tbody>${rows}</tbody>`);
}

function setClientArray(data) {
    clientArray = [];
    $.each(data.results, (i, e) => {
        clientArray.push([e.id, e.name, e.phone, e.address]);
    });
}

function exportReport(data) {
    setClientArray(data);
    let btn = $('#exportLink');
    btn.removeClass('disabled');
    btn.attr('onclick', 'createCSV()');
}

function createCSV() {
    let csv = `Client(s) with ${categoryName} Work Orders\n`;
    $.each(clientArray, function (i, e) {
        csv += e.join(',');
        csv += '\n';
    });
    let hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'report.csv'
    hiddenElement.click();
}

function formatPhoneNumber(number) {
    return number.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
}
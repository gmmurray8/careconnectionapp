﻿let unreadAlerts = [];
let allAlerts = [];

function getAlerts() {
    // Post to admin controller for JSON 
    return $.getJSON(window.location.origin + '/Admin/GetAlerts').done(function (data) {
        console.log('Successfully post server for JSON');

        updateAlertCounts(data);
        renderUnreadAlertsList(data.alerts);
        renderAllAlertsList(data.alerts);
    });
}

function updateAlertCounts(data) {
    data.unreadAlertCount ? $('.unreadAlertCount').html(data.unreadAlertCount) : $('.unreadAlertCount').html('');
    data.allAlertCount ? $('.allAlertCount').html(data.allAlertCount) : $('.allAlertCount').html('');
}

function renderUnreadAlertsList(alerts) {
    const alertRows = [];
    const alertsList = [];
    $.each(alerts, function (i, e) {
        if (!e.read) {
            alertsList.push(e.id);
            let timeStamp = new Date(Date.parse(e.timeStamp));
            let formattedDate = `${timeStamp.getMonth() + 1}/${timeStamp.getDate()}/${timeStamp.getFullYear()}`;

            let row = `<div class="alert alert-warning text-primary" role="alert"><div class="row"><div class="col">${e.alertText}</div><div class="col">${formattedDate}</div>`;
            if (e.clientId !== null) {
                let link = `${window.location.origin}/Clients/ClientDetails/${e.clientId}`;
                row += `<div class="col"><a href="${link}">Client Details</a></div>`;
            }
            if (e.workOrderId !== null) {
                let link = `${window.location.origin}/Clients/ViewWorkOrderDetails/${e.workOrderId}`;
                row += `<div class="col"><a href="${link}">Work Order Details</a></div>`;
            }
            if (e.assistiveDeviceId !== null) {
                let link = `${window.location.origin}/Admin/UpdateAssistiveDevice/${e.assistiveDeviceId}`;
                row += `<div class="col"><a href="${link}">Assistive Device Details</a></div>`;
            }
            row += `<div class="col-2"><a class="text-primary read-button text-link" readId=${e.id}>Mark Read</a></div>`;
            row += `<div class="col-2"><a class="text-primary read-button text-link" deleteId=${e.id}>Delete</a></div>`;
            alertRows.push(row);
            alertRows.push('</div>');
        }
    });
    unreadAlerts = alertsList.slice();
    unreadListHtml(alertRows);
}

function renderAllAlertsList(alerts) {
    const alertRows = [];
    const unreadAlertList = [];
    const allAlertList = [];
    $.each(alerts, function (i, e) {
        allAlertList.push(e.id);
        let read = e.read;
        let timeStamp = new Date(Date.parse(e.timeStamp));
        let formattedDate = `${timeStamp.getMonth() + 1}/${timeStamp.getDate()}/${timeStamp.getFullYear()}`;

        let row = `<div class="alert ${read ? 'alert-secondary' : 'alert-warning'} text-primary" role="alert"><div class="row"><div class="col">${e.alertText}</div><div class="col">${formattedDate}</div>`;
        if (e.clientId !== null) {
            let link = `${window.location.origin}/Clients/ClientDetails/${e.clientId}`;
            row += `<div class="col"><a href="${link}">Client Details</a></div>`;
        }
        if (e.workOrderId !== null) {
            let link = `${window.location.origin}/Clients/ViewWorkOrderDetails/${e.workOrderId}`;
            row += `<div class="col"><a href="${link}">Work Order Details</a></div>`;
        }
        if (e.assistiveDeviceId !== null) {
            let link = `${window.location.origin}/Admin/UpdateAssistiveDevice/${e.assistiveDeviceId}`;
            row += `<div class="col"><a href="${link}">Assistive Device Details</a></div>`;
        }
        if (!read) {
            unreadAlertList.push(e.id);
            row += `<div class="col-2"><a class="text-primary read-button text-link" readId=${e.id}>Mark Read</a></div>`;
        } else {
            row += '<div class=col-2></div>';
        }
        row += `<div class="col-2"><a class="text-primary read-button text-link" deleteId=${e.id}>Delete</a></div>`;
        alertRows.push(row);
        alertRows.push('</div>');
    });
    unreadAlerts = unreadAlertList.slice();
    allAlerts = allAlertList.slice();
    allAlertListHtml(alertRows);
}

function markRead(e) {
    let target = e.target;

    if (target.hasAttribute('readid')) {
        let thisId = $(target).attr('readid');
        $(target).attr("disabled", true);
        let thisAlert = [];
        thisAlert.push(thisId);

        $.ajax({
            type: "POST",
            data: JSON.stringify(thisAlert),
            url: window.location.origin + "/Admin/MarkAlerts",
            contentType: "application/json; charset=utf-8",
            processData: true,
            cache: false,
        }).done(function () {
            getAlerts();
        });

    }
}

function handleAlertControls(e) {
    let target = e.target;
    if (target.hasAttribute('action') && $(target).attr('action') == 'read') {
        readAllUnreadAlerts();
    } else if (target.hasAttribute('action') && $(target).attr('action') == 'delete'){
        deleteAllAlerts();
    }
}

function readAllUnreadAlerts() {
    $.ajax({
        type: "POST",
        data: JSON.stringify(unreadAlerts),
        url: window.location.origin + "/Admin/MarkAlerts",
        contentType: "application/json; charset=utf-8",
        processData: true,
        cache: false,
    }).done(function () {
        getAlerts();
    });
}

function deleteAllAlerts() {    
    $.ajax({
        type: "POST",
        data: JSON.stringify(allAlerts),
        url: window.location.origin + "/Admin/DeleteAlerts",
        contentType: "application/json; charset=utf-8",
        processData: true,
        cache: false,
    }).done(function () {
        getAlerts();
    });
}

function unreadListHtml(content) {
    if (unreadAlerts.length == 0) {
        $('#unreadAlertsControls').html('');
        $('#unreadAlertsList').html('<h1>No unread alerts</h1>');
    } else {
        $('#unreadAlertsControls').html(renderAlertControls());
        $('#unreadAlertsList').html(content);
    }
}

function allAlertListHtml(content) {
    if (allAlerts.length == 0) {
        $('#allAlertsControls').html('');
        $('#allAlertsList').html('<h1>No alerts</h1>');
    } else {
        $('#allAlertsControls').html(renderAlertControls());
        $('#allAlertsList').html(content);
    }
}

function deleteAlert(e) {
    let target = e.target;

    if (target.hasAttribute('deleteid')) {
        let thisId = $(target).attr('deleteid');
        let thisAlert = [];
        thisAlert.push(thisId);
        console.log(`deleting alert ${thisId}`);
        
        $.ajax({
            type: "POST",
            data: JSON.stringify(thisAlert),
            url: window.location.origin + "/Admin/DeleteAlerts",
            contentType: "application/json; charset=utf-8",
            processData: true,
            cache: false,
        }).done(function () {
            getAlerts();
        });
    }
}

function renderAlertControls() {
    return (
        '<div class="col-2"><a class="text-primary text-link" action="read">Read All</a></div><div class="col-2"><a class="text-primary text-link" action="delete">Delete All</a></div>'
    );
}
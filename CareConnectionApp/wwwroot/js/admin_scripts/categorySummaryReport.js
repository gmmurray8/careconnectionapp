﻿let jobArray = [];
$('#jobReport').submit(function (e) {
    e.preventDefault();
    showLoader(true);
    let url = $(this).attr('action');
    $.getJSON(url, $(this).serialize(), function (data) {
        if ($('input[name=LowerBound]').val() > $('input[name=UpperBound]').val()) {
            alert('Please enter a start date that is before the end date.')
            showLoader(false);
            return;
        }
        $('#categoryHeader').html(data.jobType);
        showLoader(false);
        createTable(data);
        exportReport(data);
    });
});

function showLoader(loading) {
    if (loading) {
        $('#loaderRow').removeClass('d-none');
        $('#loaderDiv').html('<div class="spinner-border text-primary float-center ml-5" style="width:5rem; height:5rem"role="status"><span class="sr-only"> Loading...</span></div>');
    } else {
        $('#loaderDiv').html('');
        $('#loaderRow').addClass('d-none');
    }
}

function createTable(data) {
    let rows = '';
    rows = `<tr><td>${data.numOfUniqueClients}</td><td>${data.numOfTypes}</td><td>${data.numOfTypeActive}</td><td>${data.numOfTypeCompleted}</td><td>${data.numOfTypeDeferred}</td><td>${data.numOfTypeInactive}</td><td>${data.donations.toFixed(2)}</td><td>${data.expenses.toFixed(2)}</td><td>${data.hours}</td><td>${data.miles}</td></tr>`;
    $('#reportTable').html(`<thead class="bg-primary text-white"><tr><td>Unique Clients</td><td>Work Orders</td><td>Active</td><td>Completed</td><td>Deferred</td><td>Inactive</td><td>Donations</td><td>Expenses</td><td>Hours</td><td>Miles</td></tr></thead><tbody>${rows}</tbody>`);
}

function setJobArray(data) {
    jobArray = [];
    $.each(data, (i, e) => {
        if (i !== 'jobType') {
            jobArray.push([i, e]);
        }
    });
}

function exportReport(data) {
    setJobArray(data);
    let btn = $('#exportLink');
    btn.removeClass('disabled');
    btn.attr('onclick', 'createCSV()');
}

function createCSV() {
    let csv = `Work Order Statistics\n`;
    $.each(jobArray, function (i, e) {
        csv += e.join(',');
        csv += '\n';
    });
    let hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'report.csv'
    hiddenElement.click();
}
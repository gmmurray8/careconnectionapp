﻿const statsTableHeader = '<thead class="bg-primary text-white"><tr><td>Active Clients</td><td>Total Clients</td><td>Avg Work Orders</td></tr></thead>';
const mostWorkOrdersTableHeader = '<thead class="bg-primary text-white"><tr><td>Name</td><td>Work Orders</td><td>Actions</td></tr></thead>';
let clientsArray = [];
let mostWorkOrdersArray = [];
function generateFullReport() {
    showLoader(true);
    // Post to admin controller for JSON 
    return $.getJSON(window.location.origin + '/Admin/Reporting/GenerateClientsReport').done(function (data) {
        console.log('Successfully post server for JSON');
        createStatsTable(data);
        createMostWorkOrdersTable(data);
        exportReport(data);
    });
}

function showLoader(loading) {
    if (loading) {
        $('#loaderRow').removeClass('d-none');
        $('#loaderDiv').html('<div class="spinner-border text-primary float-center ml-5" style="width:5rem; height:5rem"role="status"><span class="sr-only"> Loading...</span></div>');
    } else {
        $('#loaderDiv').html('');
        $('#loaderRow').addClass('d-none');
    }
}

function createStatsTable(data) {
    let row = '<tr>';
    row += `<td>${data.totalClients}</td><td>${data.activeClients}</td><td>${data.avgWorkOrders.toFixed(2)}</td>`;
    showLoader(false);
    $('#statsTable').html(`${statsTableHeader}<tbody>${row}</tr></tbody>`);
    $('#statsHeader').html('Client Statistics');
}

function createMostWorkOrdersTable(data) {
    let row = '';
    $.each(data.mostWorkOrders, function (i, e) {
        row += `<tr><td>${e.name}</td>`;
        row += `<td>${e.numRequests}</td>`;
        row += `<td><a class="text-link" onclick="getIndividualReport(${e.id}, '${e.name}')">Details</a></td></tr>`;
    });
    $('#workOrdersTable').html(`${mostWorkOrdersTableHeader}<tbody>${row}</tbody>`);
    $('#workOrdersHeader').html('Most Work Orders');
}

function getIndividualReport(clientId, name) {
    $.getJSON(window.location.origin + '/Admin/Reporting/GenerateIndividualClientReport?clientId=' + clientId, function (data) {
        if (!validateResults(data)) {
            return false;
        }
        createIndividualTable(data);
        $('#individualClientName').html(name);
        $('#individualClientToggle').html('Hide');
    });
}

function validateResults(data) {
    if (data == 0) {
        alert('That client could not be found');
        return false;
    } else {
        return true;
    }
}

function createIndividualTable(data) {
    let rows = '';
    $.each(data, function (i, e) {
        if (e !== 0) {
            rows += `<tr><td>${i}</td><td>${e}</td></tr>`
        }
    });
    $('#individualTable').html(`<thead class="bg-primary text-white"><tr><td>Category</td><td>Value</td></tr></thead><tbody>${rows}</tbody>`);
}

function exportReport(data) {
    setClientsArray(data);
    setMostWorkOrdersArray(data);
    let btn = $('#exportLink');
    btn.removeClass('disabled');
    btn.attr('onclick', 'createCSV()');
}

function setClientsArray(data) {
    clientsArray = [];
    clientsArray.push(['Total Clients', 'Active Clients', 'Avg Work Orders']);
    clientsArray.push([data.totalClients, data.activeClients, data.avgWorkOrders.toFixed(2)]);
}

function setMostWorkOrdersArray(data) {
    mostWorkOrdersArray = [];
    mostWorkOrdersArray.push(['Name', 'Work Orders']);
    $.each(data.mostWorkOrders, function (i, e) {
        mostWorkOrdersArray.push([e.name, e.numRequests]);
    });
}

function createCSV() {
    let csv = 'All Client Statistics\n';
    $.each(clientsArray, function (i, e) {
        csv += e.join(',');
        csv += '\n';
    });
    csv += 'Clients With the Most Work Orders\n';
    $.each(mostWorkOrdersArray, function (i, e) {
        csv += e.join(',');
        csv += '\n';
    });
    let hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'report.csv'
    hiddenElement.click();
}

function hideIndividualClient() {
    $('#individualTable').html('');
    $('#individualClientName').html('');
    $('#individualClientToggle').html('');
}
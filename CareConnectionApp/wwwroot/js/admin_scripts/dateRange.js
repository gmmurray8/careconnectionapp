﻿let categoriesArray = [];
let statsArray = [];
let submitted = false;
function resetGraph() {
    categoriesArray = [];
    statsArray = [];
    location = location;
}
$(function () {
    $('#dateRangeReport').submit(function (e) {
        e.preventDefault();
        showLoader(true);
        if ($('input[name=lower]').val() > $('input[name=upper]').val()) {
            alert('Please enter a start date that is before the end date.');
            showLoader(false);
            return;
        }
        let url = $(this).attr('action');
        $.getJSON(url, $(this).serialize(), function (data) {
            let categoryData = parseReport(data);
            showLoader(false);
            generatePieChart(categoryData);
            createSummary(data);
            exportReport(data);
        });
    });
});

function showLoader(loading) {
    if (loading) {
        $('#loaderRow').removeClass('d-none');
        $('#loaderDiv').html('<div class="spinner-border text-primary float-center ml-5" style="width:5rem; height:5rem"role="status"><span class="sr-only"> Loading...</span></div>');
    } else {
        $('#loaderDiv').html('');
        $('#loaderRow').addClass('d-none');
    }
}

function generatePieChart(categoryData) {
    let startDate = $('#lower').val();
    let endDate = $('#upper').val();
    let data = [{
        values: Object.values(categoryData),
        labels: Object.keys(categoryData),
        type: 'pie',
        automargin: true,
        hoverinfo: 'label+percent+value',
        textinfo: 'label+percent',
        textposition: 'outside'
    }];

    let layout = {
        title: `Work Requests by Category from ${startDate} to ${endDate}`,
        showlegend: true
    };

    Plotly.newPlot('pieChart', data, layout);
}

function parseReport(data) {
    let categories = {};
    $.each(data.categoriesReport, function (i, e) {
        categories[e.category] = (categories[e.category] || 0) + e.count;
    });
    let result = `<ul><li>${data.numOfUniqueClients}</li><li>${data.numOfJobsCompleted}</li><li>` +
        `${data.numOfJobsDeferred}</li><li>${data.donations}</li><li>${data.expenses}</li><li>` +
        `${data.miles}</li><li>${data.hours}</li></ul>`;
    $('#rawData').html(result);
    return categories;
}

function createSummary(data) {    
    let numClients = `<div class="card text-white bg-primary"><div class="card-body"><h5 class="card-title">Unique Clients</h5><p class="card-text">${data.numOfUniqueClients}</p></div></div>`;
    let numCompleted = `<div class="card text-white bg-primary"><div class="card-body"><h5 class="card-title">Jobs Completed</h5><p class="card-text">${data.numOfJobsCompleted}</p></div></div>`;
    let numDeferred = `<div class="card text-white bg-primary"><div class="card-body"><h5 class="card-title">Jobs Deferred</h5><p class="card-text">${data.numOfJobsDeferred}</p></div></div>`;
    let donations = `<div class="card text-white bg-success"><div class="card-body"><h5 class="card-title">Donations</h5><p class="card-text">$${data.donations}</p></div></div>`;
    let expenses = `<div class="card text-white bg-danger"><div class="card-body"><h5 class="card-title">Expenses</h5><p class="card-text">$${data.expenses}</p></div></div>`;
    let miles = `<div class="card text-white bg-primary"><div class="card-body"><h5 class="card-title">Miles</h5><p class="card-text">${data.miles}</p></div></div>`;
    let hours = `<div class="card text-white bg-primary"><div class="card-body"><h5 class="card-title">Hours</h5><p class="card-text">${data.hours}</p></div></div>`;
    let result = numClients + numCompleted + numDeferred + miles + hours + donations + expenses;

    $('#summaryDiv').html(result);
}

function exportReport(data) {
    setCategoriesArray(data);
    setStatsArray(data);
    let btn = $('#exportLink');
    btn.removeClass('disabled');
    btn.attr('onclick', 'createCSV()');
}

function setCategoriesArray(data) {
    categoriesArray = [];
    $.each(data.categoriesReport, (i, e) => {
        categoriesArray.push([e.category, e.count]);
    });
}

function setStatsArray(data) {
    statsArray = [];
    statsArray.push(['Clients', 'Completed', 'Deferred', 'Donations', 'Expenses', 'Miles', 'Hours']);
    statsArray.push([data.numOfUniqueClients, data.numOfJobsCompleted, data.numOfJobsDeferred, data.donations, data.expenses, data.miles, data.hours]);
}

function createCSV() {
    let csv = `Work Requests by Category from ${$('#lower').val()} to ${$('#upper').val()}\n`;
    $.each(categoriesArray, function (i, e) {
        csv += e.join(',');
        csv += '\n';
    });
    csv += 'Overall Statistics\n';
    $.each(statsArray, function (i, e) {
        csv += e.join(',');
        csv += '\n';
    });
    let hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'report.csv'
    hiddenElement.click();
}

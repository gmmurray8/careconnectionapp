﻿function checkAdvancedSearch() {
    const idField = $('input[name=ClientID]').val();
    if (idField) {
        $('#idTabContent').toggleClass("active");
        $('#idTab').toggleClass("active");
    } else {
        $('#detailedTabContent').toggleClass("active");
        $('#detailedTab').toggleClass("active");  
    }
}
﻿let firstSwitch = true;
function sortStringInTable(tableId, n, postChanges = false, category = undefined) {
    let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;

    dir = 'asc';

    while (switching) {

        switching = false;
        rows = table.rows;

        for (i = 1; i < (rows.length - 1); i++) {

            shouldSwitch = false;

            x = rows[i].getElementsByTagName('TD')[n];
            y = rows[i + 1].getElementsByTagName('TD')[n];

            let cleanX = x
                .innerText
                .toLowerCase()
                .replace(/[0-9]/g, '');
            let cleanY = y
                .innerText
                .toLowerCase()
                .replace(/[0-9]/g, '');

            if (dir == 'asc') {
                if (cleanX > cleanY) {

                    shouldSwitch = true;
                    break;
                }
            } else if (dir == 'desc') {
                if (cleanX < cleanY) {

                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {

            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;

            switchcount++;
        } else {

            if (switchcount == 0 && dir == 'asc') {
                dir = 'desc';
                switching = true;
            }
        }
    }
    if (postChanges) {
        $('.sortBy').val(category);
        $('.sortOrder').val(dir);
    }
}

function sortNumberInTable(tableId, n, postChanges = false, category = undefined) {
    let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;

    dir = (category === 'jobCount') && firstSwitch ? 'desc' : 'asc';

    while (switching) {

        switching = false;
        rows = table.rows;

        for (i = 1; i < (rows.length - 1); i++) {

            shouldSwitch = false;

            x = rows[i].getElementsByTagName('TD')[n];
            y = rows[i + 1].getElementsByTagName('TD')[n];

            if (dir == 'asc') {
                if (Number(x.innerText) > Number(y.innerText)) {

                    shouldSwitch = true;
                    break;
                }
            } else if (dir == 'desc') {
                if (Number(x.innerText) < Number(y.innerText)) {

                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {

            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;

            switchcount++;
        } else {

            if (switchcount == 0 && dir == 'asc') {
                dir = 'desc';
                switching = true;
            }
        }
    }
    if (postChanges) {
        $('.sortBy').val(category);
        $('.sortOrder').val(dir);
    }
    firstSwitch = false;
}

function sortDateInTable(tableId, n, postChanges = false, category = undefined) {
    let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById(tableId);
    switching = true;

    dir = 'asc';

    while (switching) {

        switching = false;
        rows = table.rows;

        for (i = 1; i < (rows.length - 1); i++) {

            shouldSwitch = false;

            x = rows[i].getElementsByTagName('TD')[n];
            y = rows[i + 1].getElementsByTagName('TD')[n];

            let dateX = new Date(x.innerHTML);
            let dateY = new Date(y.innerHTML);


            if (dir == 'asc') {
                if (dateX > dateY) {

                    shouldSwitch = true;
                    break;
                }
            } else if (dir == 'desc') {
                if (dateX < dateY) {

                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {

            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;

            switchcount++;
        } else {

            if (switchcount == 0 && dir == 'asc') {
                dir = 'desc';
                switching = true;
            }
        }
    }
    if (postChanges) {
        $('.sortBy').val(category);
        $('.sortOrder').val(dir);
    }
}
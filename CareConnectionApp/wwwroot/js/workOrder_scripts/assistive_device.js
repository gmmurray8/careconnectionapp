﻿

$(function () {
    $('#deviceCategory').change(function (e) {
        let catId = $(this).children("option:selected").val();
        getDevicesByCat(e, catId);
    });
});

function getDevicesByCat(e, catId) {
    $.getJSON('/Clients/GetDevices?catId=' + catId, function (data) {
        console.log("Successful post to the server.");
        createSelectList(data);
    });
}

function createSelectList(data) {
    let dropdown = $('#deviceDropdown');
    dropdown.empty();
    dropdown.append('<option selected="true" disabled>Choose A Device</option>');
    dropdown.prop('selectedIndex', 0);
    $.each(data, function (i, e) {
        dropdown.append($('<option></option>').attr('value', e.id).text(e.name));
    })

}
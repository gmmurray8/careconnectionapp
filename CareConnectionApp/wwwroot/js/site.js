﻿$(document).ready(function () {
    $(".dropdown-toggle").dropdown();
});


window.onscroll = function () { fadeTopBtn() };

function goToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}

function fadeTopBtn() {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 300)
        $("#topBtn").fadeIn();
    else
        $("#topBtn").fadeOut();
}

let pageNum = document.getElementById('PageNum')?.value;
if (pageNum > 1) {
    $(document).scrollTop($(document).height())
}

$('.loadingForm').submit(function (e) {
    if ($(this).valid()) {
        let button = $("button[type=submit]", $(this));
        button.prop('disabled', true);
        button.addClass('disabled');
        button.html('<span class="spinner-border-sm spinner-border" role="status" aria-hidden="true"></span>Loading <span class="sr-only"></span>');
    }
});
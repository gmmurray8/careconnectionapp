﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CareConnectionApp.Migrations
{
    public partial class eheapdecimalprecision : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Expenses",
                table: "Eheaps",
                type: "decimal(18,4)",
                nullable: false,
                oldClrType: typeof(decimal));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Expenses",
                table: "Eheaps",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)");
        }
    }
}

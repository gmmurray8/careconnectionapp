﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CareConnectionApp.Migrations
{
    public partial class IsBaseCategoryaddedtocategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsBaseCategory",
                table: "WorkOrderCategories",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsBaseCategory",
                table: "WorkOrderCategories");
        }
    }
}

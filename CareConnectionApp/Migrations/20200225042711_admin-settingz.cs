﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CareConnectionApp.Migrations
{
    public partial class adminsettingz : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_WorkOrderCategories_Category",
                table: "WorkOrderCategories");

            migrationBuilder.AlterColumn<string>(
                name: "Category",
                table: "WorkOrderCategories",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Expenses",
                table: "Eheaps",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "CaseManager",
                table: "Clients",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "AssistiveDeviceCategories",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "AdminSettings",
                columns: table => new
                {
                    ApplicationUserId = table.Column<string>(nullable: false),
                    AlertLifeSpan = table.Column<int>(nullable: false),
                    AtAGlanceDuration = table.Column<int>(nullable: false),
                    DashboardCardHistoryCount = table.Column<int>(nullable: false),
                    ServiceListLimit = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdminSettings", x => x.ApplicationUserId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrderCategories_Category",
                table: "WorkOrderCategories",
                column: "Category",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AssistiveDeviceCategories_Name",
                table: "AssistiveDeviceCategories",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdminSettings");

            migrationBuilder.DropIndex(
                name: "IX_WorkOrderCategories_Category",
                table: "WorkOrderCategories");

            migrationBuilder.DropIndex(
                name: "IX_AssistiveDeviceCategories_Name",
                table: "AssistiveDeviceCategories");

            migrationBuilder.DropColumn(
                name: "Expenses",
                table: "Eheaps");

            migrationBuilder.DropColumn(
                name: "CaseManager",
                table: "Clients");

            migrationBuilder.AlterColumn<string>(
                name: "Category",
                table: "WorkOrderCategories",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "AssistiveDeviceCategories",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrderCategories_Category",
                table: "WorkOrderCategories",
                column: "Category",
                unique: true,
                filter: "[Category] IS NOT NULL");
        }
    }
}

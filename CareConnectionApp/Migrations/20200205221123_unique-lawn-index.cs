﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CareConnectionApp.Migrations
{
    public partial class uniquelawnindex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_LawnServices_ZoneId",
                table: "LawnServices");

            migrationBuilder.CreateIndex(
                name: "IX_LawnServices_ZoneId",
                table: "LawnServices",
                column: "ZoneId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_LawnServices_ZoneId",
                table: "LawnServices");

            migrationBuilder.CreateIndex(
                name: "IX_LawnServices_ZoneId",
                table: "LawnServices",
                column: "ZoneId",
                unique: true);
        }
    }
}

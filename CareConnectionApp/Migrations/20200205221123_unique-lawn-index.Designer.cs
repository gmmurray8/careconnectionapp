﻿// <auto-generated />
using System;
using CareConnectionApp.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CareConnectionApp.Migrations
{
    [DbContext(typeof(CCADbContext))]
    [Migration("20200205221123_unique-lawn-index")]
    partial class uniquelawnindex
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CareConnectionApp.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<DateTimeOffset>("CreateDate");

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FullName")
                        .IsRequired();

                    b.Property<DateTimeOffset>("LastLoginDate");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("CareConnectionApp.Models.Client", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("BirthDay");

                    b.Property<string>("CellPhone");

                    b.Property<string>("City")
                        .IsRequired();

                    b.Property<int>("ClientStatus");

                    b.Property<DateTime>("CreationDate");

                    b.Property<int?>("EheapId");

                    b.Property<string>("EmergencyContactName");

                    b.Property<string>("EmergencyContactPhoneNumber");

                    b.Property<string>("EmergencyContactRelationship");

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<string>("HasPets");

                    b.Property<string>("HomePhone")
                        .IsRequired();

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<bool>("OwnerOfHome");

                    b.Property<string>("StreetAddress")
                        .IsRequired();

                    b.Property<bool>("Veteran");

                    b.Property<int>("Zip");

                    b.HasKey("Id");

                    b.HasIndex("EheapId");

                    b.ToTable("Clients");
                });

            modelBuilder.Entity("CareConnectionApp.Models.ClientRequest", b =>
                {
                    b.Property<int>("ClientId");

                    b.Property<int>("WorkOrderId");

                    b.HasKey("ClientId", "WorkOrderId");

                    b.HasIndex("WorkOrderId")
                        .IsUnique();

                    b.ToTable("ClientRequests");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.AdminFaveClient", b =>
                {
                    b.Property<string>("AdminId");

                    b.Property<int>("ClientId");

                    b.HasKey("AdminId", "ClientId");

                    b.ToTable("AdminFavorites");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.Alert", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("AdminId");

                    b.Property<string>("AlertText");

                    b.Property<int?>("AssistiveDeviceId");

                    b.Property<int?>("ClientId");

                    b.Property<bool>("Read");

                    b.Property<DateTime>("TimeStamp");

                    b.Property<int?>("WorkOrderId");

                    b.HasKey("Id");

                    b.ToTable("Alerts");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.AssistiveDevice", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CategoryId");

                    b.Property<string>("Description");

                    b.Property<bool>("IsActive");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.ToTable("AssistiveDevices");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.AssistiveDeviceCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description");

                    b.Property<bool>("IsActive");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("AssistiveDeviceCategories");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.ClientLawnService", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ClientId");

                    b.Property<DateTime>("TimeStamp");

                    b.Property<int>("ZoneId");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.ToTable("ClientLawnServices");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.LawnService", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("TimeStamp");

                    b.Property<int>("ZoneId");

                    b.HasKey("Id");

                    b.HasIndex("ZoneId");

                    b.ToTable("LawnServices");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.LawnServiceList", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ClientId");

                    b.Property<DateTime>("TimeStamp");

                    b.Property<int>("WorkOrderId");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.HasIndex("WorkOrderId");

                    b.ToTable("LawnServiceList");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.PendingApproval", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ClientId");

                    b.Property<DateTime>("TimeStamp");

                    b.Property<int>("WorkOrderId");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.HasIndex("WorkOrderId");

                    b.ToTable("PendingApproval");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.UserClientAdditions", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<int>("ClientId");

                    b.Property<DateTime>("CreationDate");

                    b.HasKey("UserId", "ClientId");

                    b.HasIndex("ClientId");

                    b.ToTable("UserClientAdditons");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.WaitList", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ClientId");

                    b.Property<DateTime>("TimeStamp");

                    b.Property<int>("WorkOrderId");

                    b.HasKey("Id");

                    b.HasIndex("ClientId");

                    b.HasIndex("WorkOrderId");

                    b.ToTable("WaitList");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.WorkOrderCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Category");

                    b.Property<bool>("IsActive");

                    b.Property<bool>("IsBaseCategory");

                    b.HasKey("Id");

                    b.HasIndex("Category")
                        .IsUnique()
                        .HasFilter("[Category] IS NOT NULL");

                    b.ToTable("WorkOrderCategories");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.Zone", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Zones");
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.ZoneJoining", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ClientId");

                    b.Property<int>("ZoneId");

                    b.HasKey("Id");

                    b.HasIndex("ClientId")
                        .IsUnique();

                    b.HasIndex("ZoneId");

                    b.ToTable("ZoneJoinings");
                });

            modelBuilder.Entity("CareConnectionApp.Models.Eheap", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DateOfService");

                    b.HasKey("Id");

                    b.ToTable("Eheaps");
                });

            modelBuilder.Entity("CareConnectionApp.Models.RampRequest", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("ContactedVA");

                    b.Property<string>("LengthOfHomeOwnership")
                        .IsRequired();

                    b.Property<bool>("LiveAlone");

                    b.Property<int>("MobilityAssistance");

                    b.Property<string>("MobilityDescription")
                        .IsRequired();

                    b.Property<bool>("OwnerOfHome");

                    b.Property<bool>("PermanentMobilityImpairment");

                    b.HasKey("Id");

                    b.ToTable("RampRequests");
                });

            modelBuilder.Entity("CareConnectionApp.Models.WorkOrder", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AssistiveDeviceId");

                    b.Property<DateTime>("DateOfRequest");

                    b.Property<int>("JobStatus");

                    b.Property<int?>("RampRequestId");

                    b.Property<string>("ReqDescription")
                        .IsRequired();

                    b.Property<string>("SecondaryContact");

                    b.Property<string>("SecondaryContactNumber");

                    b.Property<int>("WorkOrderCategoryId");

                    b.Property<int?>("WorkOrderCompletionFormId");

                    b.HasKey("Id");

                    b.HasIndex("RampRequestId");

                    b.HasIndex("WorkOrderCategoryId");

                    b.HasIndex("WorkOrderCompletionFormId");

                    b.ToTable("WorkOrders");
                });

            modelBuilder.Entity("CareConnectionApp.Models.WorkOrderCompletionForm", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Comments");

                    b.Property<DateTime>("DateAccepted");

                    b.Property<DateTime>("DateCompleted");

                    b.Property<DateTime>("DateStarted");

                    b.Property<decimal>("Donation")
                        .HasColumnType("decimal(18,4)");

                    b.Property<decimal>("Expenses")
                        .HasColumnType("decimal(18,4)");

                    b.Property<float>("HoursWorked");

                    b.Property<int>("JobReferral");

                    b.Property<int>("Mileage");

                    b.Property<string>("Volunteers");

                    b.HasKey("Id");

                    b.ToTable("workOrderCompletionForms");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasMaxLength(128);

                    b.Property<string>("ProviderKey")
                        .HasMaxLength(128);

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider")
                        .HasMaxLength(128);

                    b.Property<string>("Name")
                        .HasMaxLength(128);

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("CareConnectionApp.Models.Client", b =>
                {
                    b.HasOne("CareConnectionApp.Models.Eheap", "Eheap")
                        .WithMany()
                        .HasForeignKey("EheapId");
                });

            modelBuilder.Entity("CareConnectionApp.Models.ClientRequest", b =>
                {
                    b.HasOne("CareConnectionApp.Models.Client", "Client")
                        .WithMany("ClientRequests")
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CareConnectionApp.Models.WorkOrder", "WorkOrder")
                        .WithOne("ClientRequest")
                        .HasForeignKey("CareConnectionApp.Models.ClientRequest", "WorkOrderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.AssistiveDevice", b =>
                {
                    b.HasOne("CareConnectionApp.Models.DbEntities.AssistiveDeviceCategory", "Category")
                        .WithMany("AssistiveDevices")
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.ClientLawnService", b =>
                {
                    b.HasOne("CareConnectionApp.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.LawnService", b =>
                {
                    b.HasOne("CareConnectionApp.Models.DbEntities.Zone")
                        .WithMany("LawnService")
                        .HasForeignKey("ZoneId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.LawnServiceList", b =>
                {
                    b.HasOne("CareConnectionApp.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CareConnectionApp.Models.WorkOrder", "WorkOrder")
                        .WithMany()
                        .HasForeignKey("WorkOrderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.PendingApproval", b =>
                {
                    b.HasOne("CareConnectionApp.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CareConnectionApp.Models.WorkOrder", "WorkOrder")
                        .WithMany()
                        .HasForeignKey("WorkOrderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.UserClientAdditions", b =>
                {
                    b.HasOne("CareConnectionApp.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CareConnectionApp.Models.ApplicationUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.WaitList", b =>
                {
                    b.HasOne("CareConnectionApp.Models.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CareConnectionApp.Models.WorkOrder", "WorkOrder")
                        .WithMany()
                        .HasForeignKey("WorkOrderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CareConnectionApp.Models.DbEntities.ZoneJoining", b =>
                {
                    b.HasOne("CareConnectionApp.Models.Client", "Client")
                        .WithOne("ZoneJoining")
                        .HasForeignKey("CareConnectionApp.Models.DbEntities.ZoneJoining", "ClientId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CareConnectionApp.Models.DbEntities.Zone", "Zone")
                        .WithMany()
                        .HasForeignKey("ZoneId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("CareConnectionApp.Models.WorkOrder", b =>
                {
                    b.HasOne("CareConnectionApp.Models.RampRequest", "RampRequest")
                        .WithMany()
                        .HasForeignKey("RampRequestId");

                    b.HasOne("CareConnectionApp.Models.DbEntities.WorkOrderCategory", "WorkOrderCategory")
                        .WithMany()
                        .HasForeignKey("WorkOrderCategoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CareConnectionApp.Models.WorkOrderCompletionForm", "WorkOrderCompletionForm")
                        .WithMany()
                        .HasForeignKey("WorkOrderCompletionFormId");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("CareConnectionApp.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("CareConnectionApp.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CareConnectionApp.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("CareConnectionApp.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}

﻿using CareConnectionApp.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CareConnectionApp.DataAccess.Abstractions
{
    public interface IAlertDataAccess
    {
        void AddAlert(Alert alert);
        void MarkAlertAsRead(IEnumerable<int> alertIds);
        IEnumerable<Alert> GetAlerts(string AdminId);
        Task PruneAlerts(ApplicationUser Admin, DateTime deleteDate);
        void DeleteAlerts(IEnumerable<int> alertIds);
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using CareConnectionApp.ViewModels;
using CareConnectionApp.ViewModels.Clients;

namespace CareConnectionApp.DataAccess.Abstractions
{
    public interface IClientDataAccess
    {
        /// <summary>
        /// Adds given Client objet to the database
        /// </summary>
        /// <param name="client"></param>
        /// <returns>Client object that was added</returns>
        Client AddClient(Client client);

        /// <summary>
        /// Adds given Eheap objet to the database
        /// </summary>
        /// <paramref name="a"/>
        /// <param name="model"></param>
        /// <returns>Eheap object that was added</returns>
        Eheap AddEheap(Eheap model);

        /// <summary>
        /// Adds given RampRequest object to the database
        /// </summary>
        /// <param name="model"></param>
        /// <returns>RampRequest object that was added</returns>
        RampRequest AddRampRequest(RampRequest model);

        /// <summary>
        /// Adds given WorkOrder object to the database 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="clientId"></param>
        /// <returns>WorkOrder object that was added</returns>
        WorkOrder AddWorkOrder(WorkOrder model, int clientId);


        /// <summary>
        /// Finds client in database by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Client that was found, null if no client was found</returns>
        Client GetClient(int id);

        /// <summary>
        /// Gets Client Objects in the database along with their related WorkOrders
        /// </summary>
        /// <returns>IEnumerable of Client objects</returns>
        IEnumerable<Client> GetClients();

        /// <summary>
        /// Gets work orders for given client with given ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="searchType"></param>
        /// <param name="jobStatus"></param>
        /// <returns>IEnumerable of ClientRequest objects </returns>
        IEnumerable<ClientRequest> GetClientWorkOrders(int id, string searchType = "Any", JobStatus jobStatus = JobStatus.Active);
        IEnumerable<WorkOrder> GetWorkOrdersByClient(int id);
        /// <summary>
        /// Gets Eheap by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Eheap object or null if not found</returns>
        Eheap GetEheap(int? id);

        Eheap UpdateEheap(Eheap eheap);

        /// <summary>
        /// Gets RampRequest by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>RampRequest object or null if not found</returns>
        RampRequest GetRampRequest(int? id);

        /// <summary>
        /// Gets WorkOrder by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>WorkOrder object or null if not found</returns>
        WorkOrder GetWorkOrder(int id);

        /// <summary>
        /// Gets WorkOrder by ID and includes client information
        /// </summary>
        /// <param name="id"></param>
        /// <returns>WorkOrder object or null if not found</returns>
        WorkOrder GetWorkOrderWithClient(int id);

        /// <summary>
        /// This feature has not been implemented yet
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Client RemoveClient(int id);

        /// <summary>
        /// Searches clients by FirstName or LastName
        /// </summary>
        /// <param name="searchClient"></param>
        /// <param name="clientStatus"></param>
        /// <returns>IEnumerable of Client objects</returns>
        //IQueryable<IGrouping<Client, int>> SearchClients(SearchClientViewModel search, ClientStatus clientStatus = ClientStatus.Active);
        IQueryable<Client> GetClientsNoTracking();

        /// <summary>
        /// Searches work orders by status and type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="jobStatus"></param>
        /// <param name="clientStatus"></param>
        /// <returns>IEnumerable of WorkOrder objects</returns>
        IQueryable<WorkOrder> SearchWorkOrders(string type, JobStatus jobStatus, ClientStatus clientStatus = ClientStatus.Active);

        /// <summary>
        /// Update client details in the database
        /// </summary>
        /// <param name="clientChanges"></param>
        /// <returns>Updated client object</returns>
        Client UpdateClient(Client clientChanges);

        /// <summary>
        /// Add WorkOrderCompletionForm to the database
        /// </summary>
        /// <param name="workOrderCompletionForm"></param>
        /// <returns>WorkOrderCompletionForm that was added to the database</returns>
        WorkOrderCompletionForm AddWorkOrderCompletionForm(WorkOrderCompletionForm workOrderCompletionForm);

        /// <summary>
        /// Get WorkOrderCompletionForm by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>WorkOrderCompletionForm object or null if not found</returns>
        WorkOrderCompletionForm GetWorkOrderCompletionForm(int id);

        /// <summary>
        /// Update WorkOrderCompletionForm details in the database
        /// </summary>
        /// <param name="workOrderCompletionFormChanges"></param>
        /// <returns>Updated WorkOrderCompletionForm object</returns>
        WorkOrderCompletionForm UpdateWorkOrderCompletionForm(WorkOrderCompletionForm workOrderCompletionFormChanges);

        /// <summary>
        /// Update WorkOrder details in the database
        /// </summary>
        /// <param name="workOrderChanges"></param>
        /// <returns>Updated WorkOrder object</returns>
        WorkOrder UpdateWorkOrder(WorkOrder workOrderChanges);
        AssistiveDevice GetAssistiveDevice(int? id);

        UserClientAdditions AddUserClientAddition(UserClientAdditions model);

        IEnumerable<UserClientAdditions> GetUserClientAdditions(string userId);
        AdminFaveClient AddAdminFaveClient(AdminFaveClient afc);
        AdminFaveClient SearchAdminFave(int clientId, string adminId);
        AdminFaveClient RemoveAdminFave(int clientId, string adminId);
        List<Client> GetAdminFaveClients(string adminId);
        List<int> GetAdminFaveIDs(string adminId);
        IEnumerable<AssistiveDeviceCategory> GetActiveAssistiveDeviceCategories();
        IEnumerable<AssistiveDevice> GetActiveAssistiveDevices();
        IEnumerable<AssistiveDevice> GetAssistiveDevices(int CategoryId);
        IEnumerable<AssistiveDevice> GetActiveAssistiveDevices(int CategoryId);
        List<WorkOrderCategory> GetWorkOrderCategories();
        int GetWorkOrderCategoryId(string cat);
        WorkOrderCategory GetWorkOrderCategory(int id);
        IEnumerable<WorkOrder> GetWorkOrders();
        RampRequest UpdateRampRequest(RampRequest ramp);

        PendingApproval AddToPendingList(PendingApproval pendingApproval);
        IEnumerable<PendingApproval> GetPendingApproval();
        IEnumerable<WaitList> GetWaitList();
        IEnumerable<LawnServiceList> GetLawnServiceList();
        IEnumerable<LawnService> GetLawnServices();
        IEnumerable<Zone> GetZones();
        IEnumerable<ClientLawnService> GetClientLawnServices();
    }
}
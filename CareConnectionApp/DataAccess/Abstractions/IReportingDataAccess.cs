﻿using CareConnectionApp.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CareConnectionApp.DataAccess.Abstractions
{
    public interface IReportingDataAccess
    {
        Task<List<WorkOrder>> GetWorkOrders(Expression<Func<WorkOrder, bool>> filter);
        List<string> GetWorkOrderCategoryNames();
        List<WorkOrder> GetWorkOrdersByClient(int id);
        IEnumerable<ClientLawnService> GetClientLawnServices();
        IEnumerable<LawnService> GetLawnServices();
    }
}

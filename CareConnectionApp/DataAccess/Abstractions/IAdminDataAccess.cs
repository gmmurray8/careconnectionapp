﻿using CareConnectionApp.Models;
using System.Collections.Generic;

namespace CareConnectionApp.DataAccess.Abstractions
{
    public interface IAdminDataAccess
    {
        List<WorkOrderCategory> GetWorkOrderCategories();
        WorkOrderCategory AddWorkOrderCategory(WorkOrderCategory workOrderCategory);
        WorkOrderCategory GetWorkOrderCategory(int Id);
        WorkOrderCategory UpdateWorkOrderCategory(WorkOrderCategory workOrderCatChanges);
        bool DeleteWorkOrderCategory(int workOrderCategory);
        AssistiveDevice AddAssistiveDevice(AssistiveDevice device);
        AssistiveDevice GetAssistiveDevice(int? id);
        AssistiveDeviceCategory AddAssistiveDeviceCategory(AssistiveDeviceCategory category);
        AssistiveDeviceCategory GetAssistiveDeviceCategory(int id);
        AssistiveDevice UpdateAssistiveDevice(AssistiveDevice assistiveDeviceChanges);

        AssistiveDeviceCategory UpdateAssistiveDeviceCategory(AssistiveDeviceCategory updateCategory);
        IEnumerable<AssistiveDevice> GetAssistiveDevices();
        IEnumerable<AssistiveDeviceCategory> GetAssistiveDeviceCategories();
        IEnumerable<PendingApproval> GetPendingApprovals();
        void DeleteFromPendingApprovals(PendingApproval pendingApproval);
        PendingApproval GetPendingApproval(int id);
        WaitList AddToWaitList(WaitList waitList);
        LawnServiceList AddToLawnServiceList(LawnServiceList lawnServiceList);
        List<LawnServiceList> AddToLawnServiceList(List<LawnServiceList> lawnServices);
        void DeleteFromWaitList(WaitList waitList);
        void DeleteFromWaitList(List<WaitList> waitLists);
        void DeleteFromServiceList(LawnServiceList lawnServiceList);
        Zone AddZone(Zone zone);
        Zone UpdateZone(Zone zone);
        IEnumerable<Zone> GetZones();
        ZoneJoining AddZoneJoin(ZoneJoining zoneJoining);
        ZoneJoining UpdateZoneJoin(ZoneJoining zoneJoin);
        IEnumerable<ZoneJoining> GetZoneJoins();
        LawnService AddLawnService(LawnService lawnService);
        IEnumerable<WorkOrder> CompleteLawnServices(IEnumerable<WorkOrder> workOrders);
        IEnumerable<WorkOrderCompletionForm> CompleteLawnServices(IEnumerable<WorkOrderCompletionForm> completionForms);
        List<WorkOrderCompletionForm> GetCompletionForms(int count);
        IEnumerable<ClientLawnService> AddClientLawnServices(IEnumerable<ClientLawnService> clsList);
        List<WaitList> AddToWaitList(List<WaitList> waitList);
        void DeleteFromPendingApprovals(List<PendingApproval> pendingApprovals);
        AdminSetting GetAdminSetting(string userId);
        AdminSetting SetAdminAlertSetting(string userId, int duration);
        AdminSetting SetAtAGlanceSetting(string userId, int duration);
        AdminSetting SetDashboardHistoryCount(string userId, int count);
        AdminSetting SetServiceListLimit(string userId, int count);
        AdminSetting UpdateAdminSetting(AdminSetting updatedAdminSetting);
    }
}

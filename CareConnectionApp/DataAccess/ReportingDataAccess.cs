﻿using CareConnectionApp.DataAccess.Abstractions;
using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CareConnectionApp.DataAccess
{
    public class ReportingDataAccess : IReportingDataAccess
    {
        private readonly CCADbContext _context;

        public ReportingDataAccess
        (
            CCADbContext context
        )
        {
            _context = context;
        }

        public async Task<List<WorkOrder>> GetWorkOrders(Expression<Func<WorkOrder, bool>> filter)
        {
            var workOrders = await _context.WorkOrders
                .Include(wo => wo.WorkOrderCompletionForm)
                .Include(wo => wo.ClientRequest)
                .ThenInclude(cr => cr.Client)
                .Include(wo => wo.WorkOrderCategory)
                .Where(filter).ToListAsync();

            return workOrders;
        }

        public List<WorkOrder> GetWorkOrdersByClient(int id)
        {
            return _context.WorkOrders
                .Where(workOrder => workOrder.ClientRequest.ClientId == id)
                .Include(workOrder => workOrder.WorkOrderCategory)
                .Include(workOrder => workOrder.WorkOrderCompletionForm).ToList();
        }

        public List<string> GetWorkOrderCategoryNames()
        {
            var categories = _context.WorkOrderCategories.ToList();
            var categoryNames = new List<string>();

            foreach (var woc in categories)
            {
                categoryNames.Add(woc.Category);
            }

            return categoryNames;
        }

        public IEnumerable<ClientLawnService> GetClientLawnServices()
        {
            return _context.ClientLawnServices
                .Include(cls => cls.Client)
                .ThenInclude(c => c.ZoneJoining)
                .ThenInclude(zj => zj.Zone);
        }

        public IEnumerable<LawnService> GetLawnServices()
        {
            return _context.LawnServices;
        }
    }
}

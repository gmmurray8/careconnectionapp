﻿using CareConnectionApp.DataAccess.Abstractions;
using CareConnectionApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CareConnectionApp.DataAccess
{
    public class AlertDataAccess : IAlertDataAccess
    {
        private readonly CCADbContext _context;

        public AlertDataAccess
        (
            CCADbContext context
        )
        {
            _context = context;
        }

        public async Task PruneAlerts(ApplicationUser Admin, DateTime deleteDate)
        {
            var alertsToBeDeleted = _context.Alerts
                 .Select(a => a)
                 .Where(a => a.AdminId.Equals(Admin.Id) && a.TimeStamp < deleteDate)
                 .ToList();
            _context.Alerts.RemoveRange(alertsToBeDeleted);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<Alert> GetAlerts(string AdminId)
        {
            var alerts = _context.Alerts
                .Select(a => a)
                .Where(a => a.AdminId.Equals(AdminId))
                .ToList();

            return alerts;
        }

        public void AddAlert(Alert alert)
        {
            _context.Alerts.Add(alert);
            _context.SaveChanges();
        }

        public void MarkAlertAsRead(IEnumerable<int> alertIds)
        {
            var alerts = _context.Alerts.Select(a => a)
                .Where(a => alertIds.Any(ai => ai == a.Id))
                .ToList();

            foreach (var a in alerts)
            {
                a.Read = true;
            }

            _context.Alerts.AttachRange(alerts);
            _context.SaveChanges();
        }

        public void DeleteAlerts(IEnumerable<int> alertIds)
        {
            var alerts = _context.Alerts.Where(a => alertIds.Any(ai => ai == a.Id)).ToList();

            _context.Alerts.RemoveRange(alerts);
            _context.SaveChanges();
        }
    }
}

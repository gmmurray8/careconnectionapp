﻿using CareConnectionApp.DataAccess.Abstractions;
using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using CareConnectionApp.Utility_Classes;
using CareConnectionApp.ViewModels;
using CareConnectionApp.ViewModels.Clients;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CareConnectionApp.DataAccess
{
    public class ClientDataAccess : IClientDataAccess
    {
        private readonly CCADbContext _context;
        private readonly IPhoneNumberClass _phoneNumberClass;

        public ClientDataAccess
        (
            CCADbContext context,
            IPhoneNumberClass phoneNumberClass
        )
        {
            _context = context;
            _phoneNumberClass = phoneNumberClass;
        }

        #region Clients

        public Client AddClient(Client client)
        {
            _context.Clients.Add(client);
            _context.SaveChanges();
            return client;
        }

        public Client GetClient(int id)
        {
            return _context.Clients
                .Include(c => c.ZoneJoining)
                .ThenInclude(zj => zj.Zone)
                .FirstOrDefault(c => c.Id == id);
        }

        public IEnumerable<Client> GetClients()
        {
            var clients = _context.Clients.Include(c => c.ClientRequests)
                .ThenInclude(cr => cr.WorkOrder)
                .Include(c => c.ZoneJoining)
                .ThenInclude(zj => zj.Zone);
            return clients;
        }

        public IQueryable<Client> GetClientsNoTracking()
        {
            return _context.Clients.AsNoTracking().Include(c => c.ClientRequests)
                .ThenInclude(cr => cr.WorkOrder);
        }

        public Client RemoveClient(int id)
        {
            throw new NotImplementedException();
        }

        public Client UpdateClient(Client clientChanges)
        {
            var client = _context.Clients.Attach(clientChanges);
            client.State = EntityState.Modified;
            _context.SaveChanges();
            return clientChanges;
        }

        public UserClientAdditions AddUserClientAddition(UserClientAdditions model)
        {
            _context.Add(model);
            _context.SaveChanges();
            return model;
        }

        public IEnumerable<UserClientAdditions> GetUserClientAdditions(string userId)
        {
            var additions = _context.UserClientAdditons
                .Include(u => u.Client)
                .Where(u => u.UserId == userId);
            return additions;
        }

        #endregion

        #region Eheaps

        public Eheap AddEheap(Eheap model)
        {
            _context.Eheaps.Add(model);
            _context.SaveChanges();
            return model;
        }

        public Eheap GetEheap(int? id)
        {
            var eheap = _context.Eheaps.FirstOrDefault(e => e.Id == id);
            return eheap;
        }

        public Eheap UpdateEheap(Eheap eheap)
        {
            var eheapEntity = _context.Eheaps.Attach(eheap);
            eheapEntity.State = EntityState.Modified;
            _context.SaveChanges();
            return eheap;
        }

        #endregion

        #region Ramp Requests

        public RampRequest AddRampRequest(RampRequest model)
        {
            _context.RampRequests.Add(model);
            _context.SaveChanges();
            return model;
        }
        public RampRequest GetRampRequest(int? id)
        {
            return _context.RampRequests.FirstOrDefault(r => r.Id == id);
        }

        public RampRequest UpdateRampRequest(RampRequest ramp)
        {
            var rampEntity = _context.RampRequests.Attach(ramp);
            rampEntity.State = EntityState.Modified;
            _context.SaveChanges();
            return ramp;
        }

        #endregion

        #region Work Orders

        public WorkOrder AddWorkOrder(WorkOrder model, int clientId)
        {
            _context.WorkOrders.Add(model);
            var clientRequest = new ClientRequest
            {
                ClientId = clientId,
                WorkOrderId = model.Id
            };
            _context.Add(clientRequest);
            _context.SaveChanges();
            return model;
        }

        public IEnumerable<ClientRequest> GetClientWorkOrders(int id, string searchType = "Any", JobStatus jobStatus = JobStatus.Active)
        {
            var clientReqs = from cr in _context.ClientRequests
                             select cr;
            if (searchType == "Any")
            {
                clientReqs = _context.ClientRequests
                     .Include(cr => cr.Client)
                     .Include(cr => cr.WorkOrder)
                     .ThenInclude(wo => wo.RampRequest)
                     .Include(cr => cr.WorkOrder)
                     .ThenInclude(wo => wo.WorkOrderCategory)
                     .Where(cr => cr.WorkOrder.JobStatus.Equals(jobStatus) && cr.ClientId.Equals(id));

                return clientReqs;
            }
            else
            {
                clientReqs = _context.ClientRequests
                    .Include(cr => cr.Client)
                    .Include(cr => cr.WorkOrder)
                    .ThenInclude(wo => wo.RampRequest)
                    .Include(cr => cr.WorkOrder)
                    .ThenInclude(wo => wo.WorkOrderCategory)
                    .Where(cr => cr.WorkOrder.JobStatus.Equals(jobStatus) && cr.ClientId.Equals(id) && cr.WorkOrder.WorkOrderCategory.Category.Equals(searchType));

                return clientReqs;
            }
        }

        public IEnumerable<WorkOrder> GetWorkOrdersByClient(int id)
        {
            return _context.WorkOrders
                .Where(workOrder => workOrder.ClientRequest.ClientId == id)
                .Include(workOrder => workOrder.WorkOrderCategory)
                .Include(workOrder => workOrder.WorkOrderCompletionForm);
        }

        public WorkOrder GetWorkOrder(int id)
        {
            WorkOrder workOrder = new WorkOrder();
            workOrder = _context.WorkOrders.Include(w => w.WorkOrderCompletionForm).FirstOrDefault(w => w.Id == id);
            return workOrder;
        }

        public WorkOrder GetWorkOrderWithClient(int id)
        {
            WorkOrder workOrder = new WorkOrder();
            workOrder = _context.WorkOrders.Include(w => w.WorkOrderCategory).Include(w => w.ClientRequest).ThenInclude(cr => cr.Client).Include(w => w.WorkOrderCompletionForm).Include(w => w.RampRequest).FirstOrDefault(w => w.Id == id);
            return workOrder;
        }

        public IQueryable<WorkOrder> SearchWorkOrders(string type, JobStatus jobStatus, ClientStatus clientStatus = ClientStatus.Active)
        {

            var workOrders = from w in _context.WorkOrders
                             select w;
            if (type == "Any")
            {
                workOrders = workOrders.Include(w => w.ClientRequest)
                .ThenInclude(cr => cr.Client)
                .Include(w => w.WorkOrderCategory)
                .Where(w => w.JobStatus.Equals(jobStatus) && w.ClientRequest.Client.ClientStatus.Equals(clientStatus));

                return workOrders;
            }
            var catId = GetWorkOrderCategoryId(type);

            workOrders = workOrders.Include(w => w.ClientRequest)
                .ThenInclude(cr => cr.Client)
                .Include(w => w.WorkOrderCategory)
                .Where(w => w.WorkOrderCategory.Id.Equals(catId) && w.JobStatus.Equals(jobStatus) && w.ClientRequest.Client.ClientStatus.Equals(clientStatus));

            return workOrders;
        }

        public WorkOrderCompletionForm AddWorkOrderCompletionForm(WorkOrderCompletionForm workOrderCompletionForm)
        {
            _context.workOrderCompletionForms.Add(workOrderCompletionForm);
            _context.SaveChanges();
            return workOrderCompletionForm;
        }

        public WorkOrderCompletionForm GetWorkOrderCompletionForm(int id)
        {
            WorkOrderCompletionForm workCompForm = _context.workOrderCompletionForms.FirstOrDefault(wo => wo.Id == id);
            return workCompForm;
        }

        public WorkOrderCompletionForm UpdateWorkOrderCompletionForm(WorkOrderCompletionForm workOrderCompletionFormChanges)
        {
            var workOrderCompForm = _context.workOrderCompletionForms.Attach(workOrderCompletionFormChanges);
            workOrderCompForm.State = EntityState.Modified;
            _context.SaveChanges();

            return workOrderCompletionFormChanges;
        }

        public WorkOrder UpdateWorkOrder(WorkOrder workOrderChanges)
        {
            var workOrder = _context.WorkOrders.Attach(workOrderChanges);
            workOrder.State = EntityState.Modified;
            _context.SaveChanges();
            return workOrderChanges;
        }

        public IEnumerable<WorkOrder> GetWorkOrders()
        {
            return _context.WorkOrders
                .Include(wo => wo.WorkOrderCompletionForm)
                .Include(wo => wo.ClientRequest)
                .ThenInclude(wo => wo.Client)
                .Include(wo => wo.WorkOrderCategory);
        }

        #endregion

        #region Admin Favorites

        public AdminFaveClient AddAdminFaveClient(AdminFaveClient afc)
        {
            _context.AdminFavorites.Add(afc);
            _context.SaveChanges();
            return afc;
        }

        public AdminFaveClient SearchAdminFave(int clientId, string adminId)
        {
            var result = _context.AdminFavorites.FirstOrDefault(af => af.AdminId == adminId && af.ClientId == clientId);
            return result;
        }

        public AdminFaveClient RemoveAdminFave(int clientId, string adminId)
        {
            var adminFave = SearchAdminFave(clientId, adminId);
            if (adminFave != null)
            {
                _context.AdminFavorites.Remove(adminFave);
                _context.SaveChanges();
                return adminFave;
            }
            else
            {
                return adminFave;
            }
        }

        public List<Client> GetAdminFaveClients(string adminId)
        {
            var clients = from c in _context.Clients
                          join fave in _context.AdminFavorites
                          on c.Id equals fave.ClientId
                          where fave.AdminId.Equals(adminId)
                          select c;

            return clients.ToList();
        }

        public List<int> GetAdminFaveIDs(string adminId)
        {
            var favIds = from adminFave in _context.AdminFavorites
                         select adminFave.ClientId;

            return favIds.ToList();
        }

        #endregion

        #region Assistive Devices

        //Gets a single assistive device based on the id of the device
        public AssistiveDevice GetAssistiveDevice(int? id)
        {
            return _context.AssistiveDevices
                 .Include(c => c.Category)
                .FirstOrDefault(ad => ad.Id == id);
        }

        //Gets only active assistive devices and orders them by the category name
        //Returns an IEnumerable containing the devices
        public IEnumerable<AssistiveDevice> GetActiveAssistiveDevices()
        {
            var devices = _context.AssistiveDevices.Where(d => d.IsActive.Equals(true)).Include(ad => ad.Category).OrderBy(c => c.Category.Name);
            return devices;
        }

        //This gets all of the assistive devices based on a certain category 
        //Returns an IEnumerable containing the devices
        public IEnumerable<AssistiveDevice> GetAssistiveDevices(int CategoryId)
        {
            var devices = _context.AssistiveDevices.Where(d => d.CategoryId.Equals(CategoryId));
            return devices;
        }

        //Gets active assistive devices based on a certain category
        //Returns an IEnumerable containing the devices
        public IEnumerable<AssistiveDevice> GetActiveAssistiveDevices(int CategoryId)
        {
            var devices = _context.AssistiveDevices.Where(d => d.CategoryId == CategoryId && d.IsActive.Equals(true));
            return devices;
        }

        //Gets all active assistive device categories
        //Returns list containing the categories
        public IEnumerable<AssistiveDeviceCategory> GetActiveAssistiveDeviceCategories()
        {
            var categories = _context.AssistiveDeviceCategories.Where(c => c.IsActive.Equals(true)).OrderBy(c => c.Name);
            return categories;
        }

        #endregion

        #region Work Order Categories

        public List<WorkOrderCategory> GetWorkOrderCategories()
        {
            return _context.WorkOrderCategories.ToList();
        }

        public int GetWorkOrderCategoryId(string cat)
        {
            var category = _context.WorkOrderCategories.Where(c => c.Category == cat).FirstOrDefault();

            return category.Id;
        }

        public WorkOrderCategory GetWorkOrderCategory(int id)
        {
            var category = _context.WorkOrderCategories.Where(c => c.Id == id).FirstOrDefault();
            return category;
        }

        #endregion

        #region Lawn Service

        public PendingApproval AddToPendingList(PendingApproval pendingApproval)
        {
            _context.PendingApproval.Add(pendingApproval);
            _context.SaveChanges();
            return pendingApproval;
        }

        public IEnumerable<PendingApproval> GetPendingApproval()
        {
            return _context.PendingApproval;
        }

        public IEnumerable<WaitList> GetWaitList()
        {
            return _context.WaitList.OrderBy(wl => wl.Client.ZoneJoining.ZoneId)
               .ThenBy(wl => wl.TimeStamp)
               .Include(wl => wl.WorkOrder)
               .Include(wl => wl.Client)
               .ThenInclude(c => c.ZoneJoining)
               .ThenInclude(zj => zj.Zone);
        }

        public IEnumerable<LawnServiceList> GetLawnServiceList()
        {
            var serviceList = _context.LawnServiceList
                .Include(ls => ls.WorkOrder)
               .Include(ls => ls.Client)
               .ThenInclude(c => c.ZoneJoining)
               .ThenInclude(zj => zj.Zone)
               .ThenInclude(z => z.LawnService)
               .GroupBy(ls => ls.Client.Id)
               .Select(y => y.Last()).Distinct();
            return serviceList;
        }

        public IEnumerable<LawnService> GetLawnServices()
        {
            var services = _context.LawnServices;
            return services;
        }

        public IEnumerable<Zone> GetZones()
        {
            return _context.Zones;
        }

        public IEnumerable<ClientLawnService> GetClientLawnServices()
        {
            return _context.ClientLawnServices;
        }

        #endregion
    }
}


﻿using CareConnectionApp.DataAccess.Abstractions;
using CareConnectionApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CareConnectionApp.DataAccess
{
    public class AdminDataAccess : IAdminDataAccess
    {
        private readonly CCADbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public AdminDataAccess
        (
            CCADbContext context,
            UserManager<ApplicationUser> userManager
        )
        {
            _context = context;
            _userManager = userManager;
        }

        #region Work Order Categories

        public List<WorkOrderCategory> GetWorkOrderCategories()
        {
            return _context.WorkOrderCategories.AsNoTracking().ToList();
        }

        public WorkOrderCategory GetWorkOrderCategory(int Id)
        {
            var cat = _context.WorkOrderCategories.Find(Id);
            return cat;
        }

        public WorkOrderCategory UpdateWorkOrderCategory(WorkOrderCategory workOrderCatChanges)
        {
            if (workOrderCatChanges.IsBaseCategory == false)
            {
                var workOrderCat = _context.WorkOrderCategories.Attach(workOrderCatChanges);
                workOrderCat.State = EntityState.Modified;
                _context.SaveChanges();
            }
            else
            {
                throw new Exception($"Base categories such as {workOrderCatChanges.Category} cannot be edited.");
            }

            return workOrderCatChanges;
        }

        public WorkOrderCategory AddWorkOrderCategory(WorkOrderCategory workOrderCategory)
        {
            var workOrderCategoryResult = _context.WorkOrderCategories.Add(workOrderCategory);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                throw e;
            }
            return workOrderCategory;
        }

        public bool DeleteWorkOrderCategory(int workOrderCategoryID)
        {
            var workOrderCategory = _context.WorkOrderCategories.AsNoTracking().FirstOrDefault(w => w.Id == workOrderCategoryID);
            if (workOrderCategory.IsBaseCategory || _context.WorkOrders.Any(wo => wo.WorkOrderCategory.Id == workOrderCategory.Id))
            {
                return false;
            }
            else
            {
                try
                {
                    _context.WorkOrderCategories.Remove(workOrderCategory);
                    _context.SaveChanges();
                }
                catch (DbUpdateException e)
                {

                    throw e;
                }

                return true;
            }
        }

        public List<WorkOrderCompletionForm> GetCompletionForms(int count)
        {
            return _context.workOrderCompletionForms.OrderByDescending(woc => woc.Id).Take(count).ToList();
        }

        #endregion

        #region Assitive Devices

        //Adds an assistive device to the database
        public AssistiveDevice AddAssistiveDevice(AssistiveDevice device)
        {
            _context.AssistiveDevices.Add(device);
            _context.SaveChanges();
            return device;
        }

        //Gets a single assistive device based on the id of the device
        public AssistiveDevice GetAssistiveDevice(int? id)
        {
            return _context.AssistiveDevices
                .Include(ad => ad.Category)
                .FirstOrDefault(ad => ad.Id == id);
        }

        //Adds an assistive device category to the database
        public AssistiveDeviceCategory AddAssistiveDeviceCategory(AssistiveDeviceCategory category)
        {
            var assistiveDeviceCategoryResult = _context.AssistiveDeviceCategories.Add(category);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException e)
            {
                return null;
            }
            return category;
        }

        //Gets the assistive device category based off of its id
        public AssistiveDeviceCategory GetAssistiveDeviceCategory(int id)
        {
            return _context.AssistiveDeviceCategories.Find(id);
        }

        //Update an assistive device in the database
        public AssistiveDevice UpdateAssistiveDevice(AssistiveDevice assistiveDeviceChanges)
        {
            var assistiveDevice = _context.AssistiveDevices.Attach(assistiveDeviceChanges);
            assistiveDevice.State = EntityState.Modified;
            _context.SaveChanges();
            return assistiveDeviceChanges;
        }

        public AssistiveDeviceCategory UpdateAssistiveDeviceCategory(AssistiveDeviceCategory updateCategory)
        {
            var assistiveDeviceCategory = _context.AssistiveDeviceCategories.Attach(updateCategory);
            assistiveDeviceCategory.State = EntityState.Modified;
            _context.SaveChanges();
            return updateCategory;
        }

        //Gets all devices regardless of activity label and orders them based off of the category name
        //Returns an IEnumerable containing the devices
        public IEnumerable<AssistiveDevice> GetAssistiveDevices()
        {
            var devices = _context.AssistiveDevices
                .Include(ad => ad.Category)
                .OrderBy(ad => ad.Category.Name);
            return devices;
        }

        //Gets all assistive device categories
        //Returns an IEnumerable containing the categories
        public IEnumerable<AssistiveDeviceCategory> GetAssistiveDeviceCategories()
        {
            var categories = _context.AssistiveDeviceCategories;
            return categories;
        }

        #endregion

        #region Lawn Service

        public IEnumerable<PendingApproval> GetPendingApprovals()
        {
            return _context.PendingApproval
                .Include(pa => pa.WorkOrder)
                .Include(pa => pa.Client)
                .ThenInclude(c => c.ZoneJoining)
                .ThenInclude(zj => zj.Zone);
        }

        public PendingApproval GetPendingApproval(int id)
        {
            return _context.PendingApproval.FirstOrDefault(pa => pa.Id == id);
        }

        public void DeleteFromPendingApprovals(PendingApproval pendingApproval)
        {
            _context.PendingApproval.Remove(pendingApproval);
            _context.SaveChanges();
        }

        public void DeleteFromPendingApprovals(List<PendingApproval> pendingApprovals)
        {
            _context.PendingApproval.RemoveRange(pendingApprovals);
            _context.SaveChanges();
        }

        public WaitList AddToWaitList(WaitList waitList)
        {
            _context.WaitList.Add(waitList);
            _context.SaveChanges();
            return waitList;
        }

        public List<WaitList> AddToWaitList(List<WaitList> waitList)
        {
            _context.WaitList.AddRange(waitList);
            _context.SaveChanges();
            return waitList;
        }

        public void DeleteFromWaitList(WaitList waitList)
        {
            _context.WaitList.Remove(waitList);
            _context.SaveChanges();
        }

        public void DeleteFromWaitList(List<WaitList> waitLists)
        {
            _context.WaitList.RemoveRange(waitLists);
            _context.SaveChanges();
        }


        public LawnServiceList AddToLawnServiceList(LawnServiceList lawnServiceList)
        {
            _context.LawnServiceList.Add(lawnServiceList);
            _context.SaveChanges();
            return lawnServiceList;
        }

        public List<LawnServiceList> AddToLawnServiceList(List<LawnServiceList> lawnServices)
        {
            _context.LawnServiceList.AddRange(lawnServices);
            _context.SaveChanges();
            return lawnServices;
        }

        public void DeleteFromServiceList(LawnServiceList lawnServiceList)
        {
            _context.LawnServiceList.Remove(lawnServiceList);
            _context.SaveChanges();
        }

        public LawnService AddLawnService(LawnService lawnService)
        {
            _context.LawnServices.Add(lawnService);
            _context.SaveChanges();
            return lawnService;
        }

        public IEnumerable<ClientLawnService> AddClientLawnServices(IEnumerable<ClientLawnService> clsList)
        {
            _context.ClientLawnServices.AddRange(clsList);
            _context.SaveChanges();
            return clsList;
        }

        public IEnumerable<WorkOrder> CompleteLawnServices(IEnumerable<WorkOrder> workOrders)
        {
            _context.WorkOrders.AttachRange(workOrders);
            _context.SaveChanges();
            return workOrders;
        }

        public IEnumerable<WorkOrderCompletionForm> CompleteLawnServices(IEnumerable<WorkOrderCompletionForm> completionForms)
        {
            _context.workOrderCompletionForms.AttachRange(completionForms);
            _context.SaveChanges();
            return completionForms;
        }

        #endregion

        #region Zones

        public Zone AddZone(Zone zone)
        {
            _context.Zones.Add(zone);
            _context.SaveChanges();
            return zone;
        }

        public Zone UpdateZone(Zone zone)
        {
            var updateZone = _context.Zones.Attach(zone);
            updateZone.State = EntityState.Modified;
            _context.SaveChanges();
            return zone;
        }

        public IEnumerable<Zone> GetZones()
        {
            return _context.Zones;
        }

        public ZoneJoining AddZoneJoin(ZoneJoining zoneJoining)
        {
            _context.ZoneJoinings.Add(zoneJoining);
            _context.SaveChanges();
            return zoneJoining;
        }

        public ZoneJoining UpdateZoneJoin(ZoneJoining zoneJoin)
        {
            var updateJoin = _context.ZoneJoinings.Attach(zoneJoin);
            updateJoin.State = EntityState.Modified;
            _context.SaveChanges();
            return zoneJoin;
        }

        public IEnumerable<ZoneJoining> GetZoneJoins()
        {
            return _context.ZoneJoinings.Include(zj => zj.Zone).Include(zj => zj.Client);
        }

        #endregion

        #region Admin Settings

        public AdminSetting GetAdminSetting(string userId)
        {
            return _context.AdminSettings.FirstOrDefault(user => user.ApplicationUserId == userId);
        }

        public AdminSetting SetAdminAlertSetting(string userId, int duration)
        {
            return SetGivenAdminSetting(userId, duration, "AlertLifeSpan");
        }

        public AdminSetting SetAtAGlanceSetting(string userId, int duration)
        {
            return SetGivenAdminSetting(userId, duration, "AtAGlanceDuration");
        }

        public AdminSetting SetDashboardHistoryCount(string userId, int count)
        {
            return SetGivenAdminSetting(userId, count, "DashboardCardHistoryCount");
        }
        public AdminSetting SetServiceListLimit(string userId, int count)
        {
            return SetGivenAdminSetting(userId, count, "ServiceListLimit");
        }

        public AdminSetting UpdateAdminSetting(AdminSetting updatedAdminSetting)
        {
            var savedAdminSetting = _context.AdminSettings.Attach(updatedAdminSetting);
            savedAdminSetting.State = EntityState.Modified;
            _context.SaveChanges();
            return savedAdminSetting.Entity;
        }

        private AdminSetting SetGivenAdminSetting(string userId, int value, string settingName)
        {
            var propInfo = typeof(AdminSetting).GetProperty(settingName); // get property from AdminSetting type, and check if it exists. return null if it doesn't
            if (propInfo == null)
            {
                return null;
            }

            var adminSetting = _context.AdminSettings.FirstOrDefault(setting => setting.ApplicationUserId == userId);

            if (adminSetting == null)
            {
                adminSetting = new AdminSetting
                {
                    ApplicationUserId = userId
                };
                propInfo.SetValue(adminSetting, value);
                _context.AdminSettings.Add(adminSetting);
            }
            else
            {
                propInfo.SetValue(adminSetting, value);
            }
            _context.SaveChanges();
            return adminSetting;
        }

        #endregion
    }
}

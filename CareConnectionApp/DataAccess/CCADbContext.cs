﻿using CareConnectionApp.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CareConnectionApp.DataAccess
{
    public class CCADbContext : IdentityDbContext<ApplicationUser>
    {
        public CCADbContext()
        {
        }

        public CCADbContext(DbContextOptions<CCADbContext> options) : base(options)
        {
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientRequest> ClientRequests { get; set; }
        public DbSet<WorkOrder> WorkOrders { get; set; }
        public DbSet<RampRequest> RampRequests { get; set; }
        public DbSet<WorkOrderCompletionForm> workOrderCompletionForms { get; set; }
        public DbSet<Eheap> Eheaps { get; set; }
        public DbSet<UserClientAdditions> UserClientAdditons { get; set; }
        public DbSet<AdminFaveClient> AdminFavorites { get; set; }
        public DbSet<AssistiveDevice> AssistiveDevices { get; set; }
        public DbSet<AssistiveDeviceCategory> AssistiveDeviceCategories { get; set; }
        public DbSet<WorkOrderCategory> WorkOrderCategories { get; set; }
        public DbSet<LawnService> LawnServices { get; set; }
        public DbSet<WaitList> WaitList { get; set; }
        public DbSet<PendingApproval> PendingApproval { get; set; }
        public DbSet<LawnServiceList> LawnServiceList { get; set; }
        public DbSet<Zone> Zones { get; set; }
        public DbSet<ZoneJoining> ZoneJoinings { get; set; }
        public DbSet<Alert> Alerts { get; set; }
        public DbSet<ClientLawnService> ClientLawnServices { get; set; }
        public DbSet<AdminSetting> AdminSettings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClientRequest>()
                .HasKey(c => new { c.ClientId, c.WorkOrderId });

            modelBuilder.Entity<UserClientAdditions>()
                .HasKey(c => new { c.UserId, c.ClientId });
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AdminFaveClient>()
                .HasKey(afc => new { afc.AdminId, afc.ClientId });
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<WorkOrderCategory>()
                .HasIndex(c => c.Category)
                .IsUnique();
            modelBuilder.Entity<AssistiveDeviceCategory>()
                .HasIndex(a => a.Name)
                .IsUnique();
        }
    }
}

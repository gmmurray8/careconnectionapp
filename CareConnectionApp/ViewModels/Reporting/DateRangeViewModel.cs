﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.Reporting
{
    public class DateRangeViewModel
    {
        [DataType(DataType.Date)]
        public DateTime LowerBound { get; set; }
        [DataType(DataType.Date)]
        public DateTime UpperBound { get; set; }
    }
}

﻿using CareConnectionApp.Models;

namespace CareConnectionApp.ViewModels.Reporting
{
    public class JobSummaryResult
    {
        public string JobType { get; set; }
        public int NumOfUniqueClients { get; set; }
        public int NumOfTypes { get; set; }
        public int NumOfTypeActive { get; set; }
        public int NumOfTypeCompleted { get; set; }
        public int NumOfTypeDeferred { get; set; }
        public int NumOfTypeInactive { get; set; }
        public decimal Expenses { get; set; }
        public decimal Donations { get; set; }
        public float Hours { get; set; }
        public int Miles { get; set; }

    }
}

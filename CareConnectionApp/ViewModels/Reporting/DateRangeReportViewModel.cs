﻿using System.Collections.Generic;

namespace CareConnectionApp.ViewModels.Reporting
{
    public class DateRangeReportViewModel : DateRangeViewModel
    {
        public int NumOfUniqueClients { get; set; }
        public int NumOfJobsCompleted { get; set; }
        public int NumOfJobsDeferred { get; set; }
        public decimal Donations { get; set; }
        public decimal Expenses { get; set; }
        public int Miles { get; set; }
        public float Hours { get; set; }
        public List<object> CategoriesReport = new List<object>();
    }
}

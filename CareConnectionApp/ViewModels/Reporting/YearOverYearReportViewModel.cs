﻿using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.Reporting
{
    public class YearOverYearReportViewModel
    {
        [Required(ErrorMessage = "Please enter a start date")]
        public int startYear { get; set; }
        [Required(ErrorMessage = "Please enter an end date")]
        public int endYear { get; set; }
    }
}

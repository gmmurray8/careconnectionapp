﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.Reporting
{
    public class JobTypeSummaryReportViewModel
    {
        [DataType(DataType.Date)]
        public DateTime LowerBound { get; set; }
        [DataType(DataType.Date)]
        public DateTime UpperBound { get; set; }
        public string workOrderCategory { get; set; }
        public List<string> Categories { get; set; }
        
    }
}

﻿using System;
using System.Collections.Generic;

namespace CareConnectionApp.ViewModels.Reporting
{
    public class LawnServiceReportViewModel
    {
        public int TotalUniqueClients { get; set; }
        public int TotalLawnsCut { get; set; }
        public int TotalLawnServices { get; set; }
        public List<object> ZoneReports = new List<object>();
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}

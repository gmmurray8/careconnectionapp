﻿using CareConnectionApp.Models;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.Account
{
    public class UpdateAccountViewModel
    {
        public string Id { get; set; }
        [Required]
        public string UserName { get; set; }
        public string Email { get; set; }
        [Required]
        public string FullName { get; set; }

        public static UpdateAccountViewModel Initialize(ApplicationUser user)
        {
            return new UpdateAccountViewModel
            {
                Id = user.Id,
                Email = user.Email,
                FullName = user.FullName,
                UserName = user.UserName
            };
        }
    }
}

﻿using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.Account
{
    public class AdminSettingsViewModel
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "Please choose an alert duration")]
        [Range(1, int.MaxValue, ErrorMessage = "Alert life span must be at least 1 day")]
        public int AlertDuration { get; set; }

        [Required(ErrorMessage = "Please choose an at a glance duration")]
        [Range(1, int.MaxValue, ErrorMessage = "At a glance statistics must go back at least 1 month")]
        public int AtAGlanceDuration { get; set; }

        [Required(ErrorMessage = "Please choose a recent request setting")]
        [Range(1, int.MaxValue, ErrorMessage = "You must show at least one recent request")]
        public int RecentRequestsSetting { get; set; }

        [Required(ErrorMessage = "Please choose a service list limit steting")]
        [Range(1, int.MaxValue, ErrorMessage = "The lawn service list must have a limit of at least 1")]
        public int ServiceListLimit { get; set; }

        public static AdminSettingsViewModel Initialize(IAdminService adminService, string userId)
        {
            return new AdminSettingsViewModel
            {
                UserId = userId,
                AlertDuration = adminService.GetAlertSetting(userId),
                AtAGlanceDuration = adminService.GetAtAGlanceSetting(userId),
                RecentRequestsSetting = adminService.GetRecentRequestSetting(userId),
                ServiceListLimit = adminService.GetServiceListLimitSetting(userId)
            };
        }

        public static AdminSetting GetAdminSetting(AdminSettingsViewModel viewModel)
        {
            return new AdminSetting
            {
                ApplicationUserId = viewModel.UserId,
                AlertLifeSpan = viewModel.AlertDuration,
                AtAGlanceDuration = viewModel.AtAGlanceDuration,
                DashboardCardHistoryCount = viewModel.RecentRequestsSetting,
                ServiceListLimit = viewModel.ServiceListLimit
            };
        }
    }
}

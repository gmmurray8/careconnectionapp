﻿namespace CareConnectionApp.ViewModels.Shared
{
    public class ConfirmationViewModel
    {
        public string Id { get; set; }
        public string Message { get; set; }
        public string Sender { get; set; }

        public static ConfirmationViewModel Initialize(string id, string message, string sender)
        {
            return new ConfirmationViewModel
            {
                Id = id,
                Message = message,
                Sender = sender
            };
        }
    }
}

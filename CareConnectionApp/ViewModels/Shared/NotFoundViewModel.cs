﻿namespace CareConnectionApp.ViewModels.Shared
{
    public class NotFoundViewModel
    {
        public string Message { get; set; }
    }
}

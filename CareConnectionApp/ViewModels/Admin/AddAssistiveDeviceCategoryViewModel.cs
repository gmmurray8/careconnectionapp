﻿using CareConnectionApp.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.Admin
{
    public class AddAssistiveDeviceCategoryViewModel
    {
        [Required(ErrorMessage="Please enter a name for the category.")]
         public String Name { get; set; }
         public string Description { get; set; }

        public static AssistiveDeviceCategory GetAssistiveDeviceCategory(AddAssistiveDeviceCategoryViewModel viewModel)
        {
            return new AssistiveDeviceCategory
            {
                Name = viewModel.Name,
                Description = viewModel.Description,
                IsActive = true
            };
        }
    }
}

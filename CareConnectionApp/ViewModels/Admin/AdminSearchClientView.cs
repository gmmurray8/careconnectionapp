﻿using System.Collections.Generic;

namespace CareConnectionApp.ViewModels.Admin
{
    public class AdminSearchClientView
    {
        public class ClientAddable
        {
            public int Id { get; set; }
            public int Zip { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string HomePhone { get; set; }
            public string CellPhone { get; set; }
            public string StreetAddress { get; set; }
            public bool Add { get; set; }
        }


        public List<ClientAddable> Clients = new List<ClientAddable>();
        public List<int> Ids = new List<int>();
    }
}

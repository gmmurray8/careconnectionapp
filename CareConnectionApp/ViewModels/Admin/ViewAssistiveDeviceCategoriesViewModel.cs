﻿using CareConnectionApp.Models;
using System.Collections.Generic;

namespace CareConnectionApp.ViewModels.Admin
{
    public class ViewAssistiveDeviceCategoriesViewModel
    {
        public IEnumerable<AssistiveDeviceCategory> AssistiveDeviceCategories { get; set; }
    }
}

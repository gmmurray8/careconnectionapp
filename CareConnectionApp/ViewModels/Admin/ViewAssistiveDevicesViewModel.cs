﻿using System.Collections.Generic;
using CareConnectionApp.Models;

namespace CareConnectionApp.ViewModels.Admin
{
    public class ViewAssistiveDevicesViewModel
    {
        public IEnumerable<AssistiveDevice> AssistiveDevices { get; set; }
    }
}

﻿using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.Admin
{
    public class AddAssistiveDeviceViewModel
    {
        [Required(ErrorMessage ="Please enter a name for the device.")]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Please pick a category.")]
        public int CategoryId { get; set; }
        public IEnumerable<AssistiveDeviceCategory> assistiveDeviceCategories { get; set; }

        public static AssistiveDevice SetupAssistiveDevice(AddAssistiveDeviceViewModel viewModel)
        {
            var assistiveDevice = new AssistiveDevice
            {
                Name = viewModel.Name,
                Description = viewModel.Description,
                CategoryId = viewModel.CategoryId,
                IsActive = true
            };
            return assistiveDevice;
        }

        public static AddAssistiveDeviceViewModel Initialize(IClientService clientService)
        {
            return new AddAssistiveDeviceViewModel
            {
                assistiveDeviceCategories = clientService.GetActiveAssistiveDeviceCategories()
            };
        }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;

namespace CareConnectionApp.ViewModels.Admin
{
    public class UpdateAssistiveDeviceViewModel
    {
        public IEnumerable<AssistiveDeviceCategory> AssistiveDeviceCategories { get; set; }
        [Required]
        public AssistiveDevice AssistiveDevice { get; set; }

        public static UpdateAssistiveDeviceViewModel Initialize(IAdminService adminService, int id)
        {
            return new UpdateAssistiveDeviceViewModel
            {
                AssistiveDeviceCategories = adminService.GetAssistiveDeviceCategories(),
                AssistiveDevice = adminService.GetAssistiveDevice(id)
            };
        }
    }
}

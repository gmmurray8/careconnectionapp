﻿using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System.Collections.Generic;

namespace CareConnectionApp.ViewModels.Admin
{
    public class AdminHomeViewModel
    {
        public List<Client> FaveClients { get; set; }
        public Dictionary<int, int> NewClients { get; set; }
        public Dictionary<int, int> JobsOpened { get; set; }
        public Dictionary<int, int> JobsCompleted { get; set; }
        public IEnumerable<WorkOrder> RecentWorkOrders { get; set; }
        public Dictionary<WorkOrder, AssistiveDevice> RecentAssistiveDeviceWorkOrders { get; set; }
        public IEnumerable<PendingApproval> RecentPendingApprovals { get; set; }

        public static AdminHomeViewModel Initialize(IClientService clientService, IAdminService adminService, string userId)
        {
            return new AdminHomeViewModel
            {
                FaveClients = clientService.GetAdminFaveClients(userId),
                NewClients = adminService.GetNumberNewClients(userId),
                JobsOpened = adminService.GetNumberJobsOpened(userId),
                JobsCompleted = adminService.GetNumberJobsCompleted(userId),
                RecentWorkOrders = adminService.GetRecentWorkOrders(userId),
                RecentAssistiveDeviceWorkOrders = adminService.CombineWorkOrderAssistiveDevice(adminService.GetRecentAssistiveDeviceWorkOrders(userId)),
                RecentPendingApprovals = adminService.GetRecentPendingApprovals(userId)
            };
        }
    }
}

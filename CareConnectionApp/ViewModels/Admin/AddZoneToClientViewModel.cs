﻿using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.Admin
{
    public class AddZoneToClientViewModel
    {
        public Client Client { get; set; }
        [Required]
        public Zone Zone { get; set; }
        public string ReturnUrl { get; set; }
        public IEnumerable<Zone> Zones { get; set; }

        public static AddZoneToClientViewModel Initialize(IAdminService adminService, IClientService clientService, int id)
        {
            var viewModel = new AddZoneToClientViewModel()
            {
                Zones = adminService.GetZones(),
                Client = clientService.GetClient(id),
            };
            if (clientService.GetClient(id).ZoneJoining != null)
            {
                viewModel.Zone = clientService.GetClient(id).ZoneJoining.Zone;
            }
            return viewModel;
        }
    }
}

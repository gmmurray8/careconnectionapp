﻿using CareConnectionApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CareConnectionApp.ViewModels.Users
{
    public class UpdateUserInfoViewModel
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Please enter a username")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Please enter an email address")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter a name")]
        public string FullName { get; set; }

        public static UpdateUserInfoViewModel Initialize(ApplicationUser user)
        {
            return new UpdateUserInfoViewModel
            {
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email,
                FullName = user.FullName
            };
        }
    }
}

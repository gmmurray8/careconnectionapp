﻿using CareConnectionApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CareConnectionApp.ViewModels.Users
{
    public class ViewUserClientsViewModel
    {
        public IEnumerable<UserClientAdditions> UserClientAdditions { get; set; }
        public string SearchString { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }

        public static ViewUserClientsViewModel Initialize(string userId, string username, IEnumerable<UserClientAdditions> userClientAdditions)
        {
            return new ViewUserClientsViewModel
            {
                UserId = userId,
                Username = username,
                UserClientAdditions = userClientAdditions
            };
        }
    }
}

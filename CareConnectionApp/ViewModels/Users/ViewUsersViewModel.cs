﻿using CareConnectionApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CareConnectionApp.ViewModels.Users
{
    public class ViewUsersViewModel
    {
        public IEnumerable<ApplicationUser> Users { get; set; }
        public string SearchString { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.Users
{
    public class UpdatePasswordViewModel
    {
        public string Id { get; set; }

        [DataType(DataType.Password), Required(ErrorMessage = "Please enter a new password")]
        public string NewPassword { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CareConnectionApp.ViewModels.Users
{
    public class NewUserViewModel
    {
        [Required(ErrorMessage = "Please enter an email address"), EmailAddress]
        
        [RegularExpression(@"^[A-Za-z]+[A-Za-z0-9_.]*[^._@]+@[a-z]+\.([a-z]{3})$", ErrorMessage = "Invalid E-mail Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter a username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Please enter a name")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Please enter a role")]
        public string RoleName { get; set; }

        [Required(ErrorMessage = "Please enter a password"), DataType(DataType.Password, ErrorMessage = "Please enter a valid password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please confirm the password"), DataType(DataType.Password), Compare("Password", ErrorMessage = "Passwords do not match.")]
        public string ConfirmPassword { get; set; }
    }
}

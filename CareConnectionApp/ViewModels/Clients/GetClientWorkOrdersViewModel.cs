﻿using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System.Collections.Generic;

namespace CareConnectionApp.ViewModels.Clients
{
    public class GetClientWorkOrdersViewModel
    {
        public Client Client { get; set; }
        public IEnumerable<ClientRequest> WorkOrders { get; set; }
        public string WorkOrderCategory { get; set; }
        public JobStatus JobStatus { get; set; }
        public List<string> Categories { get; set; }

        public static GetClientWorkOrdersViewModel Initialize(IClientService clientService, Client client, IEnumerable<ClientRequest> clientRequests)
        {
            return new GetClientWorkOrdersViewModel
            {
                Categories = clientService.GetWorkOrderCategoryNamesForSearching(),
                Client = client,
                WorkOrders = clientRequests
            };
        }
    }
}

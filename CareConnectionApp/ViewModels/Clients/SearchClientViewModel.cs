﻿using CareConnectionApp.DataAccess.Abstractions;
using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using CareConnectionApp.Utility_Classes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CareConnectionApp.ViewModels.Clients
{
    public class SearchClientViewModel : ResultsPage
    {
        public int? ClientID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        [RegularExpression(@"(^\d{5}(-\d{4})?$)", ErrorMessage = "Invalid Zip Code.")]
        public int? Zip { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Invalid Phone Number")]
        public string HomePhone { get; set; }
        public IEnumerable<IGrouping<Client, int>> SearchClientResults { get; set; }
        public ClientStatus clientStatus { get; set; }
        public string SortBy { get; set; }
        public string SortOrder { get; set; }
        public bool FromPost { get; set; }
        //public int PageNum { get; set; } = 15;

        public static SearchClientViewModel Initialize(IClientService clientService)
        {
            var viewModel = new SearchClientViewModel
            {
                PageNum = 1,
                ResultsPerPage = 15,
                //SearchClientResults = clientService.SearchClients(new SearchClientViewModel { });
                FromPost = false
            };

            viewModel = clientService.SearchClients(viewModel);

            return viewModel;
        }

        public static SearchClientViewModel MoreClients(IClientService clientService, SearchClientViewModel model)
        {
            model.PageNum += 1;
            model.ResultsPerPage = 15;
            return ModelFill(clientService, model);
        }

        public static SearchClientViewModel SearchClients(IClientService clientService, SearchClientViewModel model)
        {
            model.PageNum = 1;
            model.ResultsPerPage = 15;

            return ModelFill(clientService, model);
        }

        private static SearchClientViewModel ModelFill(IClientService clientService, SearchClientViewModel model)
        {
            //model.SearchClientResults = LazyLoader.ClientLazyLoad(clientService.SearchClients(model), model.PageNum, 15,
            //out int results, out int shownResults);
            //model.SearchClientResults = ListSorter.ClientSort(model.SearchClientResults, model.SortBy, model.SortOrder);
            //model.TotalResults = results;
            //model.ShownResults = shownResults;
            model = clientService.SearchClients(model);
            model.FromPost = true;

           

            return model;
        }
    }

}

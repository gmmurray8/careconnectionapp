﻿using CareConnectionApp.Models;

namespace CareConnectionApp.ViewModels.Clients
{
    public class GetEheapViewModel
    {
        public Eheap Eheap { get; set; }
        public Client Client { get; set; }

    }
}

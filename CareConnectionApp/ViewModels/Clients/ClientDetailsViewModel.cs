﻿using System.Collections.Generic;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;

namespace CareConnectionApp.ViewModels.Clients
{
    public class ClientDetailsViewModel
    {
        public Client Client { get; set; }
        public IEnumerable<Zone> Zones { get; set; }
        public int? ZoneId { get; set; }

        public static ClientDetailsViewModel Initialize(IClientService clientService, int id)
        {
            return new ClientDetailsViewModel
            {
                Client = clientService.GetClient(id),
                Zones = clientService.GetZones()
            };
        }
    }
}

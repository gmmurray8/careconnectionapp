﻿using CareConnectionApp.Models;

namespace CareConnectionApp.ViewModels.Clients
{
    public class CreateClientViewModel
    {
        public Client Client { get; set; }
        public bool CreateWorkRequestAfter { get; set; }
        public string UserId { get; set; }
    }
}

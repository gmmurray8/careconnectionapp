﻿using CareConnectionApp.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.Clients
{
    public class UpdateEheapViewModel
    {
        [DataType(DataType.Date)]
        public DateTime DateOfService { get; set; }

        [DataType(DataType.Currency)]
        public decimal Expenses { get; set; }

        public int? EheapId { get; set; }

        public int ClientId { get; set; }
        public Client Client { get; set; }
    }
}

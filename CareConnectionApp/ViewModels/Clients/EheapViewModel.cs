﻿using System;
using CareConnectionApp.Models;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.Clients
{
    public class EheapViewModel
    {
        [DataType(DataType.Date), Required(ErrorMessage = "Please enter a date.")]
        public DateTime DateOfService { get; set; }

        [DataType(DataType.Currency)]
        public decimal Expenses { get; set; }
        public int ClientId { get; set; }
        public Client Client { get; set; }
    }
}

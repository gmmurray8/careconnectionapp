﻿using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CareConnectionApp.ViewModels.WorkOrders
{
    public class CompleteWorkOrderViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Type Of Order")]
        public string WorkOrderCategory { get; set; }
        [Required]
        [Display(Name = "Request Description")]
        public string ReqDescription { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Date Of Request")]
        public DateTime DateOfRequest { get; set; }
        public JobStatus JobStatus { get; set; }
        public int ClientId { get; set; }

        [Display(Name = "Secondary Contact Name")]
        public string SecondaryContact { get; set; }
        [Display(Name = "Secondary Contact Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Invalid Phone Number")]
        public string SecondaryContactNumber { get; set; }

        public WorkOrderCompletionForm WorkOrderCompletionForm { get; set; }
        public IEnumerable<AssistiveDeviceCategory> DeviceCategories { get; set; }
        public List<AssistiveDevice> Devices { get; set; }
        public AssistiveDevice Device { get; set; }
        public int? DeviceId { get; set; }
        public RampRequest RampRequest { get; set; }
    }
}

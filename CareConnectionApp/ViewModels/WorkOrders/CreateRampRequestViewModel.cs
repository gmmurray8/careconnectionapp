﻿using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.WorkOrders
{
    public class CreateRampRequestViewModel:CreateWorkOrderViewModel
    {
        public bool PermanentMobilityImpairment { get; set; }
        [Required(ErrorMessage = "Please select the client's mobility assistance")]
        public MobilityAssistance MobilityAssistance { get; set; }
        [Required(ErrorMessage = "Please provide a short description of client's mobility.")]
        public string MobilityDescription { get; set; }
        public bool ContactedVA { get; set; }
        public bool OwnerOfHome { get; set; }
        [Required(ErrorMessage = "Please provide a length of home residency.")]
        public string LengthOfHomeOwnership { get; set; }
        public bool LiveAlone { get; set; }

        public static CreateRampRequestViewModel Initialize(CreateWorkOrderViewModel viewModel)
        {
            return new CreateRampRequestViewModel
            {
                ClientId = viewModel.ClientId,
                WorkOrderCategory = viewModel.WorkOrderCategory,
                ReqDescription = viewModel.ReqDescription
            };
        }

        public static RampRequest GetRampRequest(CreateRampRequestViewModel viewModel)
        {
            return new RampRequest
            {
                PermanentMobilityImpairment = viewModel.PermanentMobilityImpairment,
                MobilityAssistance = viewModel.MobilityAssistance,
                MobilityDescription = viewModel.MobilityDescription,
                ContactedVA = viewModel.ContactedVA,
                OwnerOfHome = viewModel.OwnerOfHome,
                LengthOfHomeOwnership = viewModel.LengthOfHomeOwnership,
                LiveAlone = viewModel.LiveAlone
            };
        }

        public static WorkOrder GetWorkOrder(IClientService clientService, CreateRampRequestViewModel viewModel)
        {
            return new WorkOrder
            {
                WorkOrderCategoryId = clientService.GetWorkOrderCategoryId(viewModel.WorkOrderCategory),
                ReqDescription = viewModel.ReqDescription
            };
        }
    }
}

﻿using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CareConnectionApp.ViewModels.WorkOrders
{
    public class CreateWorkOrderViewModel
    {
        public Client Client { get; set; }

        [Required]
        public int ClientId { get; set; }
        public List<string> Categories {get;set;}
        [Required(ErrorMessage = "Please select a category")]
        public string WorkOrderCategory { get; set; }
        [Required(ErrorMessage = "Please provide a description for the type of order the client requested.")]
        public string ReqDescription { get; set; }

        [Display(Name = "Secondary Contact Name")]
        public string SecondaryContact { get; set; }
        
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Invalid Phone Number")]
        [Display(Name = "Secondary Contact Phone Number")]
        public string SecondaryContactNumber { get; set; }

        public static CreateWorkOrderViewModel Initialize(IClientService clientService, Client client)
        {
            return new CreateWorkOrderViewModel
            {
                Categories = clientService.GetworkOrderCategoryNamesForCreation().OrderBy(c => c).ToList(),
                Client = client,
                ClientId = client.Id
            };
        }
    }
}

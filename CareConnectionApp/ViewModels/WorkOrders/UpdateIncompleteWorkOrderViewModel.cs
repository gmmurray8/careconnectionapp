﻿using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CareConnectionApp.ViewModels.WorkOrders
{
    public class UpdateIncompleteWorkOrderViewModel : AbstractViewModels.AbstractUpdateWorkOrderViewModel
    {
        public static UpdateIncompleteWorkOrderViewModel Initialize(IClientService clientService, WorkOrder workOrder)
        {
            var viewModel = new UpdateIncompleteWorkOrderViewModel
            {
                Id = workOrder.Id,
                ClientId = workOrder.ClientRequest.ClientId,
                WorkOrderCategory = workOrder.WorkOrderCategory.Category,
                ReqDescription = workOrder.ReqDescription,
                DateOfRequest = workOrder.DateOfRequest,
                JobStatus = workOrder.JobStatus,
                SecondaryContact = workOrder.SecondaryContact,
                SecondaryContactNumber = workOrder.SecondaryContactNumber,
                Categories = clientService.GetworkOrderCategoryNamesForCreation()
                .Where(cat => !cat
                .Equals("Lawn Service")
                && !cat.Equals("Ramp")
                && !cat.Equals("Assistive Device"))
                .ToList(),
            };

            if (workOrder.WorkOrderCategory.Category == "Assistive Device" && workOrder.AssistiveDeviceId != null)
            {
                var tempDev = clientService.GetAssistiveDevice(workOrder.AssistiveDeviceId);
                viewModel.DeviceCategories = clientService.GetActiveAssistiveDeviceCategories();
                viewModel.Devices = clientService.GetActiveAssistiveDevices(tempDev.Category.Id);
                viewModel.Device = tempDev;
            }
            else if (workOrder.WorkOrderCategory.Category == "Ramp" && workOrder.RampRequestId != null)
            {
                var tempRamp = clientService.GetRampRequest(workOrder.RampRequestId);
                viewModel.RampRequest = tempRamp;
            }
            else if (workOrder.WorkOrderCategory.Category == "Lawn Service")
            {
                string listClientIsOn = clientService.ListClientIsOn(workOrder.ClientRequest.ClientId);
                if (listClientIsOn != "Not on a list")
                {
                    viewModel.ClientOnList = true;
                }
            }

            return viewModel;
        }

        public static UpdateIncompleteWorkOrderViewModel ReinitializeForInvalidModelState(IClientService clientService, UpdateIncompleteWorkOrderViewModel viewModel)
        {
            viewModel.Categories = clientService.GetworkOrderCategoryNamesForCreation()
                                        .Where(cat => !cat
                                        .Equals("Lawn Service")
                                        && !cat.Equals("Ramp")
                                        && !cat.Equals("Assistive Device"))
                                        .ToList();
            if (viewModel.WorkOrderCategory == "Assistive Device")
            {
                var device = clientService.GetAssistiveDevice(viewModel.DeviceId);
                viewModel.Device = device;
                viewModel.DeviceCategories = clientService.GetActiveAssistiveDeviceCategories();
                viewModel.Devices = clientService.GetActiveAssistiveDevices(device.CategoryId);
            }
            return viewModel;
        }
    }
}

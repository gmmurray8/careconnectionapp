﻿using System.Collections.Generic;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;

namespace CareConnectionApp.ViewModels.WorkOrders
{
    public class CreateAssistiveDeviceViewModel : CreateWorkOrderViewModel
    {
        public int DeviceId { get; set; }
        public int CategoryId { get; set; }
        public IEnumerable<AssistiveDevice> assistiveDevices { get; set; }
        public IEnumerable<AssistiveDeviceCategory> categories { get; set; }

        public static CreateAssistiveDeviceViewModel Initialize(IClientService clientService, CreateWorkOrderViewModel viewModel)
        {
            return new CreateAssistiveDeviceViewModel
            {
                ClientId = viewModel.ClientId,
                WorkOrderCategory = viewModel.WorkOrderCategory,
                ReqDescription = viewModel.ReqDescription,
                assistiveDevices = clientService.GetActiveAssistiveDevices(),
                categories = clientService.GetActiveAssistiveDeviceCategories()
            };
        }

        public static WorkOrder GetWorkOrder(IClientService clientService, CreateAssistiveDeviceViewModel viewModel)
        {
            return new WorkOrder
            {
                WorkOrderCategoryId = clientService.GetWorkOrderCategoryId(viewModel.WorkOrderCategory),
                ReqDescription = viewModel.ReqDescription
            };
        }
    }
}

﻿using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;

namespace CareConnectionApp.ViewModels.WorkOrders
{
    public class WorkDetailsViewModel
    {
        public Client Client { get; set; }
        public CareConnectionApp.Models.WorkOrder WorkOrder { get; set; }
        public AssistiveDevice AssistiveDevice { get; set; }

        public static WorkDetailsViewModel Initialize(IClientService clientService, Client client, WorkOrder workOrder)
        {
            return new WorkDetailsViewModel
            {
                Client = client,
                WorkOrder = workOrder,
                AssistiveDevice = clientService.GetAssistiveDevice(workOrder.AssistiveDeviceId)
            };
        }
    }
}

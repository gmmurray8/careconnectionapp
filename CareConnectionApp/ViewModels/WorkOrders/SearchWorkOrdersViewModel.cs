﻿using System.Collections.Generic;
using System.Linq;
using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using CareConnectionApp.Utility_Classes;

namespace CareConnectionApp.ViewModels.WorkOrders
{
    public class SearchWorkOrdersViewModel : ResultsPage
    {
        
        public JobStatus JobStatus { get; set; }
        public List<string> Categories { get; set; }
        public string WorkOrderCategory { get; set; }
        public IEnumerable<WorkOrder> WorkOrders { get; set; }
        public WorkDetailsViewModel WorkDetailsViewModel { get; set; }
        public string SortBy { get; set; }
        public string SortOrder { get; set; }
        public bool FromPost { get; set; }

        public static SearchWorkOrdersViewModel Initialize(IClientService clientService, SearchWorkOrdersViewModel viewModel, bool initial)
        {
            viewModel.PageNum = initial ? 1 : viewModel.PageNum + 1;
            viewModel.ResultsPerPage = 15;

            viewModel.WorkOrders = LazyLoader.LazyLoad(clientService.FilterWorkOrders(viewModel.WorkOrderCategory, viewModel.JobStatus), viewModel.PageNum,
            viewModel.ResultsPerPage, out int results, out int ShownResults).Cast<WorkOrder>().ToList();
            viewModel.WorkOrders = ListSorter.WorkOrderSort(viewModel.WorkOrders, viewModel.SortBy, viewModel.SortOrder);

            viewModel.Categories = clientService.GetWorkOrderCategoryNamesForSearching();
            viewModel.ShownResults = ShownResults;
            viewModel.TotalResults = results;

            return viewModel;
        }
    }
}

﻿using CareConnectionApp.Models;
using System.Collections.Generic;

namespace CareConnectionApp.ViewModels.WorkOrders
{
    public class AssistiveDeviceCategoriesViewModel
    {
        public IEnumerable<AssistiveDeviceCategory> assistiveDeviceCategories { get; set; }
    }
}

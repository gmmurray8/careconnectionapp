﻿using CareConnectionApp.Models;
using System.Collections.Generic;

namespace CareConnectionApp.ViewModels.LawnService
{
    public class ViewPendingApprovalViewModel
    {
        public IEnumerable<PendingApproval> PendingApprovals { get; set; }
    }
}

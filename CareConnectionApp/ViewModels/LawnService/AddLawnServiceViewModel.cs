﻿using CareConnectionApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CareConnectionApp.ViewModels.LawnService
{
    public class AddLawnServiceViewModel
    {
        public IEnumerable<Zone> Zones { get; set; }
        [Required(ErrorMessage = "Please select a zone.")]
        public int ZoneId { get; set; }
        [Required(ErrorMessage = "Please enter a date.")]
        public DateTime TimeStamp { get; set; }
    }
}

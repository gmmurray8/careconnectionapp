﻿using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System.Collections.Generic;

namespace CareConnectionApp.ViewModels.LawnService
{
    public class PrintLawnServiceViewModel
    {
        public IEnumerable<LawnServiceList> LawnServiceList { get; set; }
        public Zone Zone { get; set; }
        public int DaysSinceLastService { get; set; }

        public static PrintLawnServiceViewModel Initialize(IClientService clientService, int id)
        {
            return new PrintLawnServiceViewModel
            {
                LawnServiceList = clientService.GetLawnServiceListForPrinting(id),
                Zone = clientService.GetZoneById(id),
                DaysSinceLastService = clientService.DaysSinceLastService(id)
            };
        }
    }
}

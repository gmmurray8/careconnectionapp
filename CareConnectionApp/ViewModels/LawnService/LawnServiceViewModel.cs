﻿using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CareConnectionApp.ViewModels.LawnService
{
    public class LawnServiceViewModel
    {
        [Required(ErrorMessage ="Please pick a zone")]
        public int Id { get; set; }
        public IEnumerable<Zone> Zones { get; set; }
        public Dictionary<LawnServiceList, string> LawnServiceLists { get; set; }
        public int? DaysSinceLastService { get; set; }
        public string LastService { get; set; }
        public int NumberOfClients { get; set; }
        public string TooManyClientsWarning { get; set; }

        public static LawnServiceViewModel Initialize(IAdminService adminService, IClientService clientService, ApplicationUser user)
        {
            var lawnServiceList = clientService.GetLawnServiceList();
            var viewModel = new LawnServiceViewModel();
            if (user != null)
            {
                viewModel.TooManyClientsWarning = adminService.TooManyClientsWarning(user.Id);
                viewModel.Zones = clientService.GetZones();
                viewModel.LawnServiceLists = lawnServiceList;
                viewModel.NumberOfClients = lawnServiceList.Count();
            }
            else
            {
                viewModel.Zones = clientService.GetZones();
                viewModel.LawnServiceLists = lawnServiceList;
                viewModel.NumberOfClients = lawnServiceList.Count();
            }
            return viewModel;
        }

        public static LawnServiceViewModel Update(IClientService clientService, int id)
        {
            var viewModel = new LawnServiceViewModel
            {
                Zones = clientService.GetZones(),
                DaysSinceLastService = clientService.DaysSinceLastService(id),
                LawnServiceLists = clientService.GetLawnServiceList(id),
                NumberOfClients = clientService.GetLawnServiceList(id).Count()
            };
            if (viewModel.DaysSinceLastService != -1)
            {
                viewModel.LastService = DateTime.Now.AddDays(-(viewModel.DaysSinceLastService ?? default)).ToString("MM/dd/yyyy");
            }
            return viewModel;
        }
    }
}

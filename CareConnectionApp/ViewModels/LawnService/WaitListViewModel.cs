﻿using CareConnectionApp.Models;
using CareConnectionApp.Services.Abstractions;
using System.Collections.Generic;
using System.Linq;

namespace CareConnectionApp.ViewModels.LawnService
{
    public class WaitListViewModel
    {
        public int Id { get; set; }
        public IEnumerable<WaitList> WaitList { get; set; }
        public IEnumerable<Zone> Zones { get; set; }
        public bool FullServiceList { get; set; }
        public string TooManyClientsWarning { get; set; }
        public int NumberOfClients { get; set; }

        public static WaitListViewModel Initialize(IClientService clientService, IAdminService adminService, ApplicationUser user)
        {
            if (user != null)
            {
                return new WaitListViewModel
                {
                    WaitList = clientService.GetWaitList(),
                    Zones = clientService.GetZones(),
                    TooManyClientsWarning = adminService.TooManyClientsWarning(user.Id),
                    NumberOfClients = clientService.GetWaitList().Count()
                };
            }
            else
            {
                return new WaitListViewModel
                    {
                        WaitList = clientService.GetWaitList(),
                        Zones = clientService.GetZones(),
                    };
            }
        }

        public static WaitListViewModel Update(IClientService clientService, int id)
        {
            return new WaitListViewModel
            {
                WaitList = clientService.GetWaitListByZone(id),
                Zones = clientService.GetZones(),
                NumberOfClients = clientService.GetWaitListByZone(id).Count()
            };
        }
    }
}

﻿using CareConnectionApp.Enums;
using CareConnectionApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CareConnectionApp.ViewModels.AbstractViewModels
{
    public abstract class AbstractUpdateWorkOrderViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter the category.")]
        [Display(Name = "Category")]
        public string WorkOrderCategory { get; set; }
        public List<string> Categories { get; set; }
        [Required(ErrorMessage = "Please enter the description.")]
        [Display(Name = "Request Description")]
        public string ReqDescription { get; set; }

        [Required(ErrorMessage = "Please enter the date of request.")]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Request")]
        public DateTime DateOfRequest { get; set; }
        [Required(ErrorMessage = "Please select a job status.")]
        public JobStatus? JobStatus { get; set; }
        public int ClientId { get; set; }

        [Display(Name = "Secondary Contact Name")]
        public string SecondaryContact { get; set; }
        [Display(Name = "Secondary Contact Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Invalid Phone Number")]
        public string SecondaryContactNumber { get; set; }

        //public WorkOrderCompletionForm WorkOrderCompletionForm { get; set; }
        public IEnumerable<AssistiveDeviceCategory> DeviceCategories { get; set; }
        public List<AssistiveDevice> Devices { get; set; }
        public AssistiveDevice Device { get; set; }
        public int? DeviceId { get; set; }
        public RampRequest RampRequest { get; set; }
        public bool ClientOnList { get; set; }
    }
}
